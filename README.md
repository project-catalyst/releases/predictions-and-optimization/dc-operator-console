# DC Operator Console

This component is responsible for displaying relevant information for the DC Operator concerning the electrical and thermal energy forecasting process as well as on flexibility optimization decision making.