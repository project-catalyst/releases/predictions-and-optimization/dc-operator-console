FROM node:8
WORKDIR /frontend
ENV CATALYST_DB_API=${CATALYST_DB_API}
COPY ./catalyst-frontend/package.json /frontend
RUN npm install
COPY ./catalyst-frontend /frontend
CMD npm start
