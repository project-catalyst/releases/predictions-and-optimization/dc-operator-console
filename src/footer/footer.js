import React from "react";
import {SocialIcon} from 'react-social-icons';
import {Button, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Row} from "reactstrap";
import AccountContainer from "../account/account-container";
import Redirect from "react-router-dom/Redirect";

const footerStyle = {
    backgroundColor: "#343a40",
    fontSize: "20px",
    color: "white",
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    padding: "20px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "60px",
    width: "100%"
};

const phantomStyle = {
    display: "block",
    padding: "20px",
    height: "60px",
    width: "100%"
};

class Footer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            modal_open: false,
            redirect: false
        };

        this.handleLoginButton = this.handleLoginButton.bind(this);
    }

    handleLoginButton() {
        if (localStorage.getItem("username") === null || localStorage.getItem("username") === undefined ) {
            this.setState({
                modal_open: !this.state.modal_open,
                redirect: false
            });
        }else{
            localStorage.removeItem("username");
            localStorage.removeItem("user_id");

            this.setState({
                modal_open: false,
                redirect: true
            });
        }

    }


    render() {
        let login_btn = (localStorage.getItem("username") === null || localStorage.getItem("username") === undefined ) ? "Login" : "Logout";
        return (
            <div style={phantomStyle}>
                {this.state.modal_open && <AccountContainer/>}
                {this.state.redirect && <Redirect to={"/catalyst-dc-operator-console"}/>}
                <div style={footerStyle}>
                    <Row>
                        <Col>
                            <small>
                                <a href="http://project-catalyst.eu" style={{color: "#4caf50"}}>Catalyst Project
                                    Official Page
                                </a>
                            </small>
                        </Col>
                        <Col>
                            <small>
                                <a href="https://www.linkedin.com/company/distributed-system-research-laboratory/"
                                   style={{color: "#4caf50"}}>
                                    Distributed Systems Research Laboratory</a>
                                <SocialIcon style={{height: 25, width: 25}}
                                            url="https://www.linkedin.com/company/distributed-system-research-laboratory/"/>
                            </small>
                        </Col>
                        <Col>
                            <small>
                                <a href="https://www.utcluj.ro/en/" style={{color: "#4caf50"}}>
                                    Technical University of Cluj-Napoca , Romania </a> </small>
                        </Col>
                        <Col>
                            <small>
                            <Button onClick={this.handleLoginButton}
                                    outline color='link' size="sm"> {login_btn} </Button></small>
                        </Col>
                    </Row>
                </div>
            </div>


        );
    };
}

export default Footer;

