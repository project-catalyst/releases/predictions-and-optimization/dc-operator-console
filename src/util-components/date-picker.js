import React from 'react'
import DayPickerInput from "react-day-picker/DayPickerInput";
import dateFnsFormat from 'date-fns/format';

import dateFnsParse from 'date-fns/parse';
import {DateUtils} from "react-day-picker";
import en from "date-fns/locale/en-GB";

const FORMAT = 'dd/MM/yyyy';

function parseDate(str, format, locale) {
    const parsed = dateFnsParse(str, format, new Date(), {locale:en});
    if (DateUtils.isDate(parsed)) {
        return parsed;
    }
    return undefined;
}

function formatDate(date, format, locale) {
    return dateFnsFormat(date, format, {locale:en});
}
let selectedDay =  new Date();

const DatePicker = ({handler}) => (

    <div>
        <p>Select a date: </p>
        <DayPickerInput
            formatDate={formatDate}
            format={FORMAT}
            parseDate={parseDate}
            placeholder={`${dateFnsFormat(selectedDay, FORMAT)}`}
            onDayChange={handler}/>
    </div>

);
export default DatePicker
