export const TIMEFRAME = {
        DAYAHEAD : "dayahead",
        INTRADAY : "intraday",
        REALTIME_INTRADAY_FRAME : "realtime-id",
        REALTIME_DAYAHEAD_FRAME : "realtime-da",
};