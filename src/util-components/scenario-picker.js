import React from "react";
import {Input, Label} from "reactstrap";

const INTERVALS = [
    {key: 1590969600000, label: "Scenario1"},
    {key: 1591142400000, label: "Scenario2"},
    {key: 1591315200000, label: "Scenario3"},
];

const ScenarioPicker = ({handler}) => (
    <div>
        <Label> Select a Scenario </Label>
        <div >
            <Input type='select' onChange={handler} label='TimeInterval'>
                {INTERVALS.map((x) => (
                    <option key={x.key} value={x.key}>{x.label}</option>))}
            </Input>
        </div>
    </div>

);
export default ScenarioPicker
