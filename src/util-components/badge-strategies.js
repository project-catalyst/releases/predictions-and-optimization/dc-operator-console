import React from "react";
import * as API_OPTIMIZATION from "../optimization/api/optimization-api";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Badge, Row} from "reactstrap";


class BadgeStrategies extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            time: this.props.time,
            confidenceLevel: this.props.confidenceLevel,
            data: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        API_OPTIMIZATION.fetchOptimizationStrategies(this.state.time, this.state.confidenceLevel, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    data: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    render() {
        return (
            <div>
                {
                    this.state.isLoaded &&
                    <Row>
                        <Badge color="primary">WE: {this.state.data.we}</Badge>
                        <Badge color="danger">WF: {this.state.data.wf}</Badge>
                        <Badge color="info">WT: {this.state.data.wt}</Badge>
                        <Badge color="warning">WRen: {this.state.data.ren}</Badge>
                        <Badge color="success">Realloc: {this.state.data.reallocActive? 1 : 0}</Badge>
                    </Row>
                }
                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage
                        errorStatus={this.state.errorStatus}
                        error={this.state.error}
                    />
                }
            </div>
        )

    }
}

export default BadgeStrategies
