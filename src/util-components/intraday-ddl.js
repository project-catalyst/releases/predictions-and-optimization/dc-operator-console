import React from 'react'
import {Input, Label} from 'reactstrap';

const INTERVALS = [
    {hour: 0, label: "0-4"},
    {hour: 4, label: "4-8"},
    {hour: 8, label: "8-12"},
    {hour: 12, label: "12-16"},
    {hour: 16, label: "16-20"},
    {hour: 20, label: "20-24"}
];

const IntradayDDL = ({handler}) => (
    <div>
        <Label> Time Interval (hours): </Label>
        <div >
            <Input type='select' onChange={handler} label='TimeInterval'>
                {INTERVALS.map((x) => (
                    <option key={x.hour} value={x.hour}>{x.label}</option>))}
            </Input>
        </div>
    </div>
);
export default IntradayDDL
