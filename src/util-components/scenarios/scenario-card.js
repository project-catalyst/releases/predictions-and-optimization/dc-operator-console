import React from "react";
import { Col, Row} from "reactstrap";
import styles from "../../commons/styles/catalyst-style.css";
import Label from "reactstrap/es/Label";
import Slider from "react-rangeslider";


const textStyle = {
    color: 'gray',
    bold: 'true',
    textAlign: 'justify'
};

const subTitleStyle = {
    color: '#009245',
    fontSize: '20px',
    bold: 'true'
};



class ScenarioCard extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            scenarioDetails: this.props.scenarioDetails
        };
    }

    render() {
        return (

            <Row>
                <Col>
                    <strong className={styles.subtitle}
                            style={subTitleStyle}> {this.state.scenarioDetails.scenarioTitle} </strong> <br/><br/>
                    {/*<b>Service Name : </b> {this.state.scenarioDetails.serviceName} <br/>*/}
                    <b>Description</b>: {this.state.scenarioDetails.description} <br/>
                    <b>Time to response</b>: {this.state.scenarioDetails.timeToResponse}<br/>
                    <b>Response length</b>: {this.state.scenarioDetails.responseLength}
                    <br/><br/><br/><br/>

                </Col>
                <Col xs='6'>
                    <Label><strong style={textStyle}> DC Profit Maximization Strategy Weight
                        (WE) </strong></Label>
                    <Slider
                        min={0}
                        max={100}
                        value={this.state.scenarioDetails.we}
                        disabled={true}
                    />
                    <Label><strong style={textStyle}> DC Electrical Flexibility Strategy Weight
                        (WF) </strong></Label>
                    <Slider
                        min={0}
                        max={100}
                        value={this.state.scenarioDetails.wf}
                        disabled={true}
                    />
                    <Label><strong style={textStyle}> DC Thermal Flexibility Strategy Weight
                        (WT) </strong></Label>
                    <Slider
                        min={0}
                        max={100}
                        value={this.state.scenarioDetails.wt}
                        disabled={true}
                    />
                    <Label><strong style={textStyle}> Workload Realloc </strong> <input type="checkbox" readOnly={true}
                                                                                        checked={this.state.scenarioDetails.realloc}/>
                    </Label>

                </Col>
            </Row>
        )

    }
}

export default ScenarioCard