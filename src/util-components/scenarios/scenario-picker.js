import React from "react";
import {Col, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import {MdInfoOutline} from "react-icons/md";
import ReactTooltip from "react-tooltip";
import styled from "styled-components";
import ScenarioCard from "./scenario-card";

import {SCENARIO_INFO, SCENARIO_INTERVALS} from "./scenario-info";




const Button = styled.button`
  margin: 1rem;
  padding: 0.1rem;
  font-size: inherit;
  background: none;
  border: none;
  outline: none;
  transition: color 0.1s ease-out;
  &:active {
    transform: translateY(0px);
  }
  &:focus {
    outline: 1px dashed skyblue;
    outline-offset: 0px;
  }
`;

class ScenarioPicker extends React.Component {

    constructor(props) {
        super(props);
        this.testFunction = this.testFunction.bind(this);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            handler: this.props.handler,
            selectedIndex: 0,
            selected: false,
            collapseForm: false
        };
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    handleChange(position) {
        this.setState(
            {selectedIndex: position});
    }

    testFunction(e) {
        let poz = e.target.value;
        this.props.handler(SCENARIO_INTERVALS[poz].key);
        this.handleChange(poz);
    }

    render() {
        return (

            <div>
                <Label> Select a Scenario </Label>
                <div>
                    <Row>
                        <Col xs="8">
                            <Input type='select' onChange={this.testFunction} label='TimeInterval'>
                                {SCENARIO_INTERVALS.map((x) => (
                                    <option key={x.key} value={x.position}>{x.label}</option>))}

                            </Input>
                        </Col>
                        <Col xs="1">
                            <Button
                                onClick={this.toggleForm}
                                aria-label="Info toggle"
                                aria-pressed={this.state.selected}
                                style={{color: this.state.selected ? "palevioletred" : "#555"}}
                                data-tip data-for="infoTip">
                                <MdInfoOutline />
                                <ReactTooltip id="infoTip" place="top" effect="solid">
                                   More information about the selected scenario
                                </ReactTooltip>
                            </Button>
                        </Col>
                    </Row>
                </div>


                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Scenario Information : </ModalHeader>
                    <ModalBody>
                        <ScenarioCard scenarioDetails={SCENARIO_INFO[this.state.selectedIndex]}/>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.toggleForm}>Cancel</Button>
                    </ModalFooter>
                </Modal>

            </div>

        )

    }
}

export default ScenarioPicker
