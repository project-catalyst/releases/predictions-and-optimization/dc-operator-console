
import React from "react";

export const SCENARIO_INFO = [
    {
        scenarioTitle : "Scenario 1",
        serviceName : "",
        description : "The Intra DC Energy Optimizer computes an action plan to minimize the DC operational cost by shifting flexible energy to decrease energy profile when energy price is high and to increase energy profile when energy price is low. ",
        timeToResponse:  "2 hours",
        responseLength:  "24 hours",
        we :  100,
        wf :  0,
        wt :  0,
        realloc:  false
    },

    {
        scenarioTitle : "Scenario 2",
        serviceName : "",
        description : "The Intra DC Energy Optimizer computes an action plan to allow the DC to follow a flexibility order curve and maximize gain of the service associated incentives. ",
        timeToResponse:  "2 hours",
        responseLength:  "24 hours",
        we :  0,
        wf :  100,
        wt :  0,
        realloc:  false
    },

    {
        scenarioTitle : "Scenario 3",
        serviceName : "",
        description : "The Intra DC Energy Optimizer computes an action plan to maximize the DC heat generation when the heat price is high by shifting flexible thermal energy ",
        timeToResponse:  "2 hours",
        responseLength:  "24 hours",
        we :  0,
        wf :  0,
        wt :  100,
        realloc:  false
    },

    {
        scenarioTitle : "Scenario 4",
        serviceName : "",
        description : "The Intra DC Energy Optimizer computes an action plan to allow the DC to sell heat in the heat market when the prices are high and at the same time buy electrical energy when the prices are low. ",
        timeToResponse:  "2 hours",
        responseLength:  "24 hours",
        we :  50,
        wf :  0,
        wt :  50,
        realloc:  true
    }
    ,

    {
        scenarioTitle : "Scenario 5",
        serviceName : "",
        description : "The Intra DC Energy Optimizer computes an action plan to allow the DC to provide flexibility services by decreasing its demand using workload relocation to other DC.",
        timeToResponse:  "2 hours",
        responseLength:  "24 hours",
        we :  0,
        wf :  100,
        wt :  0,
        realloc:  true
    }    ,

    {
        scenarioTitle : "Scenario 6",
        serviceName : "",
        description : "The Intra DC Energy Optimizer computes an action plan allowing DC to provide flexibility services by increasing its demand hosting workload from other DCs and at the same time sells waste heat taking advantage on high heat prices on the market.",
        timeToResponse:  "2 hours",
        responseLength:  "24 hours",
        we :  0,
        wf :  50,
        wt :  50,
        realloc:  true
    }

];


export const SCENARIO_INTERVALS = [
    {key: 1590969600000, label: "Scenario1", position: 0},
    {key: 1591142400000, label: "Scenario2", position: 1},
    {key: 1591315200000, label: "Scenario3", position: 2},
    {key: 1591488000000, label: "Scenario4", position: 3},
    {key: 1591660800000, label: "Scenario5", position: 4},
    {key: 1592006400000, label: "Scenario6", position: 5}
];