import React from 'react'
import logo from '../commons/images/catalyst_logo.png';
import {
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavItem,
    NavLink
} from 'reactstrap';
import * as config from "../config";

const navStyle = {
    color: 'white',
    textDecoration: 'none'
};

const textStyle = {
    color: 'gray',
    textDecoration: 'none'
};

class NavigationBar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            electricalDropdownOpen: false,
            thermalDropdownOpen: false,
            optimizationDropdownOpen: false,
            executionDropdownOpen: false,
            monitoringDropdownOpen: false,
            activeTab: localStorage.getItem("activeTab")
        };
        this.toggleTab = this.toggleTab.bind(this);
        this.toggleECDropdown = this.toggleECDropdown.bind(this);
        this.toggleOPTDropdown = this.toggleOPTDropdown.bind(this);
        this.toggleMonitoringDropdown = this.toggleMonitoringDropdown.bind(this);
    }

    toggleECDropdown() {
        this.setState(prevState => ({
            electricalDropdownOpen: !prevState.electricalDropdownOpen
        }));
    }

    toggleOPTDropdown() {
        this.setState(prevState => ({
            optimizationDropdownOpen: !prevState.optimizationDropdownOpen
        }));
    }

    toggleMonitoringDropdown() {
        this.setState(prevState => ({
            monitoringDropdownOpen: !prevState.monitoringDropdownOpen
        }));
    }

    toggleTab(tab) {
        if (this.state.activeTab !== tab) {
            localStorage.setItem("activeTab", tab);
        }
    }

    render() {

        return (
            <div>
                <Navbar color="dark" expand="md">
                    <NavbarBrand href="/catalyst-dc-operator-console">
                        <img src={logo} width={"178"}
                             height={"42"} alt="catalyst"/>
                    </NavbarBrand>
                    <Nav className="mr-auto" navbar>
                        <Dropdown nav isOpen={this.state.monitoringDropdownOpen} toggle={this.toggleMonitoringDropdown}>
                            <DropdownToggle align="right" style={navStyle} nav caret>
                                { ( this.state.activeTab === '1' )?
                                    <strong>  DC Monitoring </strong> :
                                    <label>  DC Monitoring </label>}
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem align="right" style={textStyle}
                                              href="/catalyst-dc-operator-console/electrical/monitoring"
                                              onClick={() => { this.toggleTab('1'); }}>Electrical
                                    Monitoring</DropdownItem>
                                <DropdownItem divider/>
                                <DropdownItem align="right" style={textStyle}
                                              href="/catalyst-dc-operator-console/thermal/monitoring"
                                              onClick={() => { this.toggleTab('1'); }}> Thermal
                                    Monitoring</DropdownItem>
                            </DropdownMenu>
                        </Dropdown>

                        <Dropdown nav isOpen={this.state.electricalDropdownOpen} toggle={this.toggleECDropdown}>
                            <DropdownToggle align="right" style={navStyle} nav caret>
                                { ( this.state.activeTab === '2' )?
                                    <strong>  DC Electricity and Thermal DR Prediction </strong> :
                                    <label> DC Electricity and Thermal DR Prediction </label>}
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem align="right" style={textStyle}
                                              href="/catalyst-dc-operator-console/electrical/prediction"
                                              onClick={() => { this.toggleTab('2'); }}>Electrical
                                    Prediction</DropdownItem>
                                <DropdownItem divider/>
                                <DropdownItem align="right" style={textStyle}
                                              href="/catalyst-dc-operator-console/thermal/prediction"
                                              onClick={() => { this.toggleTab('2'); }}> Thermal
                                    Prediction</DropdownItem>
                            </DropdownMenu>
                        </Dropdown>

                        {   config.CONFIG.IS_PILOT &&  <NavItem >
                            <NavLink active align="right" style={navStyle}
                                     href="/catalyst-dc-operator-console/flexibility-strategies"
                                     onClick={() => { this.toggleTab('3'); }}>
                                { ( this.state.activeTab === '3' )?
                                    <strong> Flexibility Strategies</strong> :
                                    <label> Flexibility Strategies</label>}
                            </NavLink>
                        </NavItem>
                        }

                        <Dropdown nav isOpen={this.state.optimizationDropdownOpen} toggle={this.toggleOPTDropdown}>
                            <DropdownToggle align="right" style={navStyle} nav caret>
                                { ( this.state.activeTab === '4' )?
                                    <strong>  DC Flexibility Management and Optimization</strong> :
                                    <label>  DC Flexibility Management and Optimization</label>}
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem align="right" style={textStyle}
                                              href="/catalyst-dc-operator-console/optimization/plan/da/energy"
                                              onClick={() => { this.toggleTab('4'); }}>Optimized Energy
                                    Plan</DropdownItem>
                                <DropdownItem divider/>
                                <DropdownItem  align="right" style={textStyle}
                                              href="/catalyst-dc-operator-console/optimization/plan/da/thermal"
                                              onClick={() => { this.toggleTab('4'); }}>Optimized
                                    Thermal Plan</DropdownItem>
                                <DropdownItem divider/>
                                <DropdownItem  align="right" style={textStyle}
                                              href="/catalyst-dc-operator-console/optimization/plan/da/price-driven"
                                              onClick={() => { this.toggleTab('4'); }}>Optimized
                                    Price Driven Plan</DropdownItem>
                                <DropdownItem divider/>
                                <DropdownItem  align="right" style={textStyle}
                                               href="/catalyst-dc-operator-console/optimization/plan/da/renewable"
                                               onClick={() => { this.toggleTab('4'); }}>Optimized
                                    Renewable Energy Plan</DropdownItem>
                            </DropdownMenu>
                        </Dropdown>

                        <NavItem >
                            <NavLink active align="right" style={navStyle}
                                     href="/catalyst-dc-operator-console/metrics"
                                     onClick={() => { this.toggleTab('5'); }}>
                                { ( this.state.activeTab === '5' )?
                                    <strong> Metrics</strong> :
                                    <label> Metrics</label>}
                            </NavLink>
                        </NavItem>
                        {
                            (localStorage.getItem("username") !== null) &&
                            <NavItem >
                                <NavLink active align="right" style={navStyle}
                                         href="/catalyst-dc-operator-console/datacenters"
                                         onClick={() => { this.toggleTab('6'); }}>
                                    { ( this.state.activeTab === '6' )?
                                        <strong> Data Centers</strong> :
                                        <label> Data Centers</label>}
                                </NavLink>
                            </NavItem>
                        }
                    </Nav>

                    {/*<Col>*/}
                    {/*    <DropdownButton variant="success" id={'first'} title={'DC Electricity and Thermal DR Prediction'}>*/}
                    {/*        <DropdownItem>*/}
                    {/*            <DropdownButton variant="success" drop={'right'} id={'electrical'} title={'Electrical Energy'}>*/}
                    {/*                <DropdownItem>*/}
                    {/*                    <NavLink href="/catalyst-dc-operator-console/electrical/history">History</NavLink>*/}
                    {/*                </DropdownItem>*/}
                    {/*                <DropdownItem>*/}
                    {/*                    <NavLink*/}
                    {/*                        href="/catalyst-dc-operator-console/electrical/prediction">Prediction</NavLink>*/}
                    {/*                </DropdownItem>*/}
                    {/*                <DropdownItem>*/}
                    {/*                    <NavLink*/}
                    {/*                        href="/catalyst-dc-operator-console/electrical/monitoring">Monitoring</NavLink>*/}
                    {/*                </DropdownItem>*/}
                    {/*            </DropdownButton>*/}
                    {/*        </DropdownItem>*/}
                    {/*        <DropdownItem>*/}
                    {/*            <DropdownButton variant="success" drop={'right'} id={'thermal'} title={'Thermal Energy'}>*/}
                    {/*                <DropdownItem>*/}
                    {/*                    <NavLink href="/catalyst-dc-operator-console/thermal/history">History</NavLink>*/}
                    {/*                </DropdownItem>*/}
                    {/*                <DropdownItem>*/}
                    {/*                    <NavLink*/}
                    {/*                        href="/catalyst-dc-operator-console/thermal/prediction">Prediction</NavLink>*/}
                    {/*                </DropdownItem>*/}
                    {/*                <DropdownItem>*/}
                    {/*                    <NavLink*/}
                    {/*                        href="/catalyst-dc-operator-console/thermal/monitoring">Monitoring</NavLink>*/}
                    {/*                </DropdownItem>*/}
                    {/*            </DropdownButton>*/}
                    {/*        </DropdownItem>*/}
                    {/*    </DropdownButton>*/}
                    {/*</Col>*/}
                    {/*<Col>*/}
                    {/*    <Button  variant="success" id={'second'} title={'DC Flexibility Management and Optimization'}>*/}
                    {/*        DC Flexibility Management and Optimization*/}
                    {/*    </Button>*/}
                    {/*</Col>*/}
                    {/*<Col>*/}
                    {/*    <Button href="/catalyst-dc-operator-console/flexibility-strategies" variant="success" id={'second'} title={'Flexibility Strategies'}>*/}
                    {/*        Flexibility Strategies*/}
                    {/*    </Button>*/}
                    {/*</Col>*/}
                </Navbar>
            </div>
        );
    }
}

export default NavigationBar

