import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import styles from './commons/styles/catalyst-style.css';
import NavigationBar from './navigation/navigation-bar';
import ElectricalPredictionContainer from './electrical/prediction/electrical-prediction-container';
import Home from './home/home';
import ErrorPage from './commons/errorhandling/error-page';
import Footer from './footer/footer';
import ThermalPredictionContainer from "./thermal/prediction/thermal-prediction-container"
import ElectricalMonitoringContainer from './electrical/monitoring/electrical-monitoring-container';
import ThermalMonitoringContainer from './thermal/monitoring/thermal-monitoring-container';
import FlexibilityStrategiesContainer from "./flexibility-strategies/flexibility-strategies-container";

import EnergyExecutionContainer from "./optimization/realtime-execution/energy/energy-execution-container";
import ThermalExecutionContainer from "./optimization/realtime-execution/thermal/thermal-execution-container";
import EnergyExecutionContainerScenario from "./optimization/realtime-execution/energy/energy-execution-container-scenario";
import ThermalExecutionContainerScenario from "./optimization/realtime-execution/thermal/thermal-execution-container-scenario";
import EnergyDaPlanContainer from "./optimization/plans/energy/energy-da-plan-container";
import EnergyIdPlanContainer from "./optimization/plans/energy/energy-id-plan-container";
import ThermalDaPlanContainer from "./optimization/plans/thermal/thermal-da-plan-container";
import Redirect from "react-router-dom/Redirect";
import {CONFIG} from "./config";
import ThermalIdPlanContainer from "./optimization/plans/thermal/thermal-id-plan-container";
import Metrics from "./metrics/metrics";
import PriceDrivenDaPlanContainer from "./optimization/plans/price-driven/price-driven-da-plan-container";
import PriceDrivenIdPlanContainer from "./optimization/plans/price-driven/price-driven-id-plan-container";
import ComponentConfigurationEdit from "./dc_management/component-configuration-edit";
import ComponentConfiguration from "./dc_management/component-configuration";
import Datacenters from "./dc_management/datacenters";
import AccountContainer from "./account/account-container";
import {Authorization} from "./account/authorization";
import PriceDrivenExecutionContainer
    from "./optimization/realtime-execution/price-driven/price-driven-execution-container";
import PriceDrivenExecutionContainerScenario
    from "./optimization/realtime-execution/price-driven/price-driven-execution-container-scenario";
import RenewableExecutionContainerScenario
    from "./optimization/realtime-execution/renewable/renewable-execution-container-scenario";
import RenewableExecutionContainer from "./optimization/realtime-execution/renewable/renewable-execution-container";
import RenewableDaPlanContainer from "./optimization/plans/renewable/renewable-da-plan-container";
import RenewableIdPlanContainer from "./optimization/plans/renewable/renewable-id-plan-container";


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            electricityAndThermalDisabled: true,
            optimizationDisabled: true,
            startTime: null,
            dataCenterId: null,
            active: false,
            stateRedirect: true
        };
        this.enableElectricityAndThermal = this.enableElectricityAndThermal.bind(this);
        this.enableOptimization = this.enableOptimization.bind(this);
    }

    componentDidMount() {
        this.setState({
            retard:true,
            stateRedirect: true
        })
    }
    /* let functions here to come back when adaptions to scenarios will be made*/
    choseScenario1() {
        //datacenter Paper
        // localStorage.setItem('startTime', '1561766400000');
        // localStorage.setItem('dataCenterId', '3AC2AFC3-98C3-9EC3-9D22-4469C5A1C3B6');
        // localStorage.setItem('electricityThermalDisabled', 'false');
        // localStorage.setItem('optimizationDisabled', 'true');
        // localStorage.setItem('dataCenterName', 'DATACENTER_POZNAN');
        // this.setState({
        //     active: true
        // })
    }

    choseScenario2() {
        //datacenter Poznan
        // localStorage.setItem('startTime', '1543104000000');
        // localStorage.setItem('dataCenterId', 'C382C39F-C2A5-C2BE-C38E-5011C3A9C2AD');
        // localStorage.setItem('electricityThermalDisabled', 'false');
        // localStorage.setItem('optimizationDisabled', 'true');
        // localStorage.setItem('dataCenterName', 'DATACENTER_POZNAN');
        // this.setState({
        //     active: true
        // })
    }

    choseScenario3() {
        // custom scenario for Poznan
        // localStorage.setItem('startTime', '1561766400000');
        // localStorage.setItem('dataCenterId', '3AC2AFC3-98C3-9EC3-9D22-4469C5A1C3B6');
        // localStorage.setItem('electricityThermalDisabled', 'false');
        // localStorage.setItem('optimizationDisabled', 'true');
        // localStorage.setItem('dataCenterName', 'DATACENTER_POZNAN');
        // this.setState({
        //     active: true
        // })
    }

    enableElectricityAndThermal() {
        this.setState({
            electricityAndThermalDisabled: false
        });
    }

    enableOptimization() {
        this.setState(prevState => ({
            optimizationDisabled: !prevState.optimizationDisabled
        }));
    }

    render() {

        /*let url = "";
        if(CONFIG.IS_PILOT === true){
            url = "/catalyst-dc-operator-console/electrical/monitoring";
        } else
        {
            url = "/catalyst-dc-operator-console";
        }
        */
        let url = "/catalyst-dc-operator-console";


        return (

            <div className={styles.back}>
                <Router>
                    <div>
                        {(this.state.active || window.location.pathname !== '/catalyst-dc-operator-console') && <NavigationBar
                            active={true}
                        />}
                        {(!this.state.active && window.location.pathname === '/catalyst-dc-operator-console') && <NavigationBar
                            active={false}
                        />}
                        <Switch>
                            <Route
                                exact
                                path='/catalyst-dc-operator-console'
                                render={() => <Home
                                    choseScenario1={this.choseScenario1}
                                    choseScenario2={this.choseScenario2}
                                    choseScenario3={this.choseScenario3}
                                />}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/electrical/prediction'
                                render={() => <ElectricalPredictionContainer
                                    dataCenterId={localStorage.getItem('dataCenterId')}
                                    startTime={Number(localStorage.getItem('startTime'))}/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/electrical/monitoring'
                                render={() => <ElectricalMonitoringContainer
                                    dataCenterId={localStorage.getItem('dataCenterId')}
                                    startTime={Number(localStorage.getItem('startTime'))}/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/thermal/prediction'
                                render={() => <ThermalPredictionContainer
                                    dataCenterId={localStorage.getItem('dataCenterId')}
                                    startTime={Number(localStorage.getItem('startTime'))}/>}
                            />

                            <Route
                                exact
                                path='/catalyst-dc-operator-console/thermal/monitoring'
                                render={() => <ThermalMonitoringContainer
                                    dataCenterId={localStorage.getItem('dataCenterId')}
                                    startTime={Number(localStorage.getItem('startTime'))}/>}
                            />

                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/plan/da/energy'
                                render={() => <EnergyDaPlanContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/plan/da/thermal'
                                render={() => <ThermalDaPlanContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/plan/da/price-driven'
                                render={() => <PriceDrivenDaPlanContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/plan/da/renewable'
                                render={() => <RenewableDaPlanContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/plan/id/energy'
                                render={() => <EnergyIdPlanContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/plan/id/thermal'
                                render={() => <ThermalIdPlanContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/plan/id/price-driven'
                                render={() => <PriceDrivenIdPlanContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/plan/id/renewable'
                                render={() => <RenewableIdPlanContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/flexibility-strategies'
                                render={() => <FlexibilityStrategiesContainer/>}
                            />

                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/energy/execution'
                                render={() => <EnergyExecutionContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/energy/execution-scenario'
                                render={() => <EnergyExecutionContainerScenario/>}
                            />

                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/thermal/execution'
                                render={() => <ThermalExecutionContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/thermal/execution-scenario'
                                render={() => <ThermalExecutionContainerScenario/>}
                            />

                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/price-driven/execution'
                                render={() => <PriceDrivenExecutionContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/price-driven/execution-scenario'
                                render={() => <PriceDrivenExecutionContainerScenario/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/renewable/execution'
                                render={() => <RenewableExecutionContainer/>}
                            />
                            <Route
                                exact
                                path='/catalyst-dc-operator-console/optimization/renewable/execution-scenario'
                                render={() => <RenewableExecutionContainerScenario/>}
                            />

                            <Route
                                exact
                                path='/catalyst-dc-operator-console/login'
                                render={() => <AccountContainer
                                />}
                            />

                            <Route
                                exact
                                path='/catalyst-dc-operator-console/datacenters'
                                component={Authorization(Datacenters, ['ADMIN'])}
                                />}
                            />

                            <Route
                                exact
                                path='/catalyst-dc-operator-console/component-configuration'
                                component={Authorization(ComponentConfiguration, ['ADMIN'])}
                                />}
                            />

                            <Route
                                exact
                                path='/catalyst-dc-operator-console/component-configuration-edit'
                                component={Authorization(ComponentConfigurationEdit, ['ADMIN'])}
                                />}
                            />

                            <Route
                                exact
                                path='/catalyst-dc-operator-console/metrics'
                                render={() => <Metrics/>}
                            />
                            {/*Error*/}
                            <Route
                                exact
                                path='/error'
                                render={() => <ErrorPage/>}
                            />

                            <Route render={() =><ErrorPage/>} />
                        </Switch>
                        {window.location.pathname === '/' && <Redirect to={url}/>}
                        {/*{window.location.pathname === '/catalyst-dc-operator-console' && <Redirect to={urlHome}/>}*/}
                        <Footer/>
                    </div>
                </Router>
            </div>
        )
    };
}

export default App
