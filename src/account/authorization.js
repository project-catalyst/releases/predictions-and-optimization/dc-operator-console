import React from 'react';
import Redirect from "react-router-dom/es/Redirect";

export const Authorization = (WrappedComponent, allowedRoles) => {
    return class WithAuthorization extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                user: {
                    name: localStorage.getItem("username"),
                    role: localStorage.getItem("role")
                }
            }
        }

        render() {
            const {role} = this.state.user;
            if (allowedRoles.includes(role)) {
                return <WrappedComponent {...this.props} />
            } else {
                return <Redirect to='/catalyst-dc-operator-console/login'/>
            }
        }
    }
};
