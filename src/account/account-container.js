import React from 'react';
import * as ACCOUNT_API from "./api/api";
import {Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Redirect from "react-router-dom/es/Redirect";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {withRouter} from 'react-router-dom';


class AccountContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.postLoginData = this.postLoginData.bind(this);
        this.changePage = this.changePage.bind(this);
        this.state = {
            userData: [],
            username: '',
            password: '',
            isLoaded: false,
            collapseForm: true,
            errorStatus: 0,
            error: null
        };
    }

    toggleForm() {

        this.setState({collapseForm: !this.state.collapseForm});
        let url = "/catalyst-dc-operator-console";
        this.props.history.push({
        pathname: url})
    }

    changePage(){
        let url = "/catalyst-dc-operator-console/datacenters";
        this.props.history.push({
        pathname: url})
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    postLoginData() {
        ACCOUNT_API.login(this.state.username, this.state.password, (result, status, err) => {
            if (result !== null && status === 200) {
                localStorage.setItem("username", result.username);
                localStorage.setItem("user_id", result.id);
                localStorage.setItem("role", result.role);
                this.setState({
                    userData: result,
                    collapseForm: false,
                    isLoaded: true
                });
                this.changePage()
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err,
                    collapseForm: true,
                }));
                console.log(err);
            }
        });
    };

    render() {

        return (
            <div>
                <Modal isOpen={this.state.collapseForm} toggle={this.toggleForm}
                       className={this.props.className}>
                    <ModalHeader toggle={this.toggleForm}> Already registered ? </ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label for="username">Username</Label>
                                <Input value={this.state.username} name="username" id="usernameId"
                                       onChange={this.handleChange}
                                       placeholder="My username in the platform" required/>
                            </FormGroup>
                            <FormGroup>
                                <Label for="password">Password</Label>
                                <Input type="password" value={this.state.password} name="password" id="passwordId"
                                       onChange={this.handleChange}
                                       placeholder="My password in the platform" required/>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                     {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
                        <Button color="primary" onClick={this.postLoginData}>Log In</Button>{' '}
                    </ModalFooter>
                </Modal>

              
            </div>
        )
    };
}

export default withRouter(AccountContainer)