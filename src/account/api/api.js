import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    login: '/user/login',
    resources: '/resources',
};


function login(username, password, callback) {
    let request = new Request(HOST.CATALYST_DB_API + endpoint.login, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            username: username,
            password: password
        })
    });
    RestApiClient.performRequest(request, callback);
}

export {
    login
};