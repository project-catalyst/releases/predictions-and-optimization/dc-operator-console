export const CONFIG = {
    IS_PILOT: true,
    INTRADAY_ENABLED: false,
    PILOT:  process.env.REACT_APP_PILOT || "DEFAULT", // optiuni: QUARNOT, POZNAN, PSM, SBP
};

export const GRAPH_CONFIG = {
	QARNOT: {
		Y_LIMIT_MONITORING: 170, // upper limit for montoring
		Y_LIMIT_PRICE: 1,      //price limit in charts
		Y_LIMIT_OPT_NEGATIVE: -100, //optimization and dc energy limit on charts - upper limit
		Y_LIMIT_OPT_POSITIVE: 150 //optimization and dc energy limit on charts - upper limit
	},
	PSM: {
		Y_LIMIT_MONITORING: 1000, // upper limit for montoring
		Y_LIMIT_PRICE: 1,      //price limit in charts
		Y_LIMIT_OPT_NEGATIVE: -100, //optimization and dc energy limit on charts - upper limit
		Y_LIMIT_OPT_POSITIVE: 1000 //optimization and dc energy limit on charts - upper limit
	},
	POZNAN: {
		Y_LIMIT_MONITORING: 15, // upper limit for montoring
		Y_LIMIT_PRICE: 1,      //price limit in charts
		Y_LIMIT_OPT_NEGATIVE: -10, //optimization and dc energy limit on charts - upper limit
		Y_LIMIT_OPT_POSITIVE: 15 //optimization and dc energy limit on charts - upper limit
	},
	SBP: {
		Y_LIMIT_MONITORING: 1000, // upper limit for montoring
		Y_LIMIT_PRICE: 1,      //price limit in charts
		Y_LIMIT_OPT_NEGATIVE: -10, //optimization and dc energy limit on charts - upper limit
		Y_LIMIT_OPT_POSITIVE: 1000 //optimization and dc energy limit on charts - upper limit
	},
	DEFAULT: {
		Y_LIMIT_MONITORING: 200, // upper limit for montoring
		Y_LIMIT_PRICE: 3,      //price limit in charts
		Y_LIMIT_OPT_NEGATIVE: -200, //optimization and dc energy limit on charts - upper limit
		Y_LIMIT_OPT_POSITIVE: 250 //optimization and dc energy limit on charts - upper limit
	}
}
