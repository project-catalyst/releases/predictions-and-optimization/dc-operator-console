import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import Table from "../../commons/tables/table"
import * as API from "../api/metric-api";

const columns = [
    {
        Header: 'Timestamp',
        accessor: 'timestamp',
    },
    {
        Header: 'PUE',
        accessor: 'pue',
    },
    {
        Header: 'ERE',
        accessor: 'ere',
    },
    {
        Header: 'Reuse Percent',
        accessor: 'reusePercent',
    },
    {
        Header: 'Heat Adapt',
        accessor: 'heatAdapt',
    },
    {
        Header: 'Co2',
        accessor: 'co2',
    },
    {
        Header: 'DC Profit',
        accessor: 'dcProfit',
    }
];

const filters = [
    {
        accessor: 'timestamp',
    }
];

class MetricTable extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            tableData: [],
            actionsLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        API.fetchMetrics( (result, status, err) => {
            if (result !== null && status === 200) {
                console.log(result);
                let data  = [];
                result.forEach(x => {
                    let dt = {}
                    dt['timestamp'] = x[0].time
                    for(let i = 0; i < 6; i++)
                        dt[x[i].metric] = x[i].value
                    let date = new Date(dt.timestamp);
                    data.push({
                        timestamp: date.toDateString(),
                        pue: dt.PUE.toFixed(2),
                        ere: dt.ERE.toFixed(2),
                        reusePercent: dt.ReusePercent.toFixed(2),
                        heatAdapt: dt.HeatAdaptability.toFixed(2),
                        co2: dt.CO2emissions.toFixed(2),
                        dcProfit: dt.DCProfit.toFixed(2)
                    });
                });
                this.setState({
                    tableData: data,
                    actionsLoaded: true
                });
            }else{
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
                console.log(err);
            }
        });
    }

    render() {
        console.log(this.state.tableData);
        return (
            <div>
                {
                    this.state.actionsLoaded &&
                    <Table
                        data={this.state.tableData}
                        columns={columns}
                        search={filters}
                        pageSize={20}
                    />
                }
                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage
                        errorStatus={this.state.errorStatus}
                        error={this.state.error}
                    />
                }
            </div>
        )

    }
}


export default MetricTable
