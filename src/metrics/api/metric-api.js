import {HOST} from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const ENDPOINTS = {
    METRICS: HOST.CATALYST_DB_API + '/metric/all'
}

function fetchMetrics(callback) {
    console.log(ENDPOINTS.METRICS);
    let request = new Request(ENDPOINTS.METRICS, {
        method: 'GET'
    });
    RestApiClient.performRequest(request, callback);
}

export {
    ENDPOINTS, fetchMetrics
}
