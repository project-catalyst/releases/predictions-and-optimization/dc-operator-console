import React from 'react';
import MetricTable from "./components/metric-table";
import {Card, CardHeader} from "reactstrap";

class Metrics extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong > Datacenter > Metrics </strong>
                </CardHeader>
                <br/>
                <Card>
                    <MetricTable/>
                </Card>
            </div>
        );

    }
}


export default Metrics
