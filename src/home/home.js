import React from 'react';
import DefaultBackgroundImg from '../commons/images/catalyst_background.png';
import QarnotBackgroundImg from '../commons/images/QARNOT.jpg';
import PsmBackgroundImg from '../commons/images/PSM.jpg';
import PoznanBackgroundImg from '../commons/images/psnc.jpg';
import SbpBackgroundImg from '../commons/images/sbp.jpg';

import {Button, Card, CardBody, CardSubtitle, CardText, CardTitle, Col, Jumbotron, Row} from 'reactstrap';
import Redirect from "react-router-dom/Redirect";
import {CONFIG} from "../config";
import styles from "../commons/styles/catalyst-style.css";
import Slider from "react-rangeslider";
import Label from "reactstrap/es/Label";
import ScenarioCard from "../util-components/scenarios/scenario-card";
import Image from "react-bootstrap/Image";

const BackgroundImg = {
    DEFAULT: DefaultBackgroundImg,
    QARNOT: QarnotBackgroundImg,
    PSM: PsmBackgroundImg,
    POZNAN: PoznanBackgroundImg,
    SBP: SbpBackgroundImg
}

const HEIGHT = {
    DEFAULT: "320px",
    QARNOT: "900px",
    PSM: "795px",
    POZNAN: "731px",
    SBP: "720px"
}

const backgroundStyle = {
    backgroundPosition: 'center',
    float: "right",
    // align: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    //height: "100%",
    height: "320px",
    marginLeft: '5%',
    backgroundImage: `url(${BackgroundImg[CONFIG.PILOT]})`,
    // marginLeft: '170px',
    // marginTop: '30px'
};
const textStyle = {
    color: 'gray',
    bold: 'true',
    textAlign: 'justify'
};

const subTitleStyle = {
    color: '#009245',
    fontSize: '20px',
    bold: 'true'
};

const titleStyle = {
    color: '#009245',
    fontSize: '40px',
    bold: 'true'
};

const scenarioCardStyle = {
    alignSelf: 'center',
    width: "35rem",
    marginLeft: '5%',
    marginTop: "2.5%",
};

const descriptionCardStyle = {
    alignSelf: 'center',
    width: "80%",
    marginLeft: '5%',
    marginRight: '5%',
    marginTop: "1%",
};


const scenarioTypes = {
    scenario1: {
        scenarioId: 'electrical',
    },
    scenario2: {
        scenarioId: 'thermal',
    },
    scenario3: {
        scenarioId: 'optimization'
    }
};

const DESCRIPTION = {
    DEFAULT:
        <Card style={descriptionCardStyle}>
            <CardBody>
                <CardTitle style={titleStyle}>Catalyst</CardTitle>
                <CardSubtitle style={textStyle}> About the project</CardSubtitle>
            </CardBody>
            <CardBody>
                <CardText style={textStyle}> <strong>CATALYST</strong> aspires to turn data
                    centres
                    into
                    flexible multi-energy hubs,
                    which can sustain investments in renewable energy sources and energy
                    efficiency.
                    Leveraging on results of past projects, CATALYST will adapt, scale up,
                    deploy and validate an innovative technological and business framework that
                    enables data centres to offer a range of mutualized energy flexibility
                    services
                    to both electricity and heat grids, while simultaneously increasing their
                    own
                    resiliency to energy supply.
                    The adaptation and replication potential of the CATALYST solution will be
                    demonstrated by carrying out
                    four pilots in operational data centres, spanning through the full spectrum
                    of
                    data
                    centre types,
                    from fully distributed, High Performance Computing (HPC) to colocation and
                    legacy,
                    and architectures,
                    from large centralized versus decentralized micro data centres. </CardText>
            </CardBody>
        </Card>,
    QARNOT: <Card style={descriptionCardStyle}>
        <CardBody>
            <CardTitle style={titleStyle}>Qarnot</CardTitle>
            <CardSubtitle style={textStyle}> About the pilot</CardSubtitle>
        </CardBody>
        <CardBody>
            <CardText style={textStyle}> <strong>Qarnot</strong> DC is spread over large territories in the form of
                digital heaters proposed by QRN. By design the IT nodes are turned on only if there is a “need for
                heat”. Each heater is actually a QRN server featuring three remotely managed CPUs that could be switched
                on, one by one remotely. Also, the working conditions of the CPUs can be handled remotely to master
                actual CPU power consumption. The cooling is purely passive, done by a very large extruded aluminium
                heatsink and require absolutely no energy at all. Therefore, we consider that our distributed DC’s PUE
                is 1, by design. In addition, it is important to notice that all the energy consumed by each of the
                nodes is fully transformed into heat directly where it’s expected and useful. No additional energy is
                required at all for heat reuse purpose. Thanks to the large aluminium heatsink heat is stored in the
                heater and is naturally released for one hour in case servers are stopped. This characteristic could be
                turned into profit by using as heat buffer to reduce electrical consumption seamlessly for inhabitant
                comfort. Computing job, i.e. IT loads continuously running on our hardware is fully controlled by
                Qarnot. It is therefore possible to reduce electrical consumption for time period below one hour, with
                impact either on QoS or grid availability. In Qarnot’s model, electricity consumption and heat
                generation is perfectly and instantaneously correlated without any possibility to unlink. The software
                monitoring and managing the platform is called Qware. It is a custom made software which is mastering
                each computing node. Depending on clients, IT loads running have different priority: low meaning, when
                it could be killed instantly, medium meaning, if a proper shutdown has to be performed, and high meaning
                when the task cannot be stopped (unless force majeure). Qarnot has now about 5 major deployed sites
                throughout France, representing about 500kW of IT power. Qarnot is progressively deploying other edge
                site dedicated to hot air production (passively cooled) in large supermarkets. Qarnot is about to start
                IT computing boiler deployment in 2019 representing 100kW in 4 different sites </CardText>
        </CardBody>
    </Card>,
    PSM: <Card style={descriptionCardStyle}>
        <CardBody>
            <CardTitle style={titleStyle}>PSM: Green Pont Saint Martin DC</CardTitle>
            <CardSubtitle style={textStyle}> About the pilot</CardSubtitle>
        </CardBody>
        <CardBody>
            <CardText style={textStyle}> <strong>PSM</strong> Green Pont Saint Martin DC is one of the 4 Engineering DCs
                spread throughout Italy and grouped in geographical HUBs. This DC was built about 20 years ago, however
                it is still the most relevant ENG DC since it has been upgraded over the year, from both technological
                and security point of view. The PSM DC works as Server Farm with two control rooms called Network
                Operating Center (NOC) and Software Operation Center (SOC). In this DC, customers can host their
                software application and data exploiting the highest international standards of security (6 different
                levels of security access) and reliability. Moreover, even if the DC is located in a geographical area
                under a flood watch, the DC isn’t vulnerable to natural disasters. The trial site of the PSM pilot
                consists in the overall DC. As anticipated before, it is a colocation DC that offers hosting and
                outsourcing services to customers. For this reason, it is not suitable for the workload relocation
                scenario unless setting up a specific test bed. Regarding the DC Cooling system, an important
                refurbishment is planned within this year. The project involves the replacement of the main cooling
                system, consisting of three refrigeration units, one of which is a total reserve, which produces chilled
                water at a temperature of 7°C. This water is sent to the precision air conditioners located in each
                server room. The heat condensation of the refrigeration units is disposed of using ground water or
                alternatively by sending condensation water to the evaporation towers. </CardText>
        </CardBody>
    </Card>,
    POZNAN: <Card style={descriptionCardStyle}>
        <CardBody>
            <CardTitle style={titleStyle}>PSNC: Micro data centre with renewable energy supply testbed</CardTitle>
            <CardSubtitle style={textStyle}> About the pilot</CardSubtitle>
        </CardBody>
        <CardBody>
            <CardText style={textStyle}> <strong>PSNC</strong> micro data centre laboratory contains 2 racks with ~120
                server nodes (some of them being low power micro-servers). The IT equipment of the micro DC consumes
                approximately 10kW of power in a maximum load and about 3.5kW in idle state. It is a testbed environment
                the utilization varies frequently, so there are only a few, regular services running on the servers,
                thus most of the workload (around 90%) constitute tasks that can be easily shifted. The main systems
                installed on the servers are VMware, Ubuntu 17, Windows Server and Centos 7. Racks are connected to the
                photovoltaic system, which consists of 80 PV panels with 20kW peak power. Servers are directly connected
                to inverters so that they can be supplied directly by power grid, RES in the case of sufficient
                generation or by batteries (75kWh) if energy production is too low. If batteries are discharged below
                60%, servers immediately switch to power grid to extend battery life. The maximum load of the PV system
                by servers, due to electrical constraints, is limited to 7.3 kW. With fully charged batteries and no
                solar radiation, the battery could keep the completely loaded servers for 4 hours. In addition to the
                energy consumed by the servers themselves, a large amount is drained by cooling systems. The cooling
                system consumes up to 3.2kW with cooling capacity at 10 kW (maximum 11kW). Therefore, it is more than
                30% of additional power consumption that can be reduced. Temperature change in the server room from 18
                to 20 degrees Celsius takes about 15 minutes, while the cooling down of the room from 20 to 18 takes
                more than an hour. However, it is possible to save some amount of energy, if the air conditioning power
                is reduced during the low load and it is increased during the load increases. </CardText>
        </CardBody>
    </Card>,
    SBP: <Card style={descriptionCardStyle}>
        <CardBody>
            <CardTitle style={titleStyle}>SBP: Schuberg Philis DC</CardTitle>
            <CardSubtitle style={textStyle}> About the pilot</CardSubtitle>
        </CardBody>
        <CardBody>
            <CardText style={textStyle}> <strong>Schuberg Philis</strong> Datacentre is located in the Amsterdam area,
                The Netherlands, in direct vicinity of Amsterdam Schiphol Airport. The facility is a multi-tenant,
                carrier-neutral colocation datacentre. The building consists of data rooms, utility area and a large
                office space, hosting appr. 250 employees. Data rooms plus related utility area form a critical
                facility, the office part in principle is a non-critical facility (only the office bound control room is
                a critical, DC related part of the building). The datacentre is designed as a 2MW net IT facility,
                currently running at appr. 520kW net IT, being an annual almost flat line power demand on IT equipment.
            </CardText>
        </CardBody>
    </Card>
}

class Home extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedScenario: scenarioTypes.scenario1.scenarioId,
            loadButton: false,
            redirect: false,
        };
        this.routeChange = this.routeChange.bind(this);
        this.scenarioButtonClick = this.scenarioButtonClick.bind(this);
        this.loadButtonClick = this.loadButtonClick.bind(this);
    }

    scenarioButtonClick(e) {
        this.setState({
            selectedScenario: e.target.id,
        });

        if (e.target.id === 'electrical') {
            localStorage.setItem('scenario', '0');
            this.props.choseScenario1();
        } else if (e.target.id === 'thermal') {
            localStorage.setItem('scenario', '1');
            this.props.choseScenario2();
        } else if (e.target.id === 'optimization') {
            localStorage.setItem('scenario', '2');
            this.props.choseScenario3();
        }
        this.setState({
            redirect: true,
        })
    }

    componentDidMount() {
        if (!CONFIG.IS_PILOT) {
            this.props.choseScenario3();
        }
    }

    loadButtonClick() {
        if (this.state.selectedScenario === 'electrical') {
            localStorage.setItem('scenario', '0');
            this.props.chooseScenario1();
        } else if (this.state.selectedScenario === 'thermal') {
            localStorage.setItem('scenario', '1');
            this.props.chooseScenario2();
        } else if (this.state.selectedScenario === 'optimization') {
            localStorage.setItem('scenario', '2');
            this.props.chooseScenario3();
        }
        this.setState({
            redirect: true,
        })
    }

    routeChange() {
        let path = `newPath`;
        this.props.history.push(path);
    }

    renderRedirect = () => {
        let url = "/catalyst-dc-operator-console/electrical/monitoring";
        if (this.state.redirect) {
            return <Redirect to={url}/>
        }
        return null;
    };

    render() {
        return (
            <div>
                <Row>
                    <Col>
                        <Row>
                            {/*<Col>
                            <Jumbotron fluid style={backgroundStyle}>
                            </Jumbotron>
                            </Col>*/}
                            <Col></Col>
                            <Col xs={8}>
                                <Image style={backgroundStyle} src={BackgroundImg[CONFIG.PILOT]}/>
                            </Col>
                            <Col></Col>
                        </Row>
                        <Row>
                            {DESCRIPTION[CONFIG.PILOT]}
                        </Row>
                    </Col>
                </Row>
                <br/>
            </div>
        )
    };
}

export default Home


