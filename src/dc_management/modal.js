import React from 'react'
import APIResponseErrorMessage from '../commons/errorhandling/api-response-error-message'
import  {Modal, ModalBody, ModalHeader, ModalFooter} from 'reactstrap';

class Modall extends React.Component{
	constructor(props){
		super(props)
		this.closeModal = this.closeModal.bind(this);
		this.handleSave = this.handleSave.bind(this);
		this.inputChange = this.inputChange.bind(this);
		this.state = {
			data: this.props.data,
			showFunction: this.props.showFunction,
			emptyObject: null,
			searchInput: {}
		}
	}

	componentWillReceiveProps(nextProps) {
		console.log(nextProps.add_new_flag)
		console.log(nextProps.empty)
        this.setState({
            show : nextProps.show,
            data: nextProps.data,
            empty_object: nextProps.empty,
            e_class : nextProps.empty["@class"],
            searchInput: (!nextProps.add_new_flag) ? nextProps.data : nextProps.empty
        });
    }

    closeModal(){
    	this.state.showFunction(false)
    }

    titleHandler(e) {
        this.setState({ title: e.target.value });
    }

    msgHandler(e) {
        this.setState({ msg: e.target.value });
    }

    handleSave() {
    	console.log(this.state.empty_object)
	    var object = {}
	    
	    var input_obj = this.state.searchInput
	   
	    Object.keys(this.state.searchInput).map(key =>{ 
        	object[key] = input_obj[key]
	    })
	  
	  	object['@class'] = this.state.e_class
	   	if(this.props.add_new_flag)
	   		this.props.addNew(object)
	    else {
	    	object.deviceId = this.state.data.deviceId
	    	this.props.updateElement(object)
	    }
	    this.closeModal()
    }

    inputChange(e){
    	var key = e.currentTarget.getAttribute("input_key")
    	var obj = this.state.searchInput
    	obj[key] = e.target.value
    	this.setState({
    		searchInput : obj
    	})
    }


	render(){

		if (this.props.show == false || (this.state.data == null & this.props.add_new_flag == false))
			return null

		let data;
		if (this.props.add_new_flag)
			data = this.state.empty_object
		else data = this.state.data

		let modal_content = Object.keys(data).map(key => {
			let type = "number"
			if (key == "deviceLabel")
				type = ""
			if (key != "@class" & key != "deviceId")
			return (
			<tr>
  				<td>{key}:</td>
  				<td><input  class="form-control" id="input-textbox" input_key={key} 
  				type={type} onChange={this.inputChange} value={this.state.searchInput[key]} /></td>
        	 </tr>)
		});
		return(
		<div>
		<Modal isOpen={this.state.show} toggle={this.closeModal}  id="myModalDialog">
                    <ModalHeader toggle={this.closeModal} >
                    	<h4 class="modal-title">{(this.props.add_new_flag != true) ? this.state.data.deviceLabel : "Add New Component"}</h4>
                    </ModalHeader>
                    <ModalBody>
	        		
	        			<table class="table table-borderless">
	        			<tbody>
	        			{modal_content}
	        			</tbody>
						</table>
        			</ModalBody>
        			<ModalFooter>
          				<button type="button" class="btn btn-default" data-dismiss="modal" onClick={this.handleSave}>Save</button>
        			
          				<button type="button" class="btn btn-default" data-dismiss="modal" onClick={this.closeModal}>Close</button>
        			 </ModalFooter>
      			</Modal>
    		</div>
    
    	)

	}
}

export default Modall
