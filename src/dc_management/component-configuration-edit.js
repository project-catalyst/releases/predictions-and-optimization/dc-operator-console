import React from 'react'
import * as InsertAPI from "./api/dc-manager-api.js";
import { withRouter } from 'react-router-dom';
import Modall from './modal'
import * as help from "./dc_help.js"
import * as DCAPI from "./api/dc-manager-api"
import APIResponseErrorMessage from '../commons/errorhandling/api-response-error-message'
import {CardHeader} from "reactstrap";
class ComponentConfigurationEdit extends React.Component {

    constructor(props) {
        super(props);
        this.replaceModalItem = this.replaceModalItem.bind(this)
        this.changeModalShowState = this.changeModalShowState.bind(this)
        this.addNew = this.addNew.bind(this)
        this.addBattery = this.addBattery.bind(this)
        this.addNewElementToDb = this.addNewElementToDb.bind(this)
        this.addTes = this.addTes.bind(this)
        this.addCoolingSystem = this.addCoolingSystem.bind(this)
        this.addServerRoom = this.addServerRoom.bind(this)
        this.updateTes = this.updateTes.bind(this)
        this.updateCoolingSystem = this.updateCoolingSystem.bind(this)
        this.updateServerRoom = this.updateServerRoom.bind(this)
        this.updateBattery = this.updateBattery.bind(this)
        this.updateElementDb = this.updateElementDb.bind(this)
        this.deleteDevice = this.deleteDevice.bind(this)
        this.addHes = this.addHes.bind(this)
        this.updateHes = this.updateHes.bind(this)
        this.showError = this.showError.bind(this)
        var title = ""
        var dc_name = ""
        var components = []
        var empty = {}
        var func = null
        var obj_class = ""
        if (this.props.location.state != undefined){
          title = this.props.location.state.title
          dc_name = this.props.location.state.dc_name
          components = this.props.location.state.db_data.components
          empty = this.props.location.state.db_data.empty
        }
        
        this.state={
            title: title,
            components: components,
            requiredItem: -1,
            show: false,
            add_new_flag: false,
            empty: empty,
            dc_name: dc_name,
            class: obj_class,
            errorStatus: null,
            error : null,
            collapseForm : true      
          }
    }

    showError(response, stat, err){
      this.setState({
            errorStatus: stat,
            error : err,
            collapseForm : false
          })
      console.log("after show error")
      console.log(this.state)
    }

    replaceModalItem(index){
      this.setState({
        requiredItem: index,
        show : true
      })
    }

    changeModalShowState(state){
      this.setState({
        show: state,
        add_new_flag: false
      });
    }

    addNew(){
      this.setState({
        show : true,
        add_new_flag: true
      })

    }

    addNewElementToDb(element){
      const title = this.state.title
      if (title == "Battery")
        this.addBattery(element)
      else if (title == "Thermal Storage")
        this.addTes(element)
      else if (title == "Cooling System")
        this.addCoolingSystem(element)
      else if (title == "Server Room")
        this.addServerRoom(element)
      else if (title == "Heat Reuse System")
        this.addHes(element)
    }

    updateElementDb(element){
      const title = this.state.title
      if (title == "Battery")
        this.updateBattery(element)
      else if (title == "Thermal Storage")
        this.updateTes(element)
      else if (title == "Cooling System")
        this.updateCoolingSystem(element)
      else if (title == "Server Room")
        this.updateServerRoom(element)
      else if (title == "Heat Reuse System")
        this.updateHes(element)
    }

    deleteDevice(e){
      var index = e.currentTarget.getAttribute("index")
      var label = e.target.value
      DCAPI.deleteDevice({device_label: label}, (response, stat, err) => {
        if (response != null){
          var components = this.state.components
          components.splice(index, 1)
          this.setState({
            components: components
          })
        }
         else this.showError(response, stat, err)
      })
    }

    addBattery(element){
      DCAPI.addBattery({dc_name: this.state.dc_name, element: element}, (response, stat, err) => {
        if (response != null && stat === 200){
          var selectedFields = response.map(x => help.formatBatteryFields(x))
          this.setState({
            components: selectedFields
          })
        }
        else this.showError(response, stat, err)
      })
    }

    addTes(element){
      DCAPI.addTes({dc_name: this.state.dc_name, element: element}, (response, stat, err) => {
        if (response != null && stat === 200){
        var selectedFields = response.map(x => help.formatTesFields(x))
        this.setState({
          components: selectedFields
        })
        }
        else this.showError(response, stat, err)
      })
    }

    addServerRoom(element){
      console.log("add server r")
      DCAPI.addServerRoom({dc_name: this.state.dc_name, element: element}, (response, stat, err) => {
        if (response != null && stat === 200){
        var selectedFields = response.map(x => help.formatServerRoomFields(x))
        this.setState({
          components: selectedFields
        })
      }
     else this.showError(response, stat, err)
      })
    }


    addCoolingSystem(element){
      DCAPI.addCoolingSystem({dc_name: this.state.dc_name, element: element}, (response, stat, err) => {
        if(response != null  && stat === 200){
          var selectedFields = response.map(x => help.formatCoolingFields(x))
          this.setState({
            components: selectedFields
          })
        }
        else this.showError(response, stat, err)
      })
    }

    addHes(element){
      DCAPI.addHes({dc_name: this.state.dc_name, element: element}, (response, stat, err) => {
        if(response != null && stat === 200){
          var selectedFields = response.map(x => help.formatHesFields(x))
          this.setState({
            components: selectedFields
          })
        }
        else this.showError(response, stat, err)
      })
    }


    updateBattery(element){
      DCAPI.updateBattery({dc_name: this.state.dc_name, element: element}, (response, stat, err) => {
        if (response != null && stat === 200){
          var selectedFields = response.map(x => help.formatBatteryFields(x))
          this.setState({
            components: selectedFields
          })
        }
        else {this.showError(response, stat, err)
            console.log("ce pula")}
      })
    }

    updateTes(element){
      DCAPI.updateTes({dc_name: this.state.dc_name, element: element}, (response, stat, err) => {
        if(response != null && stat === 200){
        var selectedFields = response.map(x => help.formatTesFields(x))
        this.setState({
          components: selectedFields
        })
        }
        else this.showError(response, stat, err)
      })
    }

    updateHes(element){
      DCAPI.updateHes({dc_name: this.state.dc_name, element: element}, (response, stat, err) => {
        if(response != null && stat === 200){
        var selectedFields = response.map(x => help.formatHesFields(x))
        this.setState({
          components: selectedFields
        })
        }
        else this.showError(response, stat, err)
      })
    }

    updateServerRoom(element){
      console.log("add server r")
      DCAPI.updateServerRoom({dc_name: this.state.dc_name, element: element}, (response, stat, err) => {
        if(response != null && stat === 200){
        var selectedFields = response.map(x => help.formatServerRoomFields(x))
        this.setState({
          components: selectedFields
        })
        }
        else this.showError(response, stat, err)
      })
    }

    updateCoolingSystem(element){
      DCAPI.updateCoolingSystem({dc_name: this.state.dc_name, element: element}, (response, stat, err) => {
        if(response != null && stat === 200){
          var selectedFields = response.map(x => help.formatCoolingFields(x))
          this.setState({
            components: selectedFields
          })
        }
        else this.showError(response, stat, err)
      })
    }



    render() {

      let table_layout = this.state.components.map((e,index) => {
            return(
              <tr>
                <td> {help.formatString(e.deviceLabel)} </td>
                <td> <ul>{Object.keys(e).map(x =>{
                      if (x != "deviceLabel" & x != "deviceId" & x != "@class")
                      return (
                        <li> {x} : {e[x]} </li>
                      );
                        })} </ul></td>
                  <td><button class="btn btn-light"  id="table-btn"
                  onClick ={() => this.replaceModalItem(index)}
                  data-toggle="modal"
                  data-target="#myModal">Edit</button></td>
                  <td><button class="btn btn-light"  id="table-btn" value={e.deviceLabel} index={index} onClick={this.deleteDevice}>Delete</button></td>
              </tr>
              )
      });

    const requiredItem = this.state.requiredItem;
    let modalData = Object.assign({}, this.state.components[requiredItem]);
    var empty_clone = Object.assign({}, this.state.empty);
        return (
            <div class="main-container">

                <CardHeader>
                    <strong > Admin Panel > Datacenter  {this.state.title}</strong>
                </CardHeader>


                <table class="table table-bordered manage-table">
                  <tbody>
                    <tr class="bg-success">
                     <td colspan="5" id="td-colspan-title">{this.state.title} Configuration</td>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-bordered manage-table">
                  <thead>
                    <tr>
                      <th scope="col">{this.state.title}</th>
                      <th scope="col">Properties</th>
                      <th scope="col">Edit</th>
                      <th scope="col">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    {table_layout}
                  </tbody>
                </table>
                <button class="btn btn-success" id="add_new_component" onClick={this.addNew}>Add new {this.state.title}</button>
              <Modall
                addNew = {this.addNewElementToDb}
                updateElement={this.updateElementDb}
                showFunction={this.changeModalShowState}
                show={this.state.show}
                data={modalData}
                add_new_flag={this.state.add_new_flag}
                empty = {empty_clone}
                />
             {this.state.errorStatus > 0 && <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}  />}
            </div>


        )
    };
}

export default withRouter(ComponentConfigurationEdit)