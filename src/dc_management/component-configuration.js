import React from 'react'
import * as DCManagerAPI from "./api/dc-manager-api.js";
import { withRouter } from 'react-router-dom';
import * as help from "./dc_help.js"
import {CardHeader} from "reactstrap";

class ComponentConfiguration extends React.Component {

    constructor(props) {
        super(props);

        this.getServerRoomDetails = this.getServerRoomDetails.bind(this);
        this.getCoolingSystem = this.getCoolingSystem.bind(this);
        this.getThermalStorage = this.getThermalStorage.bind(this);
        this.getHeatReuse = this.getHeatReuse.bind(this);
        this.getUPS = this.getUPS.bind(this);
        this.redirectToEditPage = this.redirectToEditPage.bind(this);
        this.getEmpty = this.getEmpty.bind(this)
        this.state = {
            components:[
                      {name: "Server Room", data: []},
                      {name: "Cooling System", data: []},
                      {name: "Thermal Storage", data: []},
                      {name: "Heat Reuse System", data: []},
                      {name:"Battery", text_data: [], addFunction: this.addBattery}],
            empty: [{}, {}, {}, {}, {}],
            dc_name: (this.props.location.state !== undefined) ? this.props.location.state.dc_name : "",
            server_room_text: "",
            server_names: [],
            cooling_names: [],
            thermal_names: [],
            heat_names: [],
            baterry_names: []
        }

        this.getServerRoomDetails()
        this.getUPS()
        this.getHeatReuse()
        this.getThermalStorage()
        this.getCoolingSystem()
        this.getEmpty()
    }

    redirectToEditPage(e){
      var element = this.state.components[e.target.value]
      const cache = {}
      cache["components"] = element.data
      cache["empty"] = this.state.empty[e.target.value]
      console.log(cache)
      let url = "/catalyst-dc-operator-console/component-configuration-edit";
      this.props.history.push({
        pathname: url,
        state:{
          title: element.name,
          dc_name: this.state.dc_name,
          db_data: cache
        }});
    }

    getEmpty(){
      DCManagerAPI.getEmpty((response, state, err) =>{
        console.log("get empty")
        console.log(response)
        var empty_server = help.formatServerRoomFields(response[0])
        empty_server["@class"] = response[0]["@class"]
        
        var empty_cooling = help.formatCoolingFields(response[1])
        empty_cooling["@class"] = response[1]["@class"]
        
        var empty_thermal = help.formatTesFields(response[2])
        empty_thermal["@class"] = response[2]["@class"]
        
        var empty_heat = help.formatHesFields(response[3])
        empty_heat["@class"] = response[3]["@class"]
        var empty_battery = help.formatBatteryFields(response[4])
        empty_battery["@class"] = response[4]["@class"]
        var empty = [empty_server, empty_cooling, empty_thermal, empty_heat, empty_battery]
        console.log(empty)
        this.setState({
          empty: empty
        })

      })
    }

    getUPS(){
      DCManagerAPI.getBatteryDetails({dc_name: this.state.dc_name, time: Date.now()}, (response, status, err) =>{
          if (response != null){
            var names = response.map(e => e.deviceLabel)
            var res = response.map(x => help.formatBatteryFields(x))

            var components = this.state.components
            components[4].data = res

            this.setState({
              baterry_names: names,
              components: components

            });
          }
        })
    }

    getHeatReuse(){
        DCManagerAPI.getHesDetails({dc_name: this.state.dc_name, time: Date.now()}, (response, status, err) =>{
          if (response != null){
            var names = response.map(e => e.deviceLabel)
            var res = response.map(x => help.formatHesFields(x))

            var components = this.state.components
            components[3].data = res

            this.setState({
              heat_names: names,
              components: components
            });
          }
        })
        
    }

    getThermalStorage(){
        DCManagerAPI.getTesDetails({dc_name: this.state.dc_name, time: Date.now()}, (response, status, err) =>{
          if (response != null){
            var names = response.map(e => e.deviceLabel)
            var res = response.map(x => help.formatTesFields(x))
            var components = this.state.components
            components[2].data = res            
            this.setState({
              thermal_names: names,
              components: components

            });
          }
        })
    }

    getCoolingSystem(){
      DCManagerAPI.getCoolingSystemDetails({dc_name: this.state.dc_name, time: Date.now()}, (response, status, err) =>{
          if (response != null){
            var names = response.map(e => e.deviceLabel)
            var res = response.map(r => help.formatCoolingFields(r))
            var components = this.state.components
            components[1].data = res
           
            
            this.setState({
              cooling_names: names,
              components: components
            });
          }
        })
    }

    getServerRoomDetails(){
        DCManagerAPI.getServerRoomDetails({dc_name: this.state.dc_name, time: Date.now()}, (response, status, err) =>{
          if (response != null){
            var names = response.map(e => e.deviceLabel)
            var res = response.map(x => help.formatServerRoomFields(x))
            var components = this.state.components
            components[0].data = res
           
            this.setState({
              server_names: names,
              components: components
            });
          }
        })
    }



    render() {

      let top_table = this.state.components.map((e, index) => {
              return (
                <td id="catchy-td">
                  <button type="button" class="btn btn-outline-success" id="table-btn-components" 
                  onClick={this.redirectToEditPage} 
                  value={index}>
                    {e.name}
                  </button>
                </td>
                );
      })

        return (
            <div class="main-container">
                <CardHeader>
                    <strong > Admin Panel > Datacenter Components</strong>
                </CardHeader>

                <table class="table table-bordered manage-table">
                  <tbody>
                    <tr class="bg-success">
                     <td colspan="5" id="td-colspan-title">{this.state.dc_name}</td>
                    </tr>
                    <tr>
                      {top_table}
                    </tr>
                  </tbody>
                </table>

                <table class="table table-bordered manage-table">
                  <thead>
                    <tr>
                      <th scope="col">Component</th>
                      <th scope="col">Properties</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    <tr>
                      <td>Server Room</td>
                      <td><ul>{this.state.server_names.map(x => <li>{help.formatString(x)}</li>)}</ul></td>
                      </tr><tr>
                      <td>Cooling System</td>
                      <td><ul>{this.state.cooling_names.map(x => <li>{help.formatString(x)}</li>)}</ul></td>
                      </tr><tr>
                      <td>Thermal Storage</td>
                      <td><ul>{this.state.thermal_names.map(x => <li>{help.formatString(x)}</li>)}</ul></td>
                      </tr><tr>
                      <td>Heat Reuse</td>
                      <td><ul>{this.state.heat_names.map(x => <li>{help.formatString(x)}</li>)}</ul></td>
                      </tr><tr>
                      <td>Battery</td>
                      <td><ul>{this.state.baterry_names.map(x => <li>{help.formatString(x)}</li>)}</ul></td>
                      </tr>
                  </tbody>
                </table>

            </div>
        )
    };
}

export default withRouter(ComponentConfiguration);