import {HOST}  from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const ENDPOINT = {

    DEV:'/device/all',
    RESSOURCES: '/resources',
    SERVER : "/server-room",
    COOLING: "/cooling-system",
    DC:"/datacenter",
    TES: "/tes",
    BATTERY: "/battery",
    DATACENTERS: "/datacenters",
    HES: "/heat-recovery-system",
    USER_DC: "/user_datacenter"
};

function getAllDatacenters(data, callback){
	let unix = Math.floor(Date.now() / 100)
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.DATACENTERS + "/" + Date.now(), {method: 'GET'})
	RestApiClient.performRequest(request, callback);
}

function getAllUserDatacenters(data, callback){
	console.log("getAllDatacenters2")
	let unix = Math.floor(Date.now() / 100)
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + "/user_datacenters/" + Date.now() + "/" + data.username, {method: 'GET'})
	RestApiClient.performRequest(request, callback);
}


function deleteDevice(data, callback){
	
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + "/" + data.device_label, {method: 'DELETE'})
	RestApiClient.performRequest(request, callback);
}

function addDataCenter(data, callback){
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.USER_DC + "/" + data.user_id, 
		{
			method: 'POST', 
			headers: {
            	'Accept': 'application/json',
            	'Content-Type': 'application/json',
        	},
	        body: JSON.stringify({
	        	label: data.name
	        })
	    })
	RestApiClient.performRequest(request, callback);
}

function getServerRoomDetails(data, callback){
	console.log(data)
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.SERVER+ "/" + data.dc_name + "/" + data.time);
	console.log(request.url)
	RestApiClient.performRequest(request, callback);
}

function addServerRoom(data, callback){
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.SERVER+ "/" + data.dc_name , 
		{
			method: 'POST', 
			headers: {
            	'Accept': 'application/json',
            	'Content-Type': 'application/json',
        	},
	        body: JSON.stringify(data.element)
	       
	    })
	RestApiClient.performRequest(request, callback);
}

function updateServerRoom(data, callback){
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.SERVER+ "/" + data.dc_name , 
		{
			method: 'PUT', 
			headers: {
            	'Accept': 'application/json',
            	'Content-Type': 'application/json',
        	},
	        body: JSON.stringify(data.element)
	       
	    })
	RestApiClient.performRequest(request, callback);
}

function getCoolingSystemDetails(data, callback){
	console.log(data)
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.COOLING+ "/" + data.dc_name + "/" + data.time);
	console.log(request.url)
	RestApiClient.performRequest(request, callback);
}

function addCoolingSystem(data, callback){
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.COOLING+ "/" + data.dc_name , 
		{
			method: 'POST', 
			headers: {
            	'Accept': 'application/json',
            	'Content-Type': 'application/json',
        	},
	        body: JSON.stringify(data.element)
	       
	    })
	RestApiClient.performRequest(request, callback);
}

function updateCoolingSystem(data, callback){
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.COOLING+ "/" + data.dc_name , 
		{
			method: 'PUT', 
			headers: {
            	'Accept': 'application/json',
            	'Content-Type': 'application/json',
        	},
	        body: JSON.stringify(data.element)
	       
	    })
	RestApiClient.performRequest(request, callback);
}

function getDCDetails(data, callback){
	console.log(data)
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.DC+ "/" + data.dc_name + "/" + data.time);
	console.log(request.url)
	RestApiClient.performRequest(request, callback);
}

function getTesDetails(data, callback){
	console.log(data)
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.TES+ "/" + data.dc_name + "/" + data.time);
	console.log(request.url)
	RestApiClient.performRequest(request, callback);
}

function addTes(data, callback){
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.TES+ "/" + data.dc_name , 
		{
			method: 'POST', 
			headers: {
            	'Accept': 'application/json',
            	'Content-Type': 'application/json',
        	},
	        body: JSON.stringify(data.element)
	       
	    })
	RestApiClient.performRequest(request, callback);
}

function updateTes(data, callback){
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.TES+ "/" + data.dc_name , 
		{
			method: 'PUT', 
			headers: {
            	'Accept': 'application/json',
            	'Content-Type': 'application/json',
        	},
	        body: JSON.stringify(data.element)
	       
	    })
	RestApiClient.performRequest(request, callback);
}

function getBatteryDetails(data, callback){
	console.log(data)
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.BATTERY+ "/" + data.dc_name + "/" + data.time);
	console.log(request.url)
	RestApiClient.performRequest(request, callback);
}

function addBattery(data, callback){
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.BATTERY+ "/" + data.dc_name , 
		{
			method: 'POST', 
			headers: {
            	'Accept': 'application/json',
            	'Content-Type': 'application/json',
        	},
	        body: JSON.stringify(data.element)
	       
	    })
	RestApiClient.performRequest(request, callback);
}

function updateBattery(data, callback){
	console.log("inside uuuuuuuuuuuuuuuuuuuuupdate")
	console.log(data.element)

	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.BATTERY+ "/" + data.dc_name , 
		{
			method: 'PUT', 
			headers: {
            	'Accept': 'application/json',
            	'Content-Type': 'application/json',
        	},
	        body: JSON.stringify(data.element)
	       
	    })
	console.log(request.url)
	RestApiClient.performRequest(request, callback);
}

function getHesDetails(data, callback){
	console.log(data)
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.HES+ "/" + data.dc_name + "/" + data.time);
	console.log(request.url)
	RestApiClient.performRequest(request, callback);
}

function addHes(data, callback){
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.HES+ "/" + data.dc_name , 
		{
			method: 'POST', 
			headers: {
            	'Accept': 'application/json',
            	'Content-Type': 'application/json',
        	},
	        body: JSON.stringify(data.element)
	       
	    })
	RestApiClient.performRequest(request, callback);
}

function updateHes(data, callback){
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + ENDPOINT.HES+ "/" + data.dc_name , 
		{
			method: 'PUT', 
			headers: {
            	'Accept': 'application/json',
            	'Content-Type': 'application/json',
        	},
	        body: JSON.stringify(data.element)
	       
	    })
	RestApiClient.performRequest(request, callback);
}

function getEmpty(callback){
	var request = new Request(HOST.CATALYST_DB_API + ENDPOINT.RESSOURCES + "/empty");
	console.log(request.url)
	RestApiClient.performRequest(request, callback);
}

export{
	getAllUserDatacenters,
	getAllDatacenters,
	getServerRoomDetails,
	getCoolingSystemDetails,
	getDCDetails,
	getTesDetails,
	getBatteryDetails,
	addBattery,
	addDataCenter,
	addTes,
	addCoolingSystem,
	addServerRoom,
	getEmpty,
	updateBattery,
	updateTes,
	updateCoolingSystem,
	updateServerRoom,
	deleteDevice,
	getHesDetails,
	updateHes,
	addHes,
}