function formatString(str){
      str = str.toLowerCase()
      var s_arr = str.split("_")
      str = s_arr.map(x => x.charAt(0).toUpperCase() + x.slice(1, ))
      var res = ""
      for (var e in str)
      	res = res + str[e] + " "
      return res
}

function formatBatteryFields(battery){
	var res = (({deviceLabel, maximumCapacity, actualLoadedCapacity, maxChargeRate, 
										maxDischargeRate, chargeLossRate, dischargeLossRate, esdFactor, dod, deviceId})=>({
               							deviceLabel, maximumCapacity , actualLoadedCapacity, maxChargeRate, maxDischargeRate,
               							chargeLossRate, dischargeLossRate, esdFactor, dod, deviceId}))(battery)
	return res            
}

function formatTesFields(tes){
	var res = (({deviceLabel, maximumCapacity, actualLoadedCapacity, maxChargeRate, maxDischargeRate, chargeLossRate, 
							dischargeLossRate, tesFactor, deviceId})=>({
              				deviceLabel, maximumCapacity , actualLoadedCapacity, maxChargeRate, maxDischargeRate, chargeLossRate, 
              				dischargeLossRate, tesFactor, deviceId}))(tes)  
	return res            
}

function formatCoolingFields(tes){
	var res = (({deviceLabel, maxCoolingLoadKWh, copC, copH, energyConsumption, coolingCapacityKWh, deviceId})=>({deviceLabel, maxCoolingLoadKWh, copC, 
							copH, energyConsumption, coolingCapacityKWh, deviceId}))(tes)
	return res            
}

function formatServerRoomFields(tes){
	var res = (({deviceLabel, maxHostLoad, maxReallocLoad, maxEnergyConsumption, edPercentage, deviceId })=>
					({deviceLabel, maxHostLoad, maxReallocLoad, maxEnergyConsumption, edPercentage, deviceId}))(tes)
	return res            
}


function formatHesFields(tes){
	var res = (({deviceLabel,  maxHeatLoadKWh , energyConsumption, heatCapacityKWh, copH, deviceId})=>
					({deviceLabel,  maxHeatLoadKWh , energyConsumption, heatCapacityKWh, copH, deviceId}))(tes)
	
	return res            
}




export{
	formatString,
	formatBatteryFields,
	formatTesFields,
	formatCoolingFields,
	formatServerRoomFields,
	formatHesFields
}
