import React from 'react'
import {Card, CardHeader, Col, Row} from "reactstrap";
import * as DCAPI from "./api/dc-manager-api.js";
import * as help from "./dc_help.js"
import {withRouter} from 'react-router-dom';
import { Alert } from 'reactstrap';
import APIResponseErrorMessage from '../commons/errorhandling/api-response-error-message'

class Datacenters extends React.Component {

    constructor(props) {
        super(props);
        this.getDataCenters = this.getDataCenters.bind(this)
        this.addDataCenter = this.addDataCenter.bind(this)
        this.searchInput = React.createRef();
        this.editEvent = this.editEvent.bind(this)
        this.deleteDevice = this.deleteDevice.bind(this)
        this.showError = this.showError.bind(this)
        this.state = {
          datacenters : [],
          errorStatus: null,
          error : null,
          collapseForm : true     
        }
        this.getDataCenters()
    }

     showError(response, stat, err){
      this.setState({
            errorStatus: stat,
            error : err,
            collapseForm : false
          })
    }

    deleteDevice(e){
      var index = e.currentTarget.getAttribute("index")
      var label = e.target.value
      DCAPI.deleteDevice({device_label: label}, (response, stat, err) => {
        if (response != null && stat === 200){
          var components = this.state.datacenters
          components.splice(index, 1)
          this.setState({
            datacenters: components
          })
        }
        else this.showError(response, stat, err)
      })
    }
    
    getDataCenters(){
      DCAPI.getAllUserDatacenters({username: localStorage.getItem("username")}, (response, status, err) => {
        if (status === 200 && response != null){
          console.log("intra")
          var table_data = response.map(dc => {
            var a = {}
            a.id = dc.idDataCentre
            a.name = dc.label
            return a
          })
          this.setState({
            datacenters: table_data
          })
        }
        else console.log("crapa")
      });
    }

    addDataCenter(){
      if (localStorage.getItem("user_id") != null){
        var input = this.searchInput.value
        DCAPI.addDataCenter({name: input, user_id : localStorage.getItem("user_id")}, (response, stat,err) =>{
          if (response != null && stat === 200){
          this.getDataCenters()
          this.setState({
            input_data: ""
          })
        }
        else this.showError(response, stat, err)
        })
         this.searchInput.value = ""
      }
    }

    editEvent(e){
      var element = e.target.value
      let url = "/catalyst-dc-operator-console/component-configuration";
      this.props.history.push({
        pathname: url,
        state:{
          dc_name: element
        }});
    }

    render() {
        let table_content = this.state.datacenters.map((x, index) =>{
          return (
            <tr key={x.name}>
              <th scope="row">{x.id}</th>
              <td>{help.formatString(x.name)}</td>
              <td><button class="btn btn-light" onClick={this.editEvent} id="table-btn" value={x.name}>Edit</button></td>
              <td><button class="btn btn-light"  id="table-btn" value={x.name} index={index}
                  onClick={this.deleteDevice}>Delete</button></td>
            </tr>
            )
        })

        return (
            <div class="main-container">
                <CardHeader>
                    <strong > Admin Panel > Datacenter Configuration</strong>
                </CardHeader>
                <br/>
                {/*<div>*/}
                {/*        <input  class="form-control" id="input-textbox" ref={el => this.searchInput = el}  placeholder="DC Name"/>*/}
                {/*        <button class="btn btn-success" id="align-input-btn" onClick={this.addDataCenter}>Create New DC</button>*/}
                {/*</div>*/}
                <Card>
                <table class="table table-bordered manage-table">
                  <thead>
                    <tr>
                      <th scope="col">Id</th>
                      <th scope="col">Name</th>
                      <th scope="col">Edit</th>
                      <th scope="col">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    {table_content}
                  </tbody>
                </table>
                </Card>
                 {this.state.errorStatus > 0 && <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}  />}
            </div>
        )
    };
}

export default withRouter(Datacenters)