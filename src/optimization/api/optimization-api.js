import {HOST} from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const ENDPOINTS = {
    OPTIMIZED: HOST.CATALYST_DB_API + '/execution/optimized/',
    UNOPTIMIZED: HOST.CATALYST_DB_API + '/execution/non-optimized/',
    METRICS: HOST.CATALYST_DB_API + '/metric/',
    OPTIMIZATION_ACTIONS: HOST.CATALYST_DB_API +'/optimization/actions',
    OPTIMIZATION_PLANS: HOST.CATALYST_DB_API +'/plan/',
    OPTIMIZATION_STRATEGIES: HOST.CATALYST_DB_API +'/optimization/strategies/dayahead/',
};


/**
 * TODO: change code on backend
 */
function fetchMetrics(timeframe, hour, time, callback) {
    let request = new Request(HOST.CATALYST_DB_API + ENDPOINTS.METRICS +"/"+ timeframe+ "/"+hour+"/" +time, {
        method: 'GET'
    });
    RestApiClient.performRequest(request, callback);
}


function fetchOptimizationActions(timeframe, hour, time, callback) {
    let request = new Request(ENDPOINTS.OPTIMIZATION_ACTIONS+"/"+ timeframe+ "/"+hour+"/" +time, {
        method: 'GET'
    });
    RestApiClient.performRequest(request, callback);
}

function fetchOptimizationStrategies( time,confidenceLevel, callback) {
    let request = new Request(ENDPOINTS.OPTIMIZATION_STRATEGIES+"/" +time+ "/"+confidenceLevel, {
        method: 'GET'
    });
    RestApiClient.performRequest(request, callback);
}


function fetchOptimizationPlans(strategyType, timeframe, hour, time,confidenceLevel, callback) {
    let request = new Request(ENDPOINTS.OPTIMIZATION_PLANS+"/"+ strategyType+"/"+ timeframe+"/" +time+ "/"+confidenceLevel+"/" +hour +",00", {
        method: 'GET'
    });
    RestApiClient.performRequest(request, callback);
}

function fetchOptimizedExecutionValues(strategyType,timeframe, time,hour, callback) {
    let request = new Request(ENDPOINTS.OPTIMIZED + strategyType+"/"+ timeframe+"/" + time + "/" + hour + ",00", {
        method: 'GET'
    });
    RestApiClient.performRequest(request, callback);
}

function fetchUnoptimizedExecutionValues(strategyType,timeframe, time,hour, callback) {
    let request = new Request(ENDPOINTS.UNOPTIMIZED + strategyType+"/"+ timeframe+"/" + time + "/" + hour + ",00", {
        method: 'GET'
    });
    RestApiClient.performRequest(request, callback);
}

export {
      fetchOptimizationActions,fetchOptimizationPlans,fetchOptimizationStrategies,fetchUnoptimizedExecutionValues, fetchOptimizedExecutionValues,
    ENDPOINTS}
