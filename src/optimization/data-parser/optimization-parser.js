function parseRT(profiles, data, key, startHour) {
    let formattedData = [];
    for (let h = 0; h < data.length; h++) {
        let item = {};
        let hour = Math.floor(h / 12) + Math.floor(startHour);
        let minutes = h % 12 * 5 + Math.floor((startHour - Math.floor(startHour)) * 100);
        let zero = (minutes % 60) < 10 ? "0":"";
        item[key] = hour + Math.floor(minutes / 60) + ":" + zero + minutes % 60;
        for (let labelIndex = 0; labelIndex < profiles.length; labelIndex++) {
            let accessor = profiles[labelIndex].accessor;
            let label = profiles[labelIndex].key;
            if (data[accessor] === null || data[accessor] === undefined) {
                item[label] = null;
            } else {
                let value = data[accessor][h];
                if (value === undefined) {
                    item[label] = null;
                } else {
                    item[label] = Math.trunc(value * 100) / 100;
                }
            }
        }
        formattedData.push(item);
    }
    return formattedData;
}

function parseDayAhead(profiles, data, key, startHour) {
    let formattedData = [];
    for (let h = 0; h < data.length; h++) {
        let item = {};
        item[key] = h+ Number(startHour);
        for (let labelIndex = 0; labelIndex < profiles.length; labelIndex++) {
            let accessor = profiles[labelIndex].accessor;
            let label = profiles[labelIndex].key;
            if (data[accessor] === null || data[accessor] === undefined) {
                item[label] = null;
            }else {
                let value = data[accessor][h];

                if (value === undefined) {
                    item[label] = null;
                } else {
                    item[label] = Math.trunc(value * 100) / 100;
                }
            }
        }
        formattedData.push(item);
    }
    return formattedData;
}

function formatData(data, step, size) {
    let formattedData = {};

    for(let key of Object.keys(data)) {
        if(data[key] == null)
            continue;
        formattedData[key] = new Array(size).fill(null);
        if (step < 24) {
            for (let i = 0; i < step; i++)
                formattedData[key][i] = data[key][i];
        } else {
            for (let i = 0; i < size; i++)
                formattedData[key][i] = data[key][i + step - 24];
        }
    }

    formattedData.length = 24;
    return formattedData;
}

function subtractRenewable(data) {
    for(let i = 0; i < data.length; i++) {
        data[i]["Provided Grid Energy"] -= data[i]["Renewable Energy"];
        /*if (data[i]["Provided Grid Energy"] < 0) {
            data[i]["Renewable Energy"] += data[i]["Provided Grid Energy"];
            data[i]["Provided Grid Energy"] = 0;
        }*/
    }
    return data;
}

module.exports = {
    parseRT, parseDayAhead, formatData, subtractRenewable
};

/*

======================================= INPUT FORMAT (JSON) =======================================
{
"consumptionDTValues": [305.68, 305.68, 308.75, 305.68,....],
"consumptionRTValues": [713.27, 713.27, 720.4, 713.27, 720.43,...]
.....
],

======================================= OUTPUT FORMAT (for Chart)=======================================
 [
        { hour: 00:00, "Consumption DT Values": 305.68, "Consumption RT Values": 713.27 },
        { hour: 00:05, "Consumption DT Values": 305.68, "Consumption RT Values": 713.27 },
        { hour: 00:10, "Consumption DT Values": 308.75, "Consumption RT Values": 720.4},
        { hour: 00:15, "Consumption DT Values": 305.68, "Consumption RT Values": 713.27 },
        ....]
 */

