import React from 'react';

import {Card, CardHeader, Col, Row} from 'reactstrap';
import APIResponseErrorMessage from "../../../commons/errorhandling/api-response-error-message";
import * as config from "../../../config";
import DatePicker from "../../../util-components/date-picker";
import ScenarioPicker from "../../../util-components/scenarios/scenario-picker";
import Redirect from "react-router-dom/Redirect";
import {TIMEFRAME} from "../../../util-components/timeframe-enums";
import PriceDrivenPlanComponent from "./components/price-driven-plan-component";

class PriceDrivenDaPlanContainer extends React.Component {
    constructor(props) {
        super(props);
        this.handleDayChange = this.handleDayChange.bind(this);
        this.handleScenarioChange = this.handleScenarioChange.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);
        this.state = {
            time: Date.now(),
            timeframe: TIMEFRAME.DAYAHEAD,
            strategyType: "price-driven",
            confidenceLevel: 0,
            hour: 0,
            redirect: false,
            nextRoute: "/catalyst-dc-operator-console/optimization/plan/id/price-driven"
        };
    }

    handleDayChange(e) {
        this.setState({
            time: e.getTime()
        });
    }


    handleScenarioChange(time) {
        this.setState({
            time: time,
        });
    }

    handleRedirect() {
        localStorage.setItem("intraday-start-hour", "0");
        localStorage.setItem("intraday-end-hour", "0");
        let url = this.state.nextRoute;
        if (!config.CONFIG.INTRADAY_ENABLED) {
            url = (config.CONFIG.IS_PILOT) ? "/catalyst-dc-operator-console/optimization/price-driven/execution" :
                "/catalyst-dc-operator-console/optimization/price-driven/execution-scenario";
        }
        localStorage.setItem('time', this.state.time);
        this.setState({
            redirect: true,
            nextRoute: url
        });
    };

    render() {
        return (
            <div>

                {this.state.redirect && <Redirect to={this.state.nextRoute}/>}
                <div>
                    {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
                    <CardHeader>
                        <strong> Optimization > Price Driven Plan</strong>
                    </CardHeader>
                    <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: 'auto', offset: 1}}>
                            {config.CONFIG.IS_PILOT &&
                            <DatePicker handler={this.handleDayChange}/>}
                            {!config.CONFIG.IS_PILOT &&
                            <ScenarioPicker handler={this.handleScenarioChange}/>}
                        </Col>
                    </Row>

                    <PriceDrivenPlanComponent key={this.state.time}
                                              time={this.state.time}
                                              timeframe={this.state.timeframe}
                                              strategyType={this.state.strategyType}
                                              confidenceLevel={this.state.confidenceLevel}
                                              hour={this.state.hour}
                                              redirectHandler={this.handleRedirect}/>
                    </Card>
                </div>

            </div>
        )
    }
}

export default PriceDrivenDaPlanContainer