import React from 'react';

import * as config from "../../../config";
import Redirect from "react-router-dom/Redirect";
import PriceDrivenPlanComponent from "./components/price-driven-plan-component";
import {Card, CardHeader} from "reactstrap";
import {TIMEFRAME} from "../../../util-components/timeframe-enums";


class PriceDrivenIdPlanContainer extends React.Component {

    constructor(props) {
        super(props);
        this.handleRedirect = this.handleRedirect.bind(this);
        this.state = {
            time: localStorage.getItem('time'),
            timeframe: TIMEFRAME.INTRADAY,
            strategyType: "price-driven",
            confidenceLevel: 0,
            hour: localStorage.getItem("intraday-end-hour"),
            redirect: false,
            nextRoute: "/catalyst-dc-operator-console/optimization/price-driven/execution"
        };
    }


    handleRedirect() {
        let endID = Number(this.state.hour) + 4;
        localStorage.setItem("intraday-start-hour", this.state.hour);
        localStorage.setItem("intraday-end-hour", endID + "");
        let url = (config.CONFIG.IS_PILOT) ? "/catalyst-dc-operator-console/optimization/price-driven/execution" :
            "/catalyst-dc-operator-console/optimization/price-driven/execution-scenario";
        this.setState({
            redirect: true,
            nextRoute: url
        });
    };

    render() {
        return (
            <div>
                {this.state.redirect && <Redirect to={this.state.nextRoute}/>}
                <div>
                    <CardHeader>
                        <strong> Optimization > Price Driven Intraday Plan</strong>
                    </CardHeader>
                    <br/><br/>
                    <Card>
                        <PriceDrivenPlanComponent key={this.state.hour}
                                                  time={this.state.time}
                                                  timeframe={this.state.timeframe}
                                                  strategyType={this.state.strategyType}
                                                  confidenceLevel={this.state.confidenceLevel}
                                                  hour={this.state.hour}
                                                  redirectHandler={this.handleRedirect}/>
                    </Card>
                </div>

            </div>
        )
    }
}

export default PriceDrivenIdPlanContainer;
