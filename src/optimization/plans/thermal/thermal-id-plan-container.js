import React from 'react';

import * as config from "../../../config";
import Redirect from "react-router-dom/Redirect";
import ThermalPlanComponent from "./components/thermal-plan-component";
import {Card, CardHeader} from "reactstrap";
import {TIMEFRAME} from "../../../util-components/timeframe-enums";


class ThermalIdPlanContainer extends React.Component {

    constructor(props) {
        super(props);
        this.handleRedirect = this.handleRedirect.bind(this);
        this.state = {
            time: localStorage.getItem('time'),
            timeframe: TIMEFRAME.INTRADAY,
            strategyType: "thermal",
            confidenceLevel: 0,
            hour: localStorage.getItem("intraday-end-hour"),
            redirect: false,
            nextRoute: "/catalyst-dc-operator-console/optimization/thermal/execution"
        };
    }


    handleRedirect() {
        let endID = Number(this.state.hour) + 4;
        localStorage.setItem("intraday-start-hour", this.state.hour);
        localStorage.setItem("intraday-end-hour", endID + "");
        let url = (config.CONFIG.IS_PILOT) ? "/catalyst-dc-operator-console/optimization/thermal/execution" :
            "/catalyst-dc-operator-console/optimization/thermal/execution-scenario";
        this.setState({
            redirect: true,
            nextRoute: url
        });
    };

    render() {
        return (
            <div>
                {this.state.redirect && <Redirect to={this.state.nextRoute}/>}
                <div>
                    <CardHeader>
                        <strong> Optimization > Thermal Intraday Plan</strong>
                    </CardHeader>
                    <br/><br/>
                    <Card>
                        <ThermalPlanComponent key={this.state.hour}
                                                  time={this.state.time}
                                                  timeframe={this.state.timeframe}
                                                  strategyType={this.state.strategyType}
                                                  confidenceLevel={this.state.confidenceLevel}
                                                  hour={this.state.hour}
                                                  redirectHandler={this.handleRedirect}/>
                    </Card>
                </div>

            </div>
        )
    }
}

export default ThermalIdPlanContainer;
