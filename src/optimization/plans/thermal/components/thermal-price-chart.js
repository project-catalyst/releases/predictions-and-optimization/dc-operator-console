import React from 'react';

import {CHART_TYPE, LABEL_TYPE, LABELS, LINE_TYPE} from "../../../../commons/constants/chart-constants";
import {COLOURS} from "../../../../commons/constants/colour-constants";

import Parser from "../../../data-parser/optimization-parser";
import GenericChart from "../../../../commons/chart-components/generic-chart";
import {CONFIG, GRAPH_CONFIG} from "../../../../config"

const PRICE_TITLE = "Thermal Price Profile";

const PROFILES = [
    {
        key: 'Heat Price Profile',
        accessor: 'refrencePrices',
        type: LABEL_TYPE.LINE,
        color: COLOURS.OPTIMIZATION_PAGES.Flex_Order
    },
];

class ThermalPriceChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            results: this.props.results,
            yLimits: [0, GRAPH_CONFIG[CONFIG.PILOT].Y_LIMIT_PRICE],
            hour: this.props.hour,
            data: [],
            isLoaded: false
        };
    }

    componentDidMount() {
        let formattedData = Parser.parseDayAhead(PROFILES, this.state.results, LABELS.HOUR, this.state.hour);
        let data = {
            dataKey: LABELS.HOUR,
            oyLabel: LABELS.PRICE,
            oxLabel: LABELS.HOUR,
            yLimit: this.state.yLimits,
            values: formattedData,
        };
        this.setState({
            data: data,
            isLoaded: true
        });
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <GenericChart
                    type={CHART_TYPE.COMPOSED}
                    title={PRICE_TITLE}
                    data={this.state.data}
                    labels={PROFILES}
                    lineType={LINE_TYPE.MONOTONE}
                    size_ratio= {1}
                />}
            </div>
        )
    }
}

export default ThermalPriceChart