import React from 'react';

import * as config from "../../../config";
import Redirect from "react-router-dom/Redirect";
import EnergyPlanComponent from "./components/energy-plan-component";
import {Card, CardHeader} from "reactstrap";
import {TIMEFRAME} from "../../../util-components/timeframe-enums";


class EnergyIdPlanContainer extends React.Component {

    constructor(props) {
        super(props);
        this.handleRedirect = this.handleRedirect.bind(this);
        this.state = {
            time: localStorage.getItem('time'),
            timeframe: TIMEFRAME.INTRADAY,
            strategyType: "energy",
            confidenceLevel: 0,
            hour: localStorage.getItem("intraday-end-hour"),
            redirect: false,
            nextRoute: "/catalyst-dc-operator-console/optimization/energy/execution"
        };
    }


    handleRedirect() {
        let endID = Number(this.state.hour) + 4;
        localStorage.setItem("intraday-start-hour", this.state.hour);
        localStorage.setItem("intraday-end-hour", endID + "");
        let url = (config.CONFIG.IS_PILOT) ? "/catalyst-dc-operator-console/optimization/energy/execution" :
            "/catalyst-dc-operator-console/optimization/energy/execution-scenario";
        this.setState({
            redirect: true,
            nextRoute: url
        });
    };

    render() {
        return (
            <div>
                {this.state.redirect && <Redirect to={this.state.nextRoute}/>}
                <div>
                    <CardHeader>
                        <strong> Optimization > Energy Intraday Plan</strong>
                    </CardHeader>
                    <br/><br/>
                    <Card>
                        <EnergyPlanComponent key={this.state.hour}
                                                  time={this.state.time}
                                                  timeframe={this.state.timeframe}
                                                  strategtyType={this.state.strategyType}
                                                  confidenceLevel={this.state.confidenceLevel}
                                                  hour={this.state.hour}
                                                  redirectHandler={this.handleRedirect}/>
                    </Card>
                </div>

            </div>
        )
    }
}

export default EnergyIdPlanContainer