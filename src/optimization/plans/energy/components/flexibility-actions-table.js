import React from 'react';
import APIResponseErrorMessage from "../../../../commons/errorhandling/api-response-error-message";
import Table from "../../../../commons/tables/table"
import {MDBBadge} from "mdbreact";
import * as API_OPTIMIZATION from "../../../api/optimization-api";

const columns = [
    {
        Header: 'Action Name',
        accessor: 'action_name',
    },
    {
        Header: 'Start Time',
        accessor: 'start_time',
    },
    {
        Header: 'End Time',
        accessor: 'end_time',
    },
    {
        Header: 'Value (kWh)',
        accessor: 'value',
    },
    {
        Header: 'Thermal Value (kWh)',
        accessor: 'thermalValue',
    },
    {
        Header: '%',
        accessor: 'percentage',
    },
    {
        Header: 'Active',
        accessor: 'active',
        Cell: (row) => {
            if (row.original && row.original.active) {
                return (<div><MDBBadge color="success">Active</MDBBadge></div>);
            } else {
                return (<div><MDBBadge color="danger">Inactive</MDBBadge></div>);
            }
        }
    }
];

const filters = [
    {
        accessor: 'action name',
    },
    {
        accessor: 'start time',
    }
];

class FlexibilityActionsTable extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            time:  this.props.time,
            timeframe: this.props.timeframe,
            hour:this.props.hour,
            tableData: [],
            actionsLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        API_OPTIMIZATION.fetchOptimizationActions( this.state.timeframe,this.state.hour, this.state.time, (result, status, err) => {
            if (result !== null && status === 200) {
                let data  =[];
                result.forEach(x => {
                    data.push({
                        action_name: x.type,
                        start_time: new Date(x.startTime).toUTCString(),
                        end_time: new Date(x.endTime).toUTCString(),
                        value: x.amount,
                        thermalValue: x.thermalAmount,
                        percentage: (x.movePercentage > 0) ? x.movePercentage : "-",
                        active: x.active,
                        color: x.active ? '#99E896' : 'white'
                    });
                });
                this.setState({
                    tableData: data,
                    actionsLoaded: true
                });
            }else{
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
                console.log(err);
            }
        });
    }

    render() {
        return (
            <div>
                {
                    this.state.actionsLoaded &&
                    <Table
                        data={this.state.tableData}
                        columns={columns}
                        search={filters}
                        pageSize={this.state.tableData.length}
                    />
                }
                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage
                        errorStatus={this.state.errorStatus}
                        error={this.state.error}
                    />
                }
            </div>
        )

    }
}


export default FlexibilityActionsTable
