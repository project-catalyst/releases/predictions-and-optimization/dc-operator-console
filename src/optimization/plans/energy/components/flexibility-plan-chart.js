import React from 'react';

import {CHART_TYPE, LABEL_TYPE, LABELS, LINE_TYPE} from "../../../../commons/constants/chart-constants";
import {COLOURS} from "../../../../commons/constants/colour-constants";

import Parser from "../../../data-parser/optimization-parser";
import GenericChart from "../../../../commons/chart-components/generic-chart";

const DC_FLEXIBILITY_ACTIONS_TITLE = "Shifting Energy Flexibility Actions";

const PROFILES = [
    {
        key: 'Consumption RT Values',
        accessor: 'consumptionRTValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Real_Time
    },
    {
        key: 'Consumption DT Values',
        accessor: 'consumptionDTValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Delay_Tolerable
    },
    {key: 'Cooling Values', accessor: 'coolingValues', type: LABEL_TYPE.BAR, color: COLOURS.OPTIMIZATION_PAGES.Cooling},
    {
        key: 'TES Charge',
        accessor: 'chargeTesValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.TES_Charge
    },

    {
        key: 'Battery Charge',
        accessor: 'chargeBatteryValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Battery_Charge
    },
    {
        key: 'Provided Grid Energy',
        accessor: 'providedEnergyValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Provided_Grid_Energy
    },
    {
        key: 'Load Hosting',
        accessor: 'hostValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Host_Values
    },
    {
        key: 'TES Discharge',
        accessor: 'dischargeTesValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.TES_Discharge
    },
    {
        key: 'Battery Discharge',
        accessor: 'dischargeBatteryValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Battery_Discharge
    },
    {
        key: 'Load Relocation',
        accessor: 'relocateValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Relocate_Values
    }
];

class FlexibilityPlanChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            results: this.props.results,
            yLimits: this.props.yLimits,
            hour: this.props.hour,
            data: [],
            isLoaded: false
        };
    }

    componentDidMount() {
        let formattedData = Parser.parseDayAhead(PROFILES, this.state.results, LABELS.HOUR, this.state.hour);
        let data = {
            dataKey: LABELS.HOUR,
            oyLabel: LABELS.KWH,
            oxLabel: LABELS.HOUR,
            yLimit: this.state.yLimits,
            values: formattedData,
        };
        this.setState({
            data: data,
            isLoaded: true
        });
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <GenericChart
                    type={CHART_TYPE.COMPOSED}
                    title={DC_FLEXIBILITY_ACTIONS_TITLE}
                    data={this.state.data}
                    labels={PROFILES}
                    lineType={LINE_TYPE.MONOTONE}
                    double={["TES Discharge", "Provided Grid Energy", "Battery Discharge", "Load Relocation"]}
                />}
            </div>
        )
    }
}

export default FlexibilityPlanChart