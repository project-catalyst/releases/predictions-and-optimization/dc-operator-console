import React from 'react';

import {CHART_TYPE, LABEL_TYPE, LABELS, LINE_TYPE} from "../../../../commons/constants/chart-constants";
import {COLOURS} from "../../../../commons/constants/colour-constants";

import Parser from "../../../data-parser/optimization-parser";
import GenericChart from "../../../../commons/chart-components/generic-chart";


const DC_ADAPTED_TITLE = "DC Adapted Energy Profile";

const PROFILES = [
    {
        key: 'Adapted DC Energy Profile',
        accessor: 'optimizedProfileValues',
        type: LABEL_TYPE.LINE,
        color: COLOURS.OPTIMIZATION_PAGES.ADAPTED
    },
    {
        key: 'DC Energy Baseline',
        accessor: 'predictedDemandValues',
        type: LABEL_TYPE.LINE,
        color: COLOURS.OPTIMIZATION_PAGES.BASELINE
    },
    {
        key: 'Renewable Energy',
        accessor: 'renewableEnergyValues',
        type: LABEL_TYPE.LINE,
        color: COLOURS.OPTIMIZATION_PAGES.GREEN
    }
];

class RenewableTargetChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            results: this.props.results,
            yLimits: this.props.yLimits,
            hour: this.props.hour,
            data: [],
            isLoaded: false
        };
    }

    componentDidMount() {
        let formattedData = Parser.parseDayAhead(PROFILES, this.state.results, LABELS.HOUR, this.state.hour);
        let data = {
            dataKey: LABELS.HOUR,
            oyLabel: LABELS.KWH,
            oxLabel: LABELS.HOUR,
            yLimit: this.state.yLimits,
            values: formattedData,
        };
        this.setState({
            data: data,
            isLoaded: true
        });
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <GenericChart
                    type={CHART_TYPE.COMPOSED}
                    title={DC_ADAPTED_TITLE}
                    data={this.state.data}
                    labels={PROFILES}
                    lineType={LINE_TYPE.MONOTONE}
                />}
            </div>
        )
    }
}

export default RenewableTargetChart