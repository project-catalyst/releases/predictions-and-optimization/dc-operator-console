import React from 'react';

import {Button, CardTitle, Col, Label, Row} from 'reactstrap';
import APIResponseErrorMessage from "../../../../commons/errorhandling/api-response-error-message";
import FlexibilityActionsTable from "./../../energy/components/flexibility-actions-table";
import * as API_OPTIMIZATION from "../../../api/optimization-api";
import Redirect from "react-router-dom/Redirect";
import Popup from "reactjs-popup";
import BadgeStrategies from "./../../../../util-components/badge-strategies";
import {CONFIG, GRAPH_CONFIG} from "../../../../config"
import RenewablePlanChar from "./renewable-plan-char";
import RenewableTargetChart from "./renewable-target-chart";

class RenewablePlanComponent extends React.Component {

    constructor(props) {
        super(props);
        this.fetchChartData = this.fetchChartData.bind(this);
        this.redirectHandler = this.props.redirectHandler;
        this.state = {
            time: this.props.time,
            timeframe: this.props.timeframe,
            strategyType: this.props.strategtyType,
            confidenceLevel: this.props.confidenceLevel,
            hour: this.props.hour,
            yLimits: [-1, GRAPH_CONFIG[CONFIG.PILOT].Y_LIMIT_OPT_POSITIVE],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            redirect: false
        };
    }

    componentDidMount() {
        this.fetchChartData();
    }

    fetchChartData() {
        console.log("hour" + this.state.hour);
        API_OPTIMIZATION.fetchOptimizationPlans(this.state.strategyType, this.state.timeframe, this.state.hour, this.state.time, this.state.confidenceLevel, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    data: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err,
                    isLoaded: false,
                }));
                console.log(err);
            }
        })
    }


    render() {
        return (
            <div>
                {this.state.redirect && <Redirect to={this.state.nextRoute}/>}
                <div>
                    {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

                    <CardTitle>
                        <Col sm={{size: 'auto', offset: 1}}>
                            <BadgeStrategies time={this.state.time} confidenceLevel={this.state.confidenceLevel}/>
                        </Col>

                        <Col sm={{size: 'auto', offset: 11}}>
                            <Button onClick={this.redirectHandler}
                                    outline color='success' size="sm"
                            > Validate>></Button>
                        </Col>
                    </CardTitle>
                    <Row>
                        <Col>
                            {this.state.isLoaded && <RenewableTargetChart key={this.state.time}
                                                                                   yLimits={this.state.yLimits}
                                                                                   results={this.state.data}
                                                                                   hour={this.state.hour}/>}
                        </Col>
                        <Col>
                            {this.state.isLoaded && <RenewablePlanChar key={this.state.time}
                                                                          yLimits={this.state.yLimits}
                                                                          results={this.state.data}
                                                                          hour={this.state.hour}/>}
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            <br/> <br/>
                            <FlexibilityActionsTable key={this.state.time}
                                                     time={this.state.time}
                                                     hour={this.state.hour}
                                                     timeframe={this.state.timeframe}/>
                        </Col>
                    </Row>

                </div>

            </div>
        )
    }
}

export default RenewablePlanComponent
