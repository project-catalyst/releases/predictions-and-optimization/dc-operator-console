import React from 'react';

import {CHART_TYPE, LABEL_TYPE, LABELS, LINE_TYPE} from "../../../../commons/constants/chart-constants";
import {COLOURS} from "../../../../commons/constants/colour-constants";

import Parser from "../../../data-parser/optimization-parser";
import GenericChart from "../../../../commons/chart-components/generic-chart";
import {CONFIG, GRAPH_CONFIG} from "../../../../config"

const DC_ADAPTED_TITLE = "Energy Reference Prices";

const PROFILES = [
    {
        key: 'Energy Price',
        accessor: 'refrencePrices',
        type: LABEL_TYPE.LINE,
        color: COLOURS.OPTIMIZATION_PAGES.Flex_Order
    }
];

class PriceDrivenPricesChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            results: this.props.results,
            yLimits: [0,GRAPH_CONFIG[CONFIG.PILOT].Y_LIMIT_PRICE],
            hour: this.props.hour,
            data: [],
            isLoaded: false
        };
    }

    componentDidMount() {
        let formattedData = Parser.parseRT(PROFILES, this.state.results, LABELS.TIME_5_MIN, this.state.hour);
        let data = {
            dataKey: LABELS.TIME_5_MIN,
            oyLabel: LABELS.PRICE,
            oxLabel: LABELS.TIME_5_MIN,
            yLimit: this.state.yLimits,
            values: formattedData,
        };
        this.setState({
            data: data,
            isLoaded: true
        });
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <GenericChart
                    type={CHART_TYPE.COMPOSED}
                    title={DC_ADAPTED_TITLE}
                    data={this.state.data}
                    labels={PROFILES}
                    lineType={LINE_TYPE.MONOTONE}
                />}
            </div>
        )
    }
}

export default PriceDrivenPricesChart;
