import React from 'react';

import {Row, Col, Input, Label, Card, CardTitle, CardHeader} from 'reactstrap';
import DCAdaptedChart from "./../energy/components/energy-dc-adapted-chart";
import DCFlexibilityActionsChart from "./../energy/components/energy-dc-flexibility-actions-chart";
import DCFlexibilityDiffChart from "./../energy/components/energy-dc-flexibility-diff-chart";
import EnergyDcBaselineChart from "./../energy/components/energy-dc-baseline-chart";
import APIResponseErrorMessage from "../../../commons/errorhandling/api-response-error-message";
import BadgeStrategies from "../../../util-components/badge-strategies";
import AnimationPanel from "../../../commons/chart-components/animation-panel";
import Parse from "../../data-parser/optimization-parser"
import Redirect from "react-router-dom/Redirect";
import Popup from "reactjs-popup";
import * as API_OPTIMIZATION from "../../api/optimization-api";
import {TIMEFRAME} from "../../../util-components/timeframe-enums";
import PriceDrivenPricesChart from "./components/price-driven-prices-chart";

/**
 * TODO: Depending on ID active or not:
 *  - change MAX
 *  - change timeframe as REALTIME_INTRADAY_FRAME or REALTIME_DAYAHEAD_FRAME
 *  - if (localStorage.getItem("intraday-start-hour") !== '20')
 *  - change         let url = "/catalyst-dc-operator-console/optimization/plan/da/energy";
 */
const MAX = 288;

class PriceDrivenExecutionContainerScenario extends React.Component {

    constructor(props) {
        super(props);
        this.fetchChartData = this.fetchChartData.bind(this);
        this.formatStepData = this.formatStepData.bind(this);
        this.startSimulationFunction = this.startSimulationFunction.bind(this);
        this.stopSimulation = this.stopSimulation.bind(this);
        this.nextSimulationStepFunction = this.nextSimulationStepFunction.bind(this);
        this.state = {
            time: localStorage.getItem('time'),
            timeframe: TIMEFRAME.REALTIME_DAYAHEAD_FRAME,
            strategyType: "price-driven",
            hour: localStorage.getItem("intraday-start-hour"),
            yLimits: [-1000, 3000],
            isLoaded: false,
            isOptimizedLoaded: false,
            isUnoptimizedLoaded: false,
            optimizedData: null,
            unoptimizedData: null,
            stepOptimizedData: null,
            stepUnoptimizedData: null,
            errorStatus: 0,
            error: null,
            step: 0,
            goOn: false
        };
    }

    componentDidMount() {
        this.fetchChartData();
    }

    fetchChartData() {
        console.log("hour " + this.state.hour);
        API_OPTIMIZATION.fetchOptimizedExecutionValues(this.state.strategyType, this.state.timeframe, this.state.time , this.state.hour,  (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    optimizedData: result,
                    isOptimizedLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err,
                    isOptimizedLoaded: false,
                }));
                console.log(err);
            }
        });

        API_OPTIMIZATION.fetchUnoptimizedExecutionValues(this.state.strategyType, this.state.timeframe, this.state.time , this.state.hour, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    unoptimizedData: result,
                    isUnoptimizedLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err,
                    isUnoptimizedLoaded: false,
                }));
                console.log(err);
            }
        });
    }

    formatStepData() {
        if (this.state.isOptimizedLoaded && this.state.isUnoptimizedLoaded) {
            if (this.state.step <= MAX) {
                const optData = Parse.formatData(this.state.optimizedData, this.state.step, 24);
                const unoptData = Parse.formatData(this.state.unoptimizedData, this.state.step, 24)
                const minute = this.state.step > 23 ? (((this.state.step - 24) % 12) * 5) : 0;
                const hour = this.state.step > 23 ? Number(Math.floor((this.state.step - 24) / 12) + minute / 100) : 0;
                const step = this.state.step + 1;

                this.setState({
                    stepOptimizedData: optData,
                    stepUnoptimizedData: unoptData,
                    hour: hour,
                    isLoaded: true,
                    step: step
                })
            }
        }
    }

    simulate() {
        let goOn = this.state.goOn;
        let step = this.state.step;
        this.formatStepData();
        setTimeout(() => {
            if (step > MAX || !goOn) {
                return;
            }
            this.simulate();
        }, this.state.timeout);
    }

    stopSimulation() {
        this.setState({
            goOn: false
        })
    }

    startSimulationFunction() {
        if (this.state.step <= MAX) {
            this.setState({
                goOn: true,
            }, this.simulate);
        }
    }

    nextSimulationStepFunction() {
        this.formatStepData();
    }


    renderRedirect = () => {
        let url = "/catalyst-dc-operator-console/optimization/plan/da/energy";
        if (this.state.step > MAX) {

            if (localStorage.getItem("intraday-end-hour") !== '0')
                return (<Redirect to={url}/>);
            else
                return (
                    <Popup
                        open={true}>
                        <div>{this.metrics}</div>
                    </Popup>
                )
        }
        return null;
    };

    render() {
        return (
            <div>
                {this.renderRedirect()}
                <div>
                    {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
                    <CardHeader>
                        <strong> Optimization > Price Driven Execution</strong>
                    </CardHeader>g
                    <Card body>
                        <CardTitle>
                            <Col sm={{size: 'auto', offset: 0}}>
                                <BadgeStrategies time={this.state.time} confidenceLevel={0.0}/>
                            </Col>
                        </CardTitle>
                        <Row>
                            <Col xs="2" sm={{offset: 0}}>
                                <AnimationPanel startFunction={this.startSimulationFunction}
                                                stopFunction={this.stopSimulation}
                                                nextStepFunction={this.nextSimulationStepFunction}/>
                            </Col>
                        </Row>

                        <br/>
                        <Row>
                            <Col>
                                {this.state.isLoaded && <PriceDrivenPricesChart key={this.state.step}
                                                                        yLimits={this.state.yLimits}
                                                                        results={this.state.stepOptimizedData}
                                                                        hour={this.state.hour}/>}
                            </Col>
                            <Col>
                                {this.state.isLoaded &&
                                <DCFlexibilityActionsChart key={this.state.step}
                                                           yLimits={this.state.yLimits}
                                                           results={this.state.stepOptimizedData}
                                                           hour={this.state.hour}/>}
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col>
                                {this.state.isLoaded &&
                                <EnergyDcBaselineChart key={this.state.step}
                                                       yLimits={this.state.yLimits}
                                                       results={this.state.stepUnoptimizedData}
                                                       hour={this.state.hour}/>}
                            </Col>
                            <Col>
                                {this.state.isLoaded &&
                                <DCFlexibilityDiffChart key={this.state.step}
                                                        yLimits={this.state.yLimits}
                                                        results={this.state.stepOptimizedData}
                                                        hour={this.state.hour}/>}
                            </Col>
                        </Row>


                    </Card>
                </div>

            </div>
        )
    }
}

export default PriceDrivenExecutionContainerScenario;
