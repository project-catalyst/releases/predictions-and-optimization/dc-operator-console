import React from 'react';

import {CHART_TYPE, LABEL_TYPE, LABELS, LINE_TYPE} from "../../../../commons/constants/chart-constants";
import {COLOURS} from "../../../../commons/constants/colour-constants";

import Parser from "../../../data-parser/optimization-parser";
import GenericChart from "../../../../commons/chart-components/generic-chart";

const DC_ADAPTED_TITLE = "DC Adapted Energy Profile";

const PROFILES = [
    {
        key: 'Provided Energy Grid',
        accessor: 'providedEnergyValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Provided_Grid_Energy
    },
    {
        key: 'Flexibility Order',
        accessor: 'flexibilityOrder',
        type: LABEL_TYPE.LINE,
        color: COLOURS.OPTIMIZATION_PAGES.Flex_Order
    }
];

class EnergyDcAdaptedChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            results: this.props.results,
            yLimits: this.props.yLimits,
            hour: this.props.hour,
            data: [],
            isLoaded: false
        };
    }

    componentDidMount() {
        let formattedData = Parser.parseRT(PROFILES, this.state.results, LABELS.TIME_5_MIN, this.state.hour);
        let data = {
            dataKey: LABELS.TIME_5_MIN,
            oyLabel: LABELS.KWH,
            oxLabel: LABELS.TIME_5_MIN,
            yLimit: this.state.yLimits,
            values: formattedData,
        };
        this.setState({
            data: data,
            isLoaded: true
        });
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <GenericChart
                    type={CHART_TYPE.COMPOSED}
                    title={DC_ADAPTED_TITLE}
                    data={this.state.data}
                    labels={PROFILES}
                    lineType={LINE_TYPE.MONOTONE}
                />}
            </div>
        )
    }
}

export default EnergyDcAdaptedChart;
