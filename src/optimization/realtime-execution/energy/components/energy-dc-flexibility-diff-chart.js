import React from 'react';

import {CHART_TYPE, LABEL_TYPE, LABELS, LINE_TYPE} from "../../../../commons/constants/chart-constants";
import {COLOURS} from "../../../../commons/constants/colour-constants";

import Parser from "../../../data-parser/optimization-parser";
import GenericChart from "../../../../commons/chart-components/generic-chart";

const DC_DIFF_TITLE = "Shifted Energy Flexibility";

const PROFILES = [
    {key: 'Delta Differences', accessor: 'deltaDiff', type: LABEL_TYPE.BAR, color: COLOURS.OPTIMIZATION_PAGES.Cooling}
];

class EnergyDcFlexibilityDiffChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            results: this.props.results,
            yLimits: this.props.yLimits,
            hour: this.props.hour,
            data: [],
            isLoaded: false
        };
    }

    componentDidMount() {
        let formattedData = Parser.parseRT(PROFILES, this.state.results, LABELS.TIME_5_MIN, this.state.hour);
        let data = {
            dataKey: LABELS.TIME_5_MIN,
            oyLabel: LABELS.KWH,
            oxLabel: LABELS.TIME_5_MIN,
            yLimit: this.state.yLimits,
            values: formattedData,
        };
        this.setState({
            data: data,
            isLoaded: true
        });
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <GenericChart
                    type={CHART_TYPE.COMPOSED}
                    title={DC_DIFF_TITLE}
                    data={this.state.data}
                    labels={PROFILES}
                    lineType={LINE_TYPE.MONOTONE}
                />}
            </div>
        )
    }
}

export default EnergyDcFlexibilityDiffChart;
