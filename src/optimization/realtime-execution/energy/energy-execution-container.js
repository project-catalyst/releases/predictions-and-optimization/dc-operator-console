import React from 'react';

import {Card, CardHeader, CardTitle, Col, Row} from 'reactstrap';
import EnergyDCAdaptedChart from "./components/energy-dc-adapted-chart";
import EnergyDCFlexibilityActionsChart from "./components/energy-dc-flexibility-actions-chart";
import EnergyDCFlexibilityDiffChart from "./components/energy-dc-flexibility-diff-chart";
import EnergyDcBaselineChart from "./components/energy-dc-baseline-chart";

import IntradayDDL from "../../../util-components/intraday-ddl";
import APIResponseErrorMessage from "../../../commons/errorhandling/api-response-error-message";
import BadgeStrategies from "../../../util-components/badge-strategies";
import * as API_OPTIMIZATION from "../../api/optimization-api";
import {TIMEFRAME} from "../../../util-components/timeframe-enums";
import UncontrolledAlert from "reactstrap/es/UncontrolledAlert";
import {CONFIG, GRAPH_CONFIG} from "../../../config"

class EnergyExecutionContainer extends React.Component {

    constructor(props) {
        super(props);
        this.dropDownHandler = this.dropDownHandler.bind(this);
        this.fetchChartData = this.fetchChartData.bind(this);
        this.state = {
            time:  localStorage.getItem('time'),
            timeframe: TIMEFRAME.REALTIME_INTRADAY_FRAME,
            strategyType: "energy",
            hour: 0,
            yLimits: [GRAPH_CONFIG[CONFIG.PILOT].Y_LIMIT_OPT_NEGATIVE, GRAPH_CONFIG[CONFIG.PILOT].Y_LIMIT_OPT_POSITIVE],
            isUnoptimizedLoaded: false,
            optimizedData: null,
            unoptimizedData: null,
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchChartData();
    }

    fetchChartData() {
        console.log("hour" + this.state.hour);
        API_OPTIMIZATION.fetchOptimizedExecutionValues(this.state.strategyType,this.state.timeframe, this.state.time , this.state.hour, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    optimizedData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err,
                    isLoaded: false,
                }));
                console.log(err);
            }
        });

        API_OPTIMIZATION.fetchUnoptimizedExecutionValues(this.state.strategyType, this.state.timeframe, this.state.time , this.state.hour, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    unoptimizedData: result,
                    isUnoptimizedLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err,
                    isUnoptimizedLoaded: false,
                }));
                console.log(err);
            }
        });
    }

    dropDownHandler(e) {
        this.setState({
            hour: e.target.value,
            isLoaded: false
        }, () => {
            this.fetchChartData();
        });
    }


    render() {
        return (
            <div>
                <div>
                    {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
                    <CardHeader>
                        <strong> Optimization > Energy Action Execution</strong>
                    </CardHeader>
                    <Card body>
                    <Row>
                        <Col xs="2" sm={{ offset: 1}} >
                            <IntradayDDL handler={this.dropDownHandler}/>
                        </Col>
                    </Row>
                    <CardTitle>
                        <Col sm={{size: 'auto', offset: 1}}>
                            <BadgeStrategies time = {this.state.time} confidenceLevel={0.0}/>
                        </Col>
                    </CardTitle>
                        {!(this.state.isLoaded && this.state.isUnoptimizedLoaded ) &&
                        <UncontrolledAlert color="warning">  Loading...
                        </UncontrolledAlert>
                        }

                        <br/>
                        <Row>
                            <Col>
                                {this.state.isLoaded && this.state.isUnoptimizedLoaded && <EnergyDCAdaptedChart
                                                                        yLimits={this.state.yLimits}
                                                                        results={this.state.optimizedData}
                                                                        hour={this.state.hour}/>}

                            </Col>
                            <Col>
                                {this.state.isLoaded && this.state.isUnoptimizedLoaded &&
                                <EnergyDCFlexibilityActionsChart
                                                           yLimits={this.state.yLimits}
                                                           results={this.state.optimizedData}
                                                           hour={this.state.hour}/>}

                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col>
                                {this.state.isLoaded && this.state.isUnoptimizedLoaded && <EnergyDcBaselineChart
                                                       yLimits={this.state.yLimits}
                                                       results={this.state.unoptimizedData}
                                                       hour={this.state.hour}/>}

                            </Col>
                            <Col>
                                {this.state.isLoaded && this.state.isUnoptimizedLoaded &&
                                <EnergyDCFlexibilityDiffChart
                                                        yLimits={this.state.yLimits}
                                                        results={this.state.optimizedData}
                                                        hour={this.state.hour}/>}

                            </Col>
                        </Row>


                    </Card>
                </div>

            </div>
        )
    }
}

export default EnergyExecutionContainer;
