import React from 'react';

import {CHART_TYPE, LABEL_TYPE, LABELS, LINE_TYPE} from "../../../../commons/constants/chart-constants";
import {COLOURS} from "../../../../commons/constants/colour-constants";

import Parser from "../../../data-parser/optimization-parser";
import GenericChart from "../../../../commons/chart-components/generic-chart";

const DC_FLEXIBILITY_ACTIONS_TITLE = "Shifting Energy Flexibility Actions";

const PROFILES = [
    {
        key: 'Consumption RT Values',
        accessor: 'consumptionRTValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Real_Time
    },
    {
        key: 'Consumption DT Values',
        accessor: 'consumptionDTValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Delay_Tolerable
    },
    {key: 'Cooling Values', accessor: 'coolingValues', type: LABEL_TYPE.BAR, color: COLOURS.OPTIMIZATION_PAGES.Cooling},
    {
        key: 'TES Charge',
        accessor: 'tesChargeValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.TES_Charge
    },
    {
        key: 'TES Discharge',
        accessor: 'tesDischargeValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.TES_Discharge
    },
    {
        key: 'Battery Charge',
        accessor: 'upsChargeValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Battery_Charge
    },
    {
        key: 'Battery Discharge',
        accessor: 'upsDischargeValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Battery_Discharge
    }
];

class ThermalDcFlexibilityActionsChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            results: this.props.results,
            yLimits: this.props.yLimits,
            hour: this.props.hour,
            data: [],
            isLoaded: false
        };
    }

    componentDidMount() {
        let formattedData = Parser.parseRT(PROFILES, this.state.results, LABELS.TIME_5_MIN, this.state.hour);
        let data = {
            dataKey: LABELS.TIME_5_MIN,
            oyLabel: LABELS.KWH,
            oxLabel: LABELS.TIME_5_MIN,
            yLimit: this.state.yLimits,
            values: formattedData,
        };
        this.setState({
            data: data,
            isLoaded: true
        });
    }

    render() {
        return (
            <div>

                {this.state.isLoaded && <GenericChart
                    type={CHART_TYPE.COMPOSED}
                    title={DC_FLEXIBILITY_ACTIONS_TITLE}
                    data={this.state.data}
                    labels={PROFILES}
                    lineType={LINE_TYPE.MONOTONE}
                />}
            </div>
        )
    }
}

export default ThermalDcFlexibilityActionsChart;
