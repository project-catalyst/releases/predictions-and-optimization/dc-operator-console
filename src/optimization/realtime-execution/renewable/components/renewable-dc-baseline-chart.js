import React from 'react';

import {
    CHART_TYPE,
    LABEL_TYPE,
    LABELS,
    LINE_TYPE
} from "../../../../commons/constants/chart-constants";
import {COLOURS} from "../../../../commons/constants/colour-constants";

import Parser from "../../../data-parser/optimization-parser";
import GenericChart from "../../../../commons/chart-components/generic-chart";
import APIResponseErrorMessage from "../../../../commons/errorhandling/api-response-error-message";

const DC_ENERGY_BASELINE_TITLE = "DC Energy Baseline";

const PROFILES = [
    {
        key: 'Consumption RT Values',
        accessor: 'consumptionRTValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Real_Time
    },
    {
        key: 'Consumption DT Values',
        accessor: 'consumptionDTValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Delay_Tolerable
    },
    {   key: 'Cooling Values',
        accessor: 'coolingValues',
        type: LABEL_TYPE.BAR,
        color: COLOURS.OPTIMIZATION_PAGES.Cooling
    }
];

class RenewableDcBaselineChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            results: this.props.results,
            hour: this.props.hour,
            yLimits: this.props.yLimits,
            isLoaded: false,
            data: []
        };
    }

    componentDidMount() {
        console.log(this.state.results);
        let formattedData = Parser.parseRT(PROFILES, this.state.results, LABELS.TIME_5_MIN, this.state.hour);
        let data = {
            dataKey: LABELS.TIME_5_MIN,
            oyLabel: LABELS.KWH,
            oxLabel: LABELS.TIME_5_MIN,
            yLimit: this.state.yLimits,
            values: formattedData,
        };
        this.setState({
            data: data,
            isLoaded: true
        });
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <GenericChart
                    type={CHART_TYPE.COMPOSED}
                    title={DC_ENERGY_BASELINE_TITLE}
                    data={this.state.data}
                    labels={PROFILES}
                    lineType={LINE_TYPE.MONOTONE}
                />}
            </div>
        )
    }
}

export default RenewableDcBaselineChart;
