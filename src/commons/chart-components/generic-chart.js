import React, {Component} from "react";
import {
    Area,
    AreaChart,
    Bar,
    BarChart,
    CartesianGrid,
    ComposedChart,
    Label,
    Legend,
    Line,
    LineChart,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis
} from "recharts";
import {CHART_TYPE, LABEL_TYPE} from "../constants/chart-constants";
import styles from '../styles/catalyst-style.css';
import {COLOURS} from "../constants/colour-constants";

const FLEXIBILITY_ORDER = 'Flexibility Order';

class GenericChart extends Component {
    constructor(props) {
        super(props);
        this.selectBar = this.selectBar.bind(this);

        let general_chart;
        if (this.props.type === CHART_TYPE.LINE)
            general_chart = LineChart;
        else if (this.props.type === CHART_TYPE.BAR)
            general_chart = BarChart;
        else if (this.props.type === CHART_TYPE.COMPOSED)
            general_chart = ComposedChart;
        else if (this.props.type === CHART_TYPE.AREA)
            general_chart = AreaChart;
        else if (this.props.type === CHART_TYPE.BARLINE)
            general_chart = ComposedChart;

        this.state = {
            general_chart: general_chart,
            type: this.props.type,
            title: this.props.title,
            dataKey: this.props.data.dataKey,
            oxLabel: this.props.data.oxLabel,
            oyLabel: this.props.data.oyLabel,
            values: this.props.data.values,
            yLimit: this.props.data.yLimit,
            labels: this.props.labels,
            double: this.props.double,
            line: ''
        }
    }

    componentDidMount() {
        if (this.state.type === CHART_TYPE.BARLINE) {
            const data = this.state.values;
            data.forEach(e => {
                if (e[FLEXIBILITY_ORDER] === 0)
                    e[FLEXIBILITY_ORDER] = null;
            });

            this.setState({
                values: data
            })
        }
    }

    selectBar(event) {
        let updatedLabels = [];
        let values = this.state.values;

        let line = true;
        for (let i = 0; i < this.state.labels.length; i++) {
            let label = this.state.labels[i];
            if (label.key !== event.dataKey) {
                updatedLabels.push(label);
            } else {
                line = false;
                if (label.key.includes('*')) {
                    let newLabel = {key: label.key.slice(0, -1), color: label.color, type: label.type};
                    updatedLabels.push(newLabel);
                } else {
                    let newLabel = {key: label.key + "*", color: label.color, type: label.type};
                    updatedLabels.push(newLabel);
                }
            }
        }

        let newLine = this.state.line;
        if (line)
            newLine = this.state.line === '*' ? '' : '*';

        this.setState({
            labels: updatedLabels,
            values: values,
            line: newLine
        });
    }

    shouldComponentUpdate(props) {
        return this.state.values !== props.data.values || this.state.labels !== props.labels;
    }

    line(label, index) {
        return (<Line
            type="monotone"
            key={index}
            dataKey={label.key}
            stroke={label.color}
            fill={label.color}
            strokeWidth={2}
            isAnimationActive={false}
        />);
    }

    bar(label, index) {
        const stackId = this.state.double && this.state.double.includes(label.key) ? this.state.dataKey + 1 : this.state.dataKey;
        return (<Bar
            key={index}
            dataKey={label.key}
            fill={label.color}
            stackId={stackId}
            isAnimationActive={false}
        />);
    }

    area(label, index) {
        return (<Area
            type="monotone"
            dataKey={label.key}
            fill={label.color}
            stroke={label.color}
        />);
    }

    render() {
        let X_axis = <XAxis dataKey={this.state.dataKey}>
            <Label
                value={this.state.oxLabel}
                position="insideBottomRight"
                dy={10}
                dx={20}
            />
        </XAxis>;

        let Y_axis = <YAxis type="number" domain={this.state.yLimit}>
            <Label
                value={this.state.oyLabel}
                position="insideTopLeft"
                dy={-30}
            />
        </YAxis>;

        let size_ratio = (typeof this.props.size_ratio !== "undefined") ? this.props.size_ratio : 2.0;

        let comp = this.state.labels.map((label, index) => {
            switch (this.state.type) {
                case CHART_TYPE.LINE:
                    return this.line(label, index);
                case CHART_TYPE.BARLINE:
                case CHART_TYPE.BAR:
                    return this.bar(label, index);
                case CHART_TYPE.AREA:
                    return this.area(label, index);
                case CHART_TYPE.COMPOSED:
                    switch(label.type) {
                        case LABEL_TYPE.LINE: return this.line(label, index);
                        case LABEL_TYPE.BAR: return this.bar(label, index);
                        case LABEL_TYPE.AREA: return this.area(label, index);
                        default: return null;
                    }
                default: return null
            }
        });

        let barline = null;
        if (this.state.type === CHART_TYPE.BARLINE) {
            barline = <Line
                type="monotone"
                key={this.state.labels.length}
                dataKey={FLEXIBILITY_ORDER + this.state.line}
                stroke={COLOURS.GOAL}
                fill={COLOURS.GOAL}
                strokeWidth={5}
                dot={false}
                isAnimationActive={false}
            />
        }
        return (
            <div>
                <h5 align="center" className={styles.charttitle}>{this.props.title}</h5>
                <ResponsiveContainer width="95%" aspect={4.0 / size_ratio} minWidth={200} minHeight={100}>
                    <this.state.general_chart
                        props={this.state.type}
                        data={this.state.values}
                        margin={{top: 40, right: 30, left: 50, bottom: 5}}
                        stackOffset="sign"
                    >
                        {X_axis}
                        {Y_axis}
                        <Tooltip/>
                        <CartesianGrid stroke='#f5f5f5'/>
                        <Legend onClick={this.selectBar} height={40} wrapperStyle={{position:'relative', top: -40, right: 0}}/>
                        {comp}
                        {barline}
                    </this.state.general_chart>
                </ResponsiveContainer>
            </div>
        );
    }
}

export default GenericChart;
