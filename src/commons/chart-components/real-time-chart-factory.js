import React from 'react';
import GenericChart from "./generic-chart";
import {AXIS_TYPE, CHART_TYPE, LABELS, LINE_TYPE} from "../constants/chart-constants";
import Parser from "./chart-data-parser";

function init(input) {
    let showingResult = JSON.parse(JSON.stringify(input));

    for (let i = 0; i < showingResult.length; i++) {
        showingResult[i].values = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }

    return showingResult;
}

function generateChartNoUrl(input, callback, fetchResult, title, labels, oyLimits, charType = CHART_TYPE.COMPOSED, key = LABELS.HOUR,
                            oyLabel = LABELS.KWH, oxLabel = LABELS.HOUR, parser = Parser.formatLabeledValues,
                            onDotLineClickFunction = null,
                            lineType = LINE_TYPE.MONOTONE,
                            xAxisType = null,
                            yAxisType = AXIS_TYPE.NUMBER) {

    fetchResult(input);

    const showingResult = init(input);

    let temp = {
        size: showingResult[0].values.length,
        data: showingResult
    };

    let values = parser(temp, key, 0, false);

    let data = {
        dataKey: key,
        oyLabel: oyLabel,
        oxLabel: oxLabel,
        yLimit: oyLimits,
        values: values,
    };
    let chart = (

        <div>
            <GenericChart
                type={charType}
                title={title}
                data={data}
                labels={labels}
                lineType={lineType}
                xAxisType={xAxisType}
                yAxisType={yAxisType}
                onDotLineClickFunction={onDotLineClickFunction}
            />
        </div>);
    callback(chart);
}

function startSimulation(result, callback, title, labels, oyLimits, charType = CHART_TYPE.COMPOSED, key = LABELS.HOUR,
                         oyLabel = LABELS.KWH, oxLabel = LABELS.HOUR, parser = Parser.formatLabeledValues,
                         onDotLineClickFunction = null,
                         lineType = LINE_TYPE.MONOTONE,
                         xAxisType = null,
                         yAxisType = AXIS_TYPE.NUMBER) {

    const showingResult = init(result);

    for (let s = 0; s <= 24; s++) {
        setTimeout(function () {
            changeData(s, result, showingResult, callback, title, labels, oyLimits, charType, key, oyLabel, oxLabel, parser,
                onDotLineClickFunction,
                lineType,
                xAxisType,
                yAxisType)
        }, 1000 * s);
    }
}

function startStepByStepSimulation(s, result, callback, title, labels, oyLimits, charType = CHART_TYPE.COMPOSED, key = LABELS.HOUR,
                                   oyLabel = LABELS.KWH, oxLabel = LABELS.HOUR, parser = Parser.formatLabeledValues,
                                   onDotLineClickFunction = null,
                                   lineType = LINE_TYPE.MONOTONE,
                                   xAxisType = null,
                                   yAxisType = AXIS_TYPE.NUMBER) {

    let showingResult = JSON.parse(JSON.stringify(result));
    const len = showingResult[0].values.length;
    for (let i = 0; i < showingResult.length; i++) {
        showingResult[i].values = [];
    }

    changeData(s, len, result, showingResult, callback, title, labels, oyLimits, charType, key, oyLabel, oxLabel, parser,
        onDotLineClickFunction,
        lineType,
        xAxisType,
        yAxisType);

}

function changeData(s, len, result, showingResult, callback, title, labels, oyLimits, charType = CHART_TYPE.COMPOSED, key = LABELS.HOUR, oyLabel = LABELS.KWH, oxLabel = LABELS.HOUR, parser = Parser.formatLabeledValues,
                    onDotLineClickFunction = null,
                    lineType = LINE_TYPE.MONOTONE,
                    xAxisType = null,
                    yAxisType = AXIS_TYPE.NUMBER) {

    let start = s > 23 ? s - 23 : 0;

    for (let i = 0; i < showingResult.length; i++) {
        for (let j = start; j < s; j++) {
            showingResult[i].values.push(result[i].values[j]);
        }
        for (let j = s; j < 24; j++) {
            showingResult[i].values.push(0);
        }
    }

    let temp = {
        size: 24,
        data: showingResult
    };

    let values = [] = parser(temp, key, start, false);
    let data = {
        dataKey: key,
        oyLabel: oyLabel,
        oxLabel: oxLabel,
        yLimit: oyLimits,
        values: values,
    };
    let chart = (
        <div>
            <GenericChart
                type={charType}
                title={title}
                data={data}
                labels={labels}
                lineType={lineType}
                xAxisType={xAxisType}
                yAxisType={yAxisType}
                onDotLineClickFunction={onDotLineClickFunction}
            />
        </div>);
    callback(chart);
}

export {
    startSimulation, startStepByStepSimulation, generateChartNoUrl
}
