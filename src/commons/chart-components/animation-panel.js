import React from 'react';
import {ResponsiveContainer} from "recharts";
import {Col} from "reactstrap";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import {PlayerIcon} from 'react-player-controls';
import * as styles from '../styles/btn-styles.css';

class AnimationPanel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            startFunction: this.props.startFunction,
            stopFunction: this.props.stopFunction,
            nextStepFunction: this.props.nextStepFunction,
            backgrounds: ['#F2F3F4', '#F2F3F4', '#F2F3F4'],
        };
    }

    componentDidMount() {
    }

    handleClick(func, itter) {
        func();

        let back = this.state.backgrounds;

        for (let i = 0; i < 3; i++)
            back[i] = i === itter ? '#145A32' : '#F2F3F4';

        this.setState({
            backgrounds: back
        })
    }

    handleHover(itter) {
        let back = this.state.backgrounds;

        if (back[itter] !== '#145A32')
            back[itter] = '#239B56';

        this.setState({
            backgrounds: back
        })
    }

    handleMouseOut(itter) {
        let back = this.state.backgrounds;

        if (back[itter] !== '#145A32')
            back[itter] = '#F2F3F4';

        this.setState({
            backgrounds: back
        })
    }

    render() {
        let buttonStyles = [];

        for (let i = 0; i < 3; i++)
            buttonStyles.push({
                appearance: 'none',
                outline: 'none',
                border: 'black',
                borderRadius: 3,
                width: 100,
                height: 40,
                background: this.state.backgrounds[i],
                color: 'blue',
                '&:hover': {
                    'color': 'lightblue',
                }
            });

        return (
            <ResponsiveContainer width="40%" aspect={4.0 / 2.0} minWidth={200} minHeight={100}>
                <Col>
                    <br/>
                    <div className={styles.playerButtons}>
                        <ButtonGroup variant="contained" size="medium" aria-label="small contained button group">

                            <PlayerIcon.Play style={buttonStyles[0]}
                                             onClick={() => this.handleClick(this.state.startFunction, 0)}
                                             onMouseOver={() => this.handleHover(0)}
                                             onMouseOut={() => this.handleMouseOut(0)}
                            />
                            <PlayerIcon.Pause style={buttonStyles[1]}
                                              onClick={() => this.handleClick(this.state.stopFunction, 1)}
                                              onMouseOver={() => this.handleHover(1)}
                                              onMouseOut={() => this.handleMouseOut(1)}
                            />
                            <PlayerIcon.Next style={buttonStyles[2]}
                                             onClick={() => this.handleClick(this.state.nextStepFunction, 2)}
                                             onMouseOver={() => this.handleHover(2)}
                                             onMouseOut={() => this.handleMouseOut(2)}
                            />
                        </ButtonGroup>
                    </div>
                </Col>
            </ResponsiveContainer>
        );
    }
}

export default AnimationPanel;
