function formatLabeledValues(input, key, startTime, special) {
    let formattedData = [];
    for(let h=0; h< input.size; h++){
        let item = {};
        if(special)
            item[key] = Math.floor((startTime + h / 2) % 24) + ':' + (h % 2) * 30;
        else
            item[key] = startTime + h;

        for (let j =0; j < input.data.length ; j++){
            let inputValues = input.data[j];
            if(inputValues.values[h] === 0) {
                item[inputValues.label] = null;
            } else {
                item[inputValues.label] = null;
                if(inputValues.values[h] != null)
                    item[inputValues.label] = inputValues.values[h].toFixed(2);
            }
        }
        formattedData.push(item);
    }
    return formattedData;
}

/*
================================   INPUT FORMAT ==========================================
    {
     size:24,
     data: [
            { label: "production",
                values: [3223, 5454, 4534, 3242, 4534, 5435, 4654, 6546, 7655, 5653,3223, 5454, 4534, 3242, 4534, 5435, 4654, 6546, 7655, 5653, 4654, 6546, 7655, 5653]
            },
            { label: "consumption",
                values: [1223, 3454, 2534, 2242, 1534, 4435, 5654, 7546, 4655, 4653,2223, 4454, 7534, 8242, 2534, 3435, 6654, 7546, 9655, 3653, 2654, 1546, 3655, 2653]
            }
            ]
        }
 */


/*
================================   OUTPUT FORMAT ==========================================
   [
        { hour: 0, production: 4000, consumption: 2400 },
        { hour: 1, production: 3000, consumption: 1398 },
        { hour: 2, production: 2000, consumption: 9800},
        { hour: 3, production: 2780, consumption: 3908 },
        { hour: 4, production: 1890, consumption: 0 },
        { hour: 5, production: 2390, consumption: 0 },
        { hour: 6, production: 3490, consumption: 0 },
        { hour: 7, production: 4000, consumption: 2400 },
        { hour: 9, production: 3000, consumption: 1398 },
        { hour: 10, production: 2000, consumption: 9800 },
        { hour: 11, production: 2780, consumption: 3908 },
        { hour: 12, production: 1890, consumption: 4800 },
        { hour: 13, production: 2390, consumption: 3800 },
        { hour: 14, production: 3490, consumption: 4300 },
        { hour: 15, production: 1890, consumption: 4800 },
        { hour: 16, production: 2390, consumption: 3800 },
        { hour: 17, production: 3490, consumption: 4300 },
        { hour: 18, production: 4000, consumption: 2400 },
        { hour: 19, production: 3000, consumption: 1398 },
        { hour: 20, production: 2000, consumption: 9800 },
        { hour: 21, production: 2780, consumption: 3908 },
        { hour: 22, production: 1890, consumption: 4800 },
        { hour: 23, production: 2390, consumption: 3800 }
    ]};
 */
function getDataSetForBarGraph(data) {
    let result = [];
    var map = new Map();
    for(let i = 0 ; i < data.length; i++) {
        let currentDataKeyValue = data[i].dataKeyValue;
        if(map.get(currentDataKeyValue) === undefined) {
            map.set(currentDataKeyValue, result.length);
            let obj = {};
            obj[data[i].dataKey] = currentDataKeyValue;
            result.push(obj);
        }
        result[map.get(currentDataKeyValue)][data[i].label] = data[i].value;
    }

    return result;
}

function getDataSetForAreaChart(data) {
    let result = [];
    let labels = new Map();
    if(data.length < 1) {
        return [];
    }
    for(let i = 0 ; i < data.length; i++) {
        if(labels.get(data[i].label) === undefined) {
            let suplimentaryObj = {};
            suplimentaryObj[data[i].dataKey] = 0;
            suplimentaryObj[data[i].label] = data[i].value;
            result.push(suplimentaryObj);
            labels.set(data[i].label, 0);
        }
        let obj = {};
        obj[data[i].dataKey] = data[i].dataKeyValue;
        obj[data[i].label] = data[i].value;
        result.push(obj);
    }

    return result;
}

function parseDataLabel(label, values, MAX) {
    if (values) {
        return ({
            label: label,
            values: values.map(element => {
                return (+element.toFixed(2))
            })
        })
    } else {
        return ({
            label: label,
            values: Array(MAX).fill(0)
        })
    }
}

module.exports = {
    formatLabeledValues,
    getDataSetForBarGraph,
    getDataSetForAreaChart,
    parseDataLabel
};
