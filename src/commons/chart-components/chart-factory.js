import * as RestApiClient from "../api/rest-client";
import React from 'react';
import APIResponseErrorMessage from "../errorhandling/api-response-error-message";
import GenericChart from "./generic-chart";
import {AXIS_TYPE, CHART_TYPE, LABELS, LINE_TYPE} from "../constants/chart-constants";
import Parser from "./chart-data-parser";

function generateChart(url, callback, title, labels, oyLimits, charType = CHART_TYPE.COMPOSED, key = LABELS.HOUR,
                       oyLabel = LABELS.KWH, oxLabel = LABELS.HOUR, parser = Parser.formatLabeledValues) {

    RestApiClient.performRequest(url, (result, status, error) => {
        if (result !== null && status === 200) {
            let values = parser(result, key);
            let data = {
                dataKey: key,
                oyLabel: oyLabel,
                oxLabel: oxLabel,
                yLimit: oyLimits,
                values: values
            };
            let chart = (
                <div><GenericChart
                    type={charType}
                    title={title}
                    data={data}
                    labels={labels}
                />
                </div>);
            callback(chart);
        } else {
            let errDiv = (<div>
                <APIResponseErrorMessage errorStatus={status} error={error}/>
            </div>);
            callback(errDiv);
        }
    });
}

function generateChartMoreParamsNoUrl(input, callback, title, labels, oyLimits, charType = CHART_TYPE.COMPOSED, key = LABELS.HOUR,
                                      oyLabel = LABELS.KWH, oxLabel = LABELS.HOUR, parser = Parser.formatLabeledValues,
                                      onDotLineClickFunction = null,
                                      lineType = LINE_TYPE.MONOTONE,
                                      xAxisType = null,
                                      yAxisType = AXIS_TYPE.NUMBER) {
    let values = parser(input, key);
    let data = {
        dataKey: key,
        oyLabel: oyLabel,
        oxLabel: oxLabel,
        yLimit: oyLimits,
        values: values
    };
    let chart = (
        <div><GenericChart
            type={charType}
            title={title}
            data={data}
            labels={labels}
            lineType={lineType}
            xAxisType={xAxisType}
            yAxisType={yAxisType}
            onDotLineClickFunction={onDotLineClickFunction}
        />
        </div>);
    callback(chart);
}

export {
    generateChart, generateChartMoreParamsNoUrl
}
