import React, {Component} from "react";
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import {Col, Row} from "react-bootstrap";

class Table extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data,
            columns: props.columns,
            search: props.search,
            filters: [],
            getTrPropsFunction: props.getTrProps,
            pageSize: props.pageSize || 10,
        };
    }

    search() {

    }

    filter(data) {
        let accepted = true;

        this.state.filters.forEach(val => {
            if (!String(data[val.accessor]).includes(String(val.value)) && !String(val.value).includes(String(data[val.accessor]))) {
                accepted = false;
            }
        });

        return accepted;
    }

    getTRPropsType(state, rowInfo) {
        if (rowInfo) {
            const {original} = rowInfo;
            const {color} = original;

            return {
                style: {
                    background: color,
                    textAlign: "center"
                }
            };
        }
        else
            return {};
    }

    render() {
        let data = this.state.data ? this.state.data.filter(data => this.filter(data)) : [];
        return (
            <div>
                <Row>
                    <Col>
                        <ReactTable
                            data={data}
                            resolveData={data => data.map(row => row)}
                            columns={this.state.columns}
                            defaultPageSize={this.state.pageSize}
                            getTrProps={this.getTRPropsType}
                            showPagination={false}
                            style={{
                                height: '300px'
                            }}
                            noDataText={''}
                            NoDataComponent={NoDataComponent}
                        />
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Table;

const NoDataComponent = (props) => {
    const {
        children
    } = props;

    return children === '' ?
        null :
        (
            <div className="rt-noData">
                {children}
            </div>
        );
};
