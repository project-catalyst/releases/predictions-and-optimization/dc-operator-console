export const COLOURS = {
    DSO: {
        PRODUCTION: "#80ca2d",
        CONSUMPTION: "#ca1827",
        FLEXIBILITY_REQUEST: "#5b51d8",
        REWARD: "#a958d8",
    },
    AGG1:{
        OFFER: "#30cabb",
    },
    AGG2:{
        OFFER: "#89ca39",
    },

    DEP1:{
        FLEX_ORDER: "#02547D",

        BASELINE:   "#88ca92",
        BELOW:      "#ca1827",
        ABOVE:      "#5b51d8",
        REQUEST:    "#a958d8",

        IMBALANCE_PLUS: "#2384D9",
        IMBALANCE_MINUS: "#2384D9",

        MONITORED:  "#D98825",
    },
    DEP2:{
        FLEX_ORDER: "#0285A8",

        BASELINE:   "#b7ea62",
        BELOW:      "#ca1827",
        ABOVE:      "#5b51d8",
        REQUEST:    "#a958d8",

        IMBALANCE_PLUS: "#033859",
        IMBALANCE_MINUS: "#033859",
        MONITORED:  "#A63387",
    },
    DEP3:{
        FLEX_ORDER: "#3265ca",

        BASELINE:   "#05b23a",
        BELOW:      "#ca1827",
        ABOVE:      "#5b51d8",
        REQUEST:    "#a958d8",

        IMBALANCE_PLUS: "#18608C",
        IMBALANCE_MINUS: "#18608C",
        MONITORED:  "#D984BB"
    },
    DEP4:{
        FLEX_ORDER: "#02BEC4",

        BASELINE:   "#158e44",
        BELOW:      "#ca1827",
        ABOVE:      "#5b51d8",
        REQUEST:    "#D9A3BC",

        IMBALANCE_PLUS: "#369AD9",
        IMBALANCE_MINUS: "#369AD9",
        MONITORED:  "#D9A3BC"
    },
    DEP5:{
        FLEX_ORDER: "#A9E8DC",

        BASELINE:   "#baca22",
        BELOW:      "#ca1827",
        ABOVE:      "#5b51d8",
        REQUEST:    "#a958d8",

        IMBALANCE_PLUS: "#A7D5F2",
        IMBALANCE_MINUS: "#A7D5F2",
        MONITORED:  "#D9C1C5"
    },
    DEP6:{
        FLEX_ORDER: "#128987",

        BASELINE:   "#a8e6a0",
        BELOW:      "#ca1827",
        ABOVE:      "#5b51d8",
        REQUEST:    "#a958d8",

        IMBALANCE_PLUS: "#62c2e7",
        IMBALANCE_MINUS: "#62c2e7",
        MONITORED:  "#a96863"
    },

    VPP: {
        GOAL: "#00C980",

        PROSUMER: ["#B4DCED", "#5E9BE8", "#69D3FF", "#748DFF",
            "#6A5EE8", "#9E67FF", "#91C4D9", "#011F26",
            "#4BAABF", "#037F8C", "#025159", "#038C7F",
            "#025959", "#013440", "#024059", "#B0C6D9"],
        GENERATION: "#014017",
        DEMAND: "#4CB5F5"
    },

    VPP_FLEXIBILITY: {
        "Baseline": "#1D3770",
        "Flexibility Above": "#67268C",
        "Flexibility Below": "#4D4487"
    },

    OPTIMIZATION_PAGES: {
        Delay_Tolerable: "#f87068",
        Real_Time: '#f80c23',
        Cooling: '#55a4f8',
        Generator: "#4b3c22",
        Battery_Charge: "#1aea59",
        Battery_Discharge: "#b2ea9f",
        TES_Charge: "#1a6be1",
        TES_Discharge: "#94d6ec",
        Provided_Grid_Energy: "#eddbba",
        Relocate_Values: "#8606bb",
        Host_Values: "#d4afe1",
        Flex_Order: "#128987",
        ADAPTED: "#00C980",
        BASELINE: "#014017",
        ORDER: "#4CB5F5",
        GREEN: "#18f50e",
        Renewable_Energy: "#d0f54c"

    },

    DC_ENERGY_PROFILE: {
        "Delay Tolerable": "#f87068",
        "Real Time": '#f80c23',
        "Cooling": '#55a4f8',
        "Generator": "#4b3c22",
        "Battery Charge": "#1aea59",
        "Battery Discharge": "#b2ea9f",
        "TES Charge": "#1a6be1",
        "TES Discharge": "#94d6ec",
        "Provided Grid Energy": "#eddbba",
        "Green Energy": "#3fa842",
        "Relocate Values": "#8606bb",
        "Host Values": "#d4afe1",

    },

    FLEXIBILITY_STRATEGY: {
        "drSig" : "#1a6be1",
        "heat" : "#c60d0d",
        "ePrice" : "#22e00d",
        "lPrice" : "#e1c01a",
        "renewableEnergy" : "#e1c01a"
    }
};
