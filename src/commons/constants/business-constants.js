
export const PredictionGranularity = {
    DAYAHEAD: {
        value: 'DAYAHEAD',
        sampleFreqMin: 60,
    },
    INTRADAY: {
        value: 'INTRADAY',
        sampleFreqMin: 30,
    },
    NEAR_REAL_TIME: {
        value: 'NEAR_REAL_TIME',
        sampleFreqMin: 10,
    },
    THERMAL_NEAR_REAL_TIME:{
        value: 'THERMAL_NEAR_REAL_TIME',
        sampleFreqMin: 10, /*SECONDS*/
    }
};

export const ValuesType = {
    MONITORED: 1,
    PREDICTED: 2
};

export const TimeUnitsInMillis = {
    DAY: 86400000,
    FOUR_HOURS: 14400000,
};
