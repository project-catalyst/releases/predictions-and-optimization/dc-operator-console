export const LABELS = {
    KWH: 'kWh',
    HOUR: 'hour',
    PRICE: '$',
    TIMESTAMP: 'timestamp',
    MIN: 'min',
    SEC: 'sec',
    TIME_5_MIN: 'Time (5 min)',
    CELSIUS: '°C'
};
export const CHART_TYPE = {
    LINE: 'Line',
    AREA: 'Area',
    BAR: 'Bar',
    BARLINE: 'Barline',
    COMPOSED: 'Composed'
};

export const LABEL_TYPE = {
    LINE: 'line',
    AREA: 'area',
    BAR: 'bar',
    COMPOSED: 'composed'
};

export const AXIS_TYPE = {
    NUMBER: 'number',
};

export const LINE_TYPE = {
    MONOTONE: 'monotone',
    STEP_AFTER: 'stepAfter',
    STEP_BEFORE: 'stepBefore',
};
