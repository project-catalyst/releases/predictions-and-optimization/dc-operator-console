import React, {Component} from "react";
import {
    Bar,
    BarChart,
    Brush,
    CartesianGrid,
    Legend,
    ReferenceLine,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis
} from "recharts";
import {LABELS} from "../constants/chart-constants";
import * as HISTORY_API from "../../electrical/monitoring/api/history-api";
import * as PREDICTION_API from "../../electrical/prediction/api/prediction-api";
import Parser from "./graph-data-parser";
import styles from '../styles/catalyst-style.css';
import {Card} from "reactstrap";
import {ValuesType} from "../constants/business-constants";
import Cell from "recharts/es6/component/Cell";
import {CONFIG, GRAPH_CONFIG} from "../../config"

const GRAPH_CONSTANTS = {
    dataKey: LABELS.TIMESTAMP,
    oyLabel: LABELS.KWH,
    oxLabel: LABELS.MIN,
    Y_LIMIT:[GRAPH_CONFIG[CONFIG.PILOT].Y_LIMIT_MONITORING]
};


class ProductionConsumptionNRT extends Component {
    constructor(props) {
        super(props);

        this.state = {
            values: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };

        this.getDataAndShiftLeft = this.getDataAndShiftLeft.bind(this);
    }

    getDataAndShiftLeft(values, startTime, granularity, dataKey, dataCenterId) {
        let formattedResult = Parser.getFormattedData(values, startTime, granularity, dataKey).slice(0, -1);
        let predictedNRT = {
            timestamp: "23:00",
            consumption: 0,
            production: 0
        };
        HISTORY_API.fetchDataCenterNRTPrediction(granularity, dataCenterId, startTime, (result, status, err) => {
            if (result !== null && status === 200) {
                predictedNRT.consumption = result.energyValues[0];
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
                console.log(err);
            }
        });
        formattedResult.push(predictedNRT);
        return formattedResult;
    }

    componentDidMount() {
        this.fetchDataCenterProductionConsumptionData(this.props.granularity,
            this.props.valuesType,
            this.props.dataCenterId,
            this.props.startTime)
    }

    fetchDataCenterProductionConsumptionData(granularity, valuesType, dataCenterId, startTime) {

        let api = (valuesType === ValuesType.MONITORED) ?
            HISTORY_API :
            PREDICTION_API;

        api.fetchDataCenterProductionConsumptionData(granularity, dataCenterId, startTime,
            (result, status, err) => {
                if (result !== null && status === 200) {
                    this.setState({
                        values: this.getDataAndShiftLeft(result, Number(startTime), granularity.sampleFreqMin, GRAPH_CONSTANTS.dataKey, dataCenterId),
                        isLoaded: true
                    });
                    console.log(this.state.values);
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                    console.log(err);
                }
            });
    }

    render() {

        let isMonitored = this.props.valuesType === ValuesType.MONITORED;

        let xAxis =
            <XAxis dataKey={GRAPH_CONSTANTS.dataKey} label={{
                value: GRAPH_CONSTANTS.oxLabel,
                offset: 0,
                position: "insideBottomRight"
            }}/>;

        let yAxis =
            <YAxis
                domain={[0, Number(GRAPH_CONSTANTS.Y_LIMIT)]}
                label={{value: GRAPH_CONSTANTS.oyLabel, angle: -90, position: 'insideLeft'}}/>;


        return (
            <Card body>
                <h5 className={styles.chartTitle}>{this.props.title}</h5>
                <ResponsiveContainer width="95%" height={400}>
                    <BarChart width={700} height={400} data={this.state.values}>
                        <CartesianGrid strokeDasharray="3 3"/>

                        {xAxis}
                        {yAxis}

                        <Legend verticalAlign="top" wrapperStyle={{lineHeight: '40px'}}/>
                        <Tooltip/>
                        <ReferenceLine y={0} stroke='#000'/>

                        {isMonitored &&
                        <Brush dataKey={GRAPH_CONSTANTS.dataKey} height={30} offset={-10} stroke="#9e9e9e"/>}

                        <Bar dataKey="production" stackId="1" fill="#4caf50">
                            {this.state.values.map((entry, index) => (
                                <Cell fill={index === 0 ? "#1b5e20" : "#4caf50"} opacity={1}/> &&
                                <Cell fill={index === 23 ? "#1b5e20" : "#4caf50"} opacity={1}/>
                            ))}
                        </Bar>
                        <Bar dataKey="consumption" stackId="1" fill="#ff9800">
                            {this.state.values.map((entry, index) => (
                                <Cell fill={index === 0 ? "#800000" : "#ff9800"} opacity={.8}/> &&
                                <Cell fill={index === 23 ? "#800000" : "#ff9800"} opacity={.8}/>
                            ))}
                        </Bar>
                    </BarChart>
                </ResponsiveContainer>
            </Card>
        )
    }
}

export default ProductionConsumptionNRT
