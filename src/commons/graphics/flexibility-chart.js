import React, {Component} from "react";
import {Card} from "reactstrap";
import styles from "../styles/catalyst-style.css";
import {
    Bar,
    BarChart,
    CartesianGrid,
    Legend,
    ReferenceLine,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis
} from "recharts";
import {LABELS} from "../constants/chart-constants";
import * as FLEXIBILITY_API from "../../electrical/flexibility/api/flexibility-api";
import Parser from "./graph-data-parser";
import {ValuesType} from "../constants/business-constants";
import {CONFIG, GRAPH_CONFIG} from "../../config"

const GRAPH_CONSTANTS = {
    dataKey: LABELS.HOUR,
    oxLabel: LABELS.HOUR,
    oyLabel: LABELS.KWH,
    domainUpperBound:800,
    BAR_GAP: 4,
    BAR_SIZE: 20,
    Y_LIMIT:[GRAPH_CONFIG[CONFIG.PILOT].Y_LIMIT_MONITORING]
}

class FlexibilityChart extends Component {

    constructor(props) {
        super(props);

        this.state = {
            values: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        }
    }

    componentDidMount() {
        this.fetchDataCenterElectricalFlexibilityData(
            this.props.granularity,
            this.props.dataCenterId,
            this.props.startTime);
    }

    fetchDataCenterElectricalFlexibilityData(granularity, dataCenterId, startTime) {
        FLEXIBILITY_API.fetchDataCenterElectricalFlexibilityData(granularity, dataCenterId, startTime,
            (result, status, err) => {
                if (result !== null && status === 200) {
                    this.setState({
                        values: Parser.getFormattedData(result, Number(startTime), granularity.sampleFreqMin, GRAPH_CONSTANTS.dataKey),
                        isLoaded: true
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                    console.log(err);
                }
            });
    }

    render() {

        let isPredicted = this.props.valuesType === ValuesType.PREDICTED;

        let xAxis =
            <XAxis dataKey={GRAPH_CONSTANTS.dataKey}
                   label={{value: GRAPH_CONSTANTS.oxLabel, offset: 0, position: "insideBottomRight"}}/>;

        let yAxis =
            <YAxis domain={[-GRAPH_CONSTANTS.Y_LIMIT[Number(localStorage.getItem('scenario'))], GRAPH_CONSTANTS.Y_LIMIT[Number(localStorage.getItem('scenario'))]]}
                label={{value: GRAPH_CONSTANTS.oyLabel, angle: -90, position: 'insideLeft'}}/>;


        return (

            <Card body>
                <h5 className={styles.chartTitle}> {this.props.title} </h5>
                <ResponsiveContainer width="100%" height={300}>
                <BarChart data={this.state.values}
                    width={1700} height={300}
                    barGap={GRAPH_CONSTANTS.BAR_GAP}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}>

                    <CartesianGrid strokeDasharray="3 3"/>

                    {xAxis}
                    {yAxis}

                    <Legend/>
                    <Tooltip/>
                    <ReferenceLine y={0} stroke="#000"/>
                    {isPredicted}
                    <Bar dataKey="lower" fill="#ffc107"
                        barSize={GRAPH_CONSTANTS.BAR_SIZE}
                        transform={`translate(${(GRAPH_CONSTANTS.BAR_GAP + GRAPH_CONSTANTS.BAR_SIZE) / 2}, 0)`}/>
                    <Bar dataKey="upper" fill="#ff9800"
                        barSize={GRAPH_CONSTANTS.BAR_SIZE}
                        transform={`translate(${-(GRAPH_CONSTANTS.BAR_GAP + GRAPH_CONSTANTS.BAR_SIZE) / 2}, 0)`}/>
                </BarChart>
                </ResponsiveContainer>
            </Card>
        )
    }
}

export default FlexibilityChart
