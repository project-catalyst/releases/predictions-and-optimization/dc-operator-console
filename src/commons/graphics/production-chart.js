import React, {Component} from "react";
import {
    Area,
    AreaChart,
    Brush,
    CartesianGrid,
    Legend,
    ReferenceLine,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis
} from "recharts";
import {LABELS} from "../constants/chart-constants";
import * as HISTORY_API from "../../thermal/monitoring/api/history-api";
import * as PREDICTION_API from "../../thermal/prediction/api/prediction-api";
import Parser from "./graph-data-parser";
import styles from '../styles/catalyst-style.css';
import {Card} from "reactstrap";
import {ValuesType} from "../constants/business-constants";
import {CONFIG, GRAPH_CONFIG} from "../../config"

const GRAPH_CONSTANTS = {
    dataKey: LABELS.TIMESTAMP,
    oyLabel: LABELS.KWH,
    oxLabel: LABELS.HOUR,
    //Y_LIM : [2000, 850, 850]
    Y_LIMIT:[GRAPH_CONFIG[CONFIG.PILOT].Y_LIMIT_MONITORING]
};

class ProductionChart extends Component {

    constructor(props) {
        super(props);

        this.state = {
            values: [],

            isLoaded: false,
            errorStatus: 0,
            error: null
        }
    }

    componentDidMount() {
        this.fetchDataCenterProductionData(this.props.granularity,
            this.props.valuesType,
            this.props.dataCenterId,
            this.props.startTime)
    }

    fetchDataCenterProductionData(granularity, valuesType, dataCenterId, startTime) {

        let api = (valuesType === ValuesType.MONITORED) ?
            HISTORY_API :
            PREDICTION_API;

        api.fetchDataCenterProductionData(granularity, dataCenterId, startTime,
            (result, status, err) => {
                if (result !== null && status === 200) {
                    this.setState({
                        values: Parser.getFormattedData(result, Number(startTime), granularity.sampleFreqMin, GRAPH_CONSTANTS.dataKey),
                        isLoaded: true
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                    console.log(err);
                }
            });
    }

    render() {

        let isMonitored = this.props.valuesType === ValuesType.MONITORED;

        let xAxis =
            <XAxis dataKey={GRAPH_CONSTANTS.dataKey} label={{
                value: GRAPH_CONSTANTS.oxLabel,
                offset: 0,
                position: "insideBottomRight"
            }}/>;

        let yAxis =
            <YAxis
                domain={[0, GRAPH_CONSTANTS.Y_LIMIT[Number(localStorage.getItem('scenario'))]]}
                label={{value: GRAPH_CONSTANTS.oyLabel, angle: -90, position: 'insideLeft'}}/>;


        return (
            <Card body>
                <h5 className={styles.chartTitle}>{this.props.title}</h5>
                <ResponsiveContainer  width="95%" height={400}>
                <AreaChart width={800} height={400} data={this.state.values}>
                    <CartesianGrid strokeDasharray="3 3"/>
                    {xAxis}
                    {yAxis}
                    <Legend verticalAlign="top" wrapperStyle={{lineHeight: '40px'}}/>
                    <Tooltip/>
                    <ReferenceLine y={0} stroke='#000'/>
                    {isMonitored &&
                    <Brush dataKey={GRAPH_CONSTANTS.dataKey} height={30} offset={-10} stroke="#9e9e9e"/>}

                    <Area type="monotone" fill="#dd2c00" fillOpacity={.5} stroke="#dd2c00" activeDot={{r: 8}}
                          dataKey="production"/>
                </AreaChart>
                </ResponsiveContainer>
            </Card>
        )
    }
}

export default ProductionChart
