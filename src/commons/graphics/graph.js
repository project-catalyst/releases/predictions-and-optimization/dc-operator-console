import React, {Component} from "react";
import {
    Area,
    AreaChart,
    Bar,
    BarChart,
    CartesianGrid,
    ComposedChart,
    Label,
    Legend,
    Line,
    LineChart,
    Tooltip,
    XAxis,
    YAxis
} from "recharts";
import {CHART_TYPE, LABEL_TYPE} from "../constants/chart-constants";
import styles from '../styles/catalyst-style.css';

/*
   <Graph
        type= ["Composed"/"Line"/"Bar"/"Area"]
        title="Whatever Title"
        data= {
            dataKey: "key",
            oyLabel: "oyLabel",
            oxLabel: "oxLabel",
            yLimit: [min, max],
            values: [{key: 0, label_key1: 4000, label_key2: 2400 ...}{}..]
        labels=[{key: label_key1, color: color, type: ["line", "area", "bar"] // type will be used only for the Composed}]
    />

*/

class Graph extends Component {
    constructor(props) {
        super(props);
        this.selectBar = this.selectBar.bind(this);

        let general_graph;
        if (this.props.type === CHART_TYPE.LINE)
            general_graph = LineChart;
        else if (this.props.type === CHART_TYPE.BAR)
            general_graph = BarChart;
        else if (this.props.type === CHART_TYPE.COMPOSED)
            general_graph = ComposedChart;
        else if (this.props.type === CHART_TYPE.AREA)
            general_graph = AreaChart;

        this.state = {
            general_graph: general_graph,
            type: this.props.type,
            title: this.props.title,
            dataKey: this.props.data.dataKey,
            oxLabel: this.props.data.oxLabel,
            oyLabel: this.props.data.oyLabel,
            values: this.props.data.values,
            yLimit: this.props.data.yLimit,
            labels: this.props.labels
        }
    }

    selectBar(event) {
        let updatedLabels = [];
        for (let i = 0; i < this.state.labels.length; i++) {
            let label = this.state.labels[i];
            if (label.key !== event.dataKey) {
                updatedLabels.push(label);
            } else {
                if (/\s/.test(label.key)) {
                    let newLabel = {key: label.key.trim(), color: label.color, type: label.type};
                    updatedLabels.push(newLabel);
                } else {
                    let newLabel = {key: label.key + " ", color: label.color, type: label.type};
                    updatedLabels.push(newLabel);
                }
            }
        }
        this.setState({
            labels: updatedLabels
        });
    }

    render() {
        let X_axis = <XAxis dataKey={this.state.dataKey}>
            <Label
                value={this.state.oxLabel}
                position="insideBottomRight"
                dy={10}
                dx={20}
            />
        </XAxis>;

        let Y_axis = <YAxis type="number" domain={this.state.yLimit}>
            <Label
                value={this.state.oyLabel}
                position="insideTopLeft"
                dy={-30}
            />
        </YAxis>;

        let comp = this.state.labels.map((label, index) => {
            if (this.state.type === CHART_TYPE.LINE || (label.type === LABEL_TYPE.LINE && this.state.type === CHART_TYPE.COMPOSED)) {
                return (<Line
                    type="monotone"
                    key={index}
                    dataKey={label.key}
                    stroke={label.color}
                    fill={label.color}
                />)
            } else {
                if (this.state.type === CHART_TYPE.BAR || (label.type === LABEL_TYPE.BAR && this.state.type === CHART_TYPE.COMPOSED)) {
                    return (<Bar
                        key={index}
                        dataKey={label.key}
                        fill={label.color}
                        stackId={this.state.dataKey}/>)
                } else {

                    if (this.state.type === CHART_TYPE.AREA || (label.type === LABEL_TYPE.AREA && this.state.type === CHART_TYPE.COMPOSED)) {
                        return (<Area
                            type="monotone"
                            dataKey={label.key}
                            fill={label.color}
                            stroke={label.color}
                        />)
                    } else {
                        return null;
                    }
                }
            }
        });
        return (
            <div>
                <h5 align="center" className={styles.chartTitle}>{this.props.title}</h5>
                <this.state.general_graph
                    props={this.state.type}
                    width={700}
                    height={300}
                    data={this.state.values}
                    margin={{top: 30, right: 30, left: 20, bottom: 5}}
                >
                    {X_axis}
                    {Y_axis}
                    <Tooltip/>
                    <CartesianGrid stroke='#f5f5f5'/>
                    <Legend onClick={this.selectBar}/>
                    {comp}
                </this.state.general_graph>
            </div>
        );
    }
}

export default Graph;
