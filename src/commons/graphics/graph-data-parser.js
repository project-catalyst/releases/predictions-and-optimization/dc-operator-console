function getFormattedData(input, startTime, min, key) {

    let formattedData = [];
    let itemTimestamp = new Date(startTime).getUTCMilliseconds();
    for(let h=0; h< input.size; h++){
        let item = {};
        let timestamp = new Date(itemTimestamp + h * min * 60000);

        item[key] = ("0" + timestamp.getUTCHours()).slice(-2) + ':' + ("0" + timestamp.getUTCMinutes()).slice(-2);
        for (let j =0; j < input.data.length ; j++){
            let inputValues = input.data[j];
            item[inputValues.label] = inputValues.values[h].toFixed(2);
        }
        formattedData.push(item);
    }
    console.log(formattedData);
    return formattedData;
}


module.exports = {
    getFormattedData
};


/*
================================   INPUT FORMAT ==========================================
    {
     size:24,
     data: [
            { label: "production",
                values: [3223, 5454, 4534, 3242, 4534, 5435, 4654, 6546, 7655, 5653,3223, 5454, 4534, 3242, 4534, 5435, 4654, 6546, 7655, 5653, 4654, 6546, 7655, 5653]
            },
            { label: "consumption",
                values: [1223, 3454, 2534, 2242, 1534, 4435, 5654, 7546, 4655, 4653,2223, 4454, 7534, 8242, 2534, 3435, 6654, 7546, 9655, 3653, 2654, 1546, 3655, 2653]
            }
            ]
        }
 */


/*
================================   OUTPUT FORMAT ==========================================
   [
        { hour: 0, production: 4000, consumption: 2400 },
        { hour: 1, production: 3000, consumption: 1398 },
        { hour: 2, production: 2000, consumption: 9800},
        { hour: 3, production: 2780, consumption: 3908 },
        { hour: 4, production: 1890, consumption: 0 },
        { hour: 5, production: 2390, consumption: 0 },
        { hour: 6, production: 3490, consumption: 0 },
        { hour: 7, production: 4000, consumption: 2400 },
        { hour: 9, production: 3000, consumption: 1398 },
        { hour: 10, production: 2000, consumption: 9800 },
        { hour: 11, production: 2780, consumption: 3908 },
        { hour: 12, production: 1890, consumption: 4800 },
        { hour: 13, production: 2390, consumption: 3800 },
        { hour: 14, production: 3490, consumption: 4300 },
        { hour: 15, production: 1890, consumption: 4800 },
        { hour: 16, production: 2390, consumption: 3800 },
        { hour: 17, production: 3490, consumption: 4300 },
        { hour: 18, production: 4000, consumption: 2400 },
        { hour: 19, production: 3000, consumption: 1398 },
        { hour: 20, production: 2000, consumption: 9800 },
        { hour: 21, production: 2780, consumption: 3908 },
        { hour: 22, production: 1890, consumption: 4800 },
        { hour: 23, production: 2390, consumption: 3800 }
    ]};
 */
