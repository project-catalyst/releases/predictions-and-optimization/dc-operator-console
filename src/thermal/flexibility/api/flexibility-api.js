import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";
import {PredictionGranularity} from "../../../commons/constants/business-constants";
import {CONGIG} from "../../../config";;

const ENDPOINT = {

    // Electrical Production-Consumption for DC
    DAYAHEAD_PRODUCTION_CONSUMPTION_HISTORY_THERMAL_DC:
        '/graph-data/thermal/flexibility/dayahead',
    INTRADAY_PRODUCTION_CONSUMPTION_HISTORY_THERMAL_DC:
        '/graph-data/thermal/flexibility/intraday',
    NEAR_REAL_TIME_PRODUCTION_CONSUMPTION_HISTORY_THERMAL_DC:
        '/graph-data/thermal/flexibility/nearRealTime',

};

function fetchDataCenterThermalFlexibilityData(granularity, dataCenterId, startTime, callback){

    let urlPath;
    switch (granularity) {
        case PredictionGranularity.DAYAHEAD:
            urlPath = ENDPOINT.DAYAHEAD_PRODUCTION_HISTORY_THERMAL_DC;
            break;
        case PredictionGranularity.INTRADAY:
            urlPath = ENDPOINT.INTRADAY_PRODUCTION_HISTORY_THERMAL_DC;
            break;
        case PredictionGranularity.NEAR_REAL_TIME:
            urlPath = ENDPOINT.NEAR_REAL_TIME_PRODUCTION_HISTORY_THERMAL_DC;
            break;
    }


    urlPath = urlPath + '/' + startTime;

    let request = new Request(HOST.CATALYST_DB_API + urlPath, { method: 'GET'});

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    fetchDataCenterThermalFlexibilityData
}
