import React from 'react';
import styles from '../../commons/styles/catalyst-style.css';
import {Badge} from "reactstrap";

class ThermalFlexibilityContainer extends React.Component {

    render() {

        return (
            <div className={styles.card}>
                <h3> <Badge color="danger">Thermal Energy Flexibility</Badge></h3>
                <div className={styles.body}>
                    Under Construction
                </div>
            </div>
        )
    };
}

export default ThermalFlexibilityContainer
