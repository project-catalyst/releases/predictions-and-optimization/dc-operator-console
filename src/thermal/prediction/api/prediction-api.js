import {HOST} from "../../../commons/hosts";
import RestApiClient from "../../../commons/api/rest-client"
import {PredictionGranularity} from "../../../commons/constants/business-constants";
import {CONFIG} from "../../../config";

function fetchDataCenterProductionData(granularity, dataCenterId, startTime, callback) {

    let urlPath;
    switch (granularity) {
        case PredictionGranularity.DAYAHEAD:
            urlPath = '/graph-data/thermal/prediction/production/dayahead';
            break;
        case PredictionGranularity.INTRADAY:
            urlPath = '/graph-data/thermal/prediction/production/intraday';
            break;
        /*case PredictionGranularity.NEAR_REAL_TIME:
            urlPath = '/graph-data/thermal/prediction/production/nearRealTime';
            break;*/
    }

    urlPath = urlPath + '/' + startTime;


    let request = new Request(HOST.CATALYST_DB_API + urlPath, { method: 'GET'});

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function fetchThermalNearRealTimeSimulation(rack_1, rack_2, room_temp, air_flow, air_temp, callback){

    let urlPath = '/thermal/prediction/near-real-time-thermal-prediction';
    let room_temp_scaled = room_temp + 273.15;
    let air_temp_scaled = air_temp + 273.15;
    urlPath = urlPath + '/' + rack_1 + '/' + rack_2 + '/' + room_temp_scaled + '/' + air_flow + '/' + air_temp_scaled;
    let request = new Request(HOST.CATALYST_EBB_THERMAL + urlPath, { method: 'GET'});
    console.log(request);

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    fetchDataCenterProductionData,
    fetchThermalNearRealTimeSimulation
}
