import React from 'react';
import styles from '../../commons/styles/catalyst-style.css';
import {Card, CardHeader, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane} from 'reactstrap';
import classnames from 'classnames';
import {PredictionGranularity} from "../../commons/constants/business-constants";
import ThermalPredictionTab from "./thermal-prediction-tab";
import ThermalPredictionWithoutFlexibilityTab from "./thermal-prediction-without-flexibility-tab"
import Slider from 'react-rangeslider';
import 'react-rangeslider/lib/index.css';
import {fetchThermalNearRealTimeSimulation} from "./api/prediction-api";
import Parser from "../../commons/graphics/graph-data-parser";
import {LABELS} from "../../commons/constants/chart-constants";
import * as PREDICTION_API from "../../thermal/prediction/api/prediction-api";
import {
    Brush,
    CartesianGrid,
    Legend,
    Line,
    LineChart,
    ReferenceLine,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis
} from "recharts";
import 'react-day-picker/lib/style.css';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import dateFnsFormat from 'date-fns/format';
import dateFnsParse from 'date-fns/parse';
import {DateUtils} from 'react-day-picker';
import {CONFIG, GRAPH_CONFIG} from "../../config";
import {SCENARIO_INTERVALS} from "../../util-components/scenarios/scenario-info";
import ScenarioPicker from "../../util-components/scenarios/scenario-picker";
import en from "date-fns/locale/en-GB";


const GRAPH_CONSTANTS = {
    dataKey: LABELS.TIMESTAMP,
    oyLabel: LABELS.CELSIUS,
    oxLabel: LABELS.SEC,
    Y_LIMIT:[GRAPH_CONFIG[CONFIG.PILOT].Y_LIMIT_MONITORING]
};

class ThermalPredictionContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleDayChange = this.handleDayChange.bind(this);
        this.handleChangePartOfDay = this.handleChangePartOfDay.bind(this);
        this.handleSimulationParams = this.handleSimulationParams.bind(this);
        this.handleScenarioChange = this.handleScenarioChange.bind(this);
        this.state = {
            activeTab: '3',
            value: this.props.startTime,
            startTime: CONFIG.IS_PILOT ? Date.UTC(new Date().getYear()+1900, new Date().getMonth(), new Date().getDate(),0,0,0)
                                       : SCENARIO_INTERVALS[0].key,
            dayTime: CONFIG.IS_PILOT ? Date.UTC(new Date().getYear()+1900, new Date().getMonth(), new Date().getDate(),0,0,0)
                                     : SCENARIO_INTERVALS[0].key,
            partOfDayTime: 0,
            dateSelected: true,
            dataCenterID: this.props.dataCenterId,
            selectedDay: CONFIG.IS_PILOT ? Date.UTC(new Date().getYear()+1900, new Date().getMonth(), new Date().getDate(),0,0,0)
                                         : SCENARIO_INTERVALS[0].key,
            partOfDaySelected: true,
            compute: false,
            rack1: 2000,
            rack2: 2000,
            roomTemp: 18,
            airFlow: 0.6,
            airTemp: 10,
            updateChart: true
        };
        this.scenario = '0';
        console.log("selected day: " + this.state.selectedDay)
        console.log("initial time: " + this.state.startTime)
    }

    convertToCelsius(result, startTime, min, dataKey, callback) {

        let rez = Parser.getFormattedData(result, startTime, min, dataKey)

        console.log(rez);

        for(let i = 0; i < rez.length; i++) {
            rez[i]['outlet probe 1 rack 1'] = (rez[i]['outlet probe 1 rack 1'] - 273.15).toFixed(2);
            rez[i]['outlet probe 1 rack 2'] = (rez[i]['outlet probe 1 rack 2'] - 273.15).toFixed(2);
            rez[i]['outlet probe 1 room'] = (rez[i]['outlet probe 1 room'] - 273.15).toFixed(2);
        }

        return rez;
    }

    componentDidMount() {

        this.scenario = '0'; //default scenario


        let api = PREDICTION_API;
        api.fetchThermalNearRealTimeSimulation(this.state.rack1,
            this.state.rack2,
            this.state.roomTemp,
            this.state.airFlow,
            this.state.airTemp,
            (result, status, err) => {
                if (result !== null && status === 200) {


                    console.log(result)
                    this.setState({
                        values: this.convertToCelsius(result, 0, 10, GRAPH_CONSTANTS.dataKey),
                        isLoaded: true,
                        /*updateChart: false*/
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                    console.log(err);
                }
            }
        )
        ;
    }

    handleSimulationParams(event) {

        event.preventDefault();
        fetchThermalNearRealTimeSimulation(
            this.state.rack1,
            this.state.rack2,
            this.state.roomTemp,
            this.state.airFlow / 10,
            this.state.airTemp,
            (result, status, err) => {
                if (result !== null && status === 200) {
                    this.setState({
                        values: this.convertToCelsius(result, 0, 10, GRAPH_CONSTANTS.dataKey),
                        isLoaded: true,
                        updateChart: false
                    }, () => this.setState({
                        updateChart: true,
                    }));
                    console.log(this.state.values);
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                    console.log(err);
                }
            });


    }

    handleChangeAirTemp = value => {
        this.setState({
            airTemp: value
        })
    };

    handleChangeRack1 = value => {
        this.setState({
            rack1: value
        })
    };

    handleChangeRack2 = value => {
        this.setState({
            rack2: value
        })
    };

    handleChangeRoomTemp = value => {
        this.setState({
            roomTemp: value
        })
    };

    handleChangeAirFlow = value => {
        this.setState({
            airFlow: value
        })
    };

    handleChange(event) {

        this.setState({
            startTime: event.target.value,
            dateSelected: false,
        }, () => this.setState({
            dateSelected: true,
        }));
        console.log(event.target.value);

        console.log(event);
    }

    handleChangePartOfDay(event) {
        this.setState({
            startTime: event.target.value,
            activeTab: '2',
            partOfDaySelected: false,
        }, () => this.setState({
            partOfDaySelected: true,
        }));
        console.log(event.target.value);

        console.log(event);
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    handleDayChange(day) {

        var ms = Date.UTC(day.getYear()+1900, day.getMonth(), day.getDate(), 0, 0, 0);

        console.log("Picked day " + ms);
        this.setState({selectedDay: day});
        this.setState({
            startTime: Date.UTC(day.getYear()+1900, day.getMonth(), day.getDate(),0,0,0),
            dateSelected: false,
            dayTime: Date.UTC(day.getYear()+1900, day.getMonth(), day.getDate(),0,0,0),
            partOfDaySelected:false
        }, () => this.setState({
            dateSelected: true,
            partOfDaySelected: true
        }));
    }

    handleScenarioChange(time){

        console.log("Picked time " + time + " from SCENARIO");

        var newDate = new Date();
        newDate.setTime(time);

        console.log("Picked day " + newDate + " from SCENARIO");
        this.setState({selectedDay: newDate});
        this.setState({
            startTime: newDate.getTime(),
            dateSelected: false,
            dayTime: newDate.getTime(),
            partOfDaySelected:false,
        }, () => this.setState({
            dateSelected: true,
            partOfDaySelected: true,
        }));
    }



    parseDate(str, format, locale) {
        const parsed = dateFnsParse(str, format, new Date(), { locale:en });
        if (DateUtils.isDate(parsed)) {
            return parsed;
        }
        return undefined;
    }

    formatDate(date, format, locale) {
        return dateFnsFormat(date, format, { locale:en });
    }

    handleChangePartOfDay(event){

        console.log("Picked added hours " + event.target.value);

        var selectedDate = new Date();
        selectedDate.setTime(event.target.value);
        var newDate = this.state.dayTime + selectedDate.getTime();
        console.log("New date " + newDate);
        this.setState({
            partOfTime: event.target.value,
            startTime: newDate,
            activeTab: '2',
            partOfDaySelected: false,
            value: this.state.startTime
        }, () => this.setState({
            partOfDaySelected: true,
            //startTime: this.state.startTime - this.state.partOfTime
        }));
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render() {

        console.log(this.scenario);
        const FORMAT = 'dd/MM/yyyy';
        const { selectedDay } = this.state;

        let xAxis =
            <XAxis dataKey={GRAPH_CONSTANTS.dataKey} label={{
                value: GRAPH_CONSTANTS.oxLabel,
                offset: 0,
                position: "insideBottomRight"
            }}/>;

        let yAxis =
            <YAxis
                domain={[0, Number(GRAPH_CONSTANTS.Y_LIMIT)]}
                label={{value: GRAPH_CONSTANTS.oyLabel, angle: -90, position: 'insideLeft'}}/>;

        return (

            <div className={styles.card}>
                <CardHeader>
                    <strong> DC Forecasting > Thermal Predictions </strong>
                </CardHeader>
                {/*<h3><Badge color="danger">Thermal Energy Prediction</Badge></h3>*/}
                <div className={styles.body}>
                    <div>
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    className={classnames({active: this.state.activeTab === '3'})}
                                    onClick={() => {
                                        this.toggle('3');
                                    }}
                                >
                                    Day-Ahead Prediction
                                </NavLink>
                            </NavItem>

                            <NavItem>
                                <NavLink
                                    className={classnames({active: this.state.activeTab === '2'})}
                                    onClick={() => {
                                        this.toggle('2');
                                    }}
                                >
                                    Intraday Prediction
                                </NavLink>
                            </NavItem>

                            <NavItem>
                                <NavLink
                                    className={classnames({active: this.state.activeTab === '1'})}
                                    onClick={() => {
                                        this.toggle('1');
                                    }}
                                >
                                    Near Real Time Prediction
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <Row>
                                    <Col sm="8">
                                        <div className={styles.card}>
                                            <br/>
                                            {this.state.updateChart &&
                                            <Card body>
                                                <h5 className={styles.chartTitle}>Server Room Probes Temperature</h5>
                                                <ResponsiveContainer width="95%" height={400}>
                                                    <LineChart width={700} height={400} data={this.state.values}>
                                                        <CartesianGrid strokeDasharray="3 3"/>

                                                        {xAxis}
                                                        {yAxis}

                                                        <Legend verticalAlign="top"
                                                                wrapperStyle={{lineHeight: '40px'}}/>
                                                        <Tooltip/>
                                                        <ReferenceLine y={0} stroke='#000'/>

                                                        <Brush dataKey={GRAPH_CONSTANTS.dataKey} height={30}
                                                               offset={-10}
                                                               stroke="#9e9e9e"/>}

                                                        <Line type="monotone" fill="#64dd17" fillOpacity={.6}
                                                              stroke="#64dd17" activeDot={{r: 8}}
                                                              dataKey="outlet probe 1 room"/>

                                                        <Line type="monotone" fill="#6200ea" fillOpacity={.6}
                                                              stroke="#6200ea" activeDot={{r: 8}}
                                                              dataKey="outlet probe 1 rack 1"/>

                                                        <Line type="monotone" fill="#d50000" fillOpacity={.6}
                                                              stroke="#d50000" activeDot={{r: 8}}
                                                              dataKey="outlet probe 1 rack 2"/>
                                                    </LineChart>
                                                </ResponsiveContainer>
                                            </Card>
                                            }
                                        </div>
                                    </Col>
                                    <Col sm="4">
                                        <br/>
                                        <h5> Select simulation parameters: </h5>
                                        <br/>
                                        <br/>
                                        <form onSubmit={this.handleSimulationParams}>
                                            <center> RACK 1 Load [2000-10000] W  &nbsp; &nbsp; &nbsp;  Current
                                                Value: {this.state.rack1} W
                                            </center>
                                            <Slider
                                                min={2000}
                                                max={10000}
                                                value={this.state.rack1}
                                                onChangeStart={this.handleChangeStartRack1}
                                                onChange={this.handleChangeRack1}
                                                onChangeComplete={this.handleChangeCompleteRack1}
                                            />

                                            <center> RACK 2 Load [2000-10000] W  &nbsp; &nbsp; &nbsp;  Current
                                                Value: {this.state.rack2} W
                                            </center>
                                            <Slider
                                                min={2000}
                                                max={10000}
                                                value={this.state.rack2}
                                                onChange={this.handleChangeRack2}
                                            />

                                            <center> Initial Room Temperature [18-26] °C  &nbsp; &nbsp; &nbsp;  Current
                                                Value: {this.state.roomTemp} °C
                                            </center>
                                            <Slider
                                                min={18}
                                                max={26}
                                                value={this.state.roomTemp}
                                                onChange={this.handleChangeRoomTemp}
                                            />
                                            <center> Air Flow [0.6-1.8] m3/s  &nbsp; &nbsp; &nbsp;  Current
                                                Value: {this.state.airFlow / 10} °C
                                            </center>
                                            <Slider
                                                min={6}
                                                max={18}
                                                value={this.state.airFlow}
                                                onChange={this.handleChangeAirFlow}
                                            />
                                            <center> Input Air Temperature Range [10-18]
                                                °C  &nbsp; &nbsp; &nbsp;  Current
                                                Value: {this.state.airTemp} °C
                                            </center>
                                            <Slider
                                                min={10}
                                                max={18}
                                                value={this.state.airTemp}
                                                onChange={this.handleChangeAirTemp}
                                            />
                                            <br/>
                                            &nbsp; &nbsp;<input className={styles.buttonCard} name={"params"}
                                                                type="submit"
                                                                value="Simulate Scenario" className="btn btn-success"/>
                                        </form>
                                    </Col>
                                </Row>

                            </TabPane>
                            {
                            <TabPane tabId="2">
                                <div className={styles.card}>
                                    <br/>
                                        {this.scenario === '0' &&
                                        <label>
                                            <div>
                                                {CONFIG.IS_PILOT &&
                                                <p>Select a date: </p>
                                                }
                                                {CONFIG.IS_PILOT &&
                                                <DayPickerInput
                                                    formatDate={this.formatDate}
                                                    format={FORMAT}
                                                    parseDate={this.parseDate}
                                                    placeholder={`${dateFnsFormat(selectedDay, FORMAT)}`}
                                                    onDayChange={this.handleDayChange}/>}
                                                {!CONFIG.IS_PILOT  &&
                                                <ScenarioPicker handler={this.handleScenarioChange}/>}
                                                &nbsp;&nbsp;Select part of day:&nbsp;&nbsp;
                                                <select value= {this.state.partOfTime}  onChange={this.handleChangePartOfDay}>
                                                    <option value={0}>00:00-04:00</option>
                                                    <option value={14400000}>04:00-08:00</option>
                                                    <option value={28800000}>08:00-12:00</option>
                                                    <option value={43200000}>12:00-16:00</option>
                                                    <option value={57600000}>16:00-20:00</option>
                                                    <option value={72000000}>20:00-00:00</option>
                                                </select>
                                            </div>
                                        </label>
                                        }
                                        &nbsp; &nbsp;
                                </div>
                                {this.state.partOfDaySelected &&
                                <ThermalPredictionWithoutFlexibilityTab
                                    historyProductionChartTitle="Energy production over the last 4 hours"
                                    predictionProductionChartTitle="Energy production prediction over the next 4 hours"
                                    granularity={PredictionGranularity.INTRADAY}
                                    dataCenterId={this.props.dataCenterId}
                                    startTime={this.state.startTime}
                                    domainUpperBound={1500}/>
                                }
                            </TabPane>}
                            {
                            <TabPane tabId="3">
                                <div className={styles.card}>
                                    <br/>
                                        {this.scenario === '0' &&
                                        <label>
                                            <div>
                                                {CONFIG.IS_PILOT &&
                                                <p>Select a date: </p>
                                                }
                                                {CONFIG.IS_PILOT &&
                                                <DayPickerInput
                                                    formatDate={this.formatDate}
                                                    format={FORMAT}
                                                    parseDate={this.parseDate}
                                                    placeholder={`${dateFnsFormat(selectedDay, FORMAT)}`}
                                                    onDayChange={this.handleDayChange}/>}
                                                {!CONFIG.IS_PILOT  &&
                                                <ScenarioPicker handler={this.handleScenarioChange}/>}
                                            </div>
                                        </label>
                                        }
                                        &nbsp; &nbsp;
                                </div>
                                {this.state.dateSelected &&
                                <ThermalPredictionTab
                                    historyProductionChartTitle="Energy production over the last 24 hours"
                                    predictionProductionChartTitle="Energy production prediction over the next 24 hours"
                                    electricalFlexibilityChartTitle="Predicted energy flexibility for the next 24 hours"
                                    granularity={PredictionGranularity.DAYAHEAD}
                                    dataCenterId={this.props.dataCenterId}
                                    startTime={this.state.startTime}
                                    domainUpperBound={1500}/>
                                }
                            </TabPane>}
                        </TabContent>
                    </div>
                </div>
            </div>
        )
    }
    ;
}

export default ThermalPredictionContainer
