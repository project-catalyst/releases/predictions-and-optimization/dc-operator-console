import React, {Component} from "react";
import {Col, Row} from "reactstrap";
import ProductionChart from "../../commons/graphics/production-chart";
import {PredictionGranularity, ValuesType} from "../../commons/constants/business-constants";
import FlexibilityChart from "../../commons/graphics/flexibility-chart";

class ThermalPredictionTab extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div>
                <Row>
                    <Col sm="6">
                        <br/>
                        <br/>
                        <ProductionChart
                            title={this.props.historyProductionChartTitle}
                            granularity={this.props.granularity}
                            dataCenterId={this.props.dataCenterId}
                            startTime={this.props.startTime}
                            valuesType={ValuesType.MONITORED}
                            domainUpperBound={this.props.domainUpperBound}/>
                    </Col>
                    <Col sm="6">
                        <br/>
                        <br/>
                        <ProductionChart
                            title={this.props.predictionProductionChartTitle}
                            granularity={this.props.granularity}
                            dataCenterId={this.props.dataCenterId}
                            startTime={this.props.startTime}
                            valuesType={ValuesType.PREDICTED}
                            domainUpperBound={this.props.domainUpperBound}/>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <br/>
                        <br/>
                        <FlexibilityChart
                            title={this.props.electricalFlexibilityChartTitle}
                            granularity={PredictionGranularity.DAYAHEAD}
                            dataCenterId={this.props.dataCenterId}
                            startTime={this.props.startTime}
                            domainUpperBound={this.props.domainUpperBound}/>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default ThermalPredictionTab;
