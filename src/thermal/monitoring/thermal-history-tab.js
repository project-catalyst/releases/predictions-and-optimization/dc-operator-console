import React, {Component} from "react";
import {Col, Row} from "reactstrap";
import ProductionChart from "../../commons/graphics/production-chart";
import {ValuesType} from "../../commons/constants/business-constants";


class ThermalHistoryTab extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return(
            <Row>
                <Col sm="6">
                    <br/>
                    <br/>
                    <ProductionChart
                        title={this.props.productionConsumptionChartTitle}
                        granularity={this.props.granularity}
                        dataCenterId={this.props.dataCenterId}
                        startTime={this.props.startTime}
                        valuesType={ValuesType.MONITORED}
                        domainUpperBound={this.props.domainUpperBound}/>
                </Col>
            </Row>
        )
    }
}

export default ThermalHistoryTab;
