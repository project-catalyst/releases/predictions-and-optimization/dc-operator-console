import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";
import {PredictionGranularity} from "../../../commons/constants/business-constants";
import {CONFIG} from "../../../config";

function fetchDataCenterProductionData(granularity, dataCenterId, startTime, callback){

    let urlPath;
    switch (granularity) {
        case PredictionGranularity.DAYAHEAD:
            urlPath =  '/graph-data/thermal/history/production/dayahead';
            break;
        case PredictionGranularity.INTRADAY:
            urlPath =  '/graph-data/thermal/history/production/intraday';
            break;
        case PredictionGranularity.NEAR_REAL_TIME:
            urlPath = '/graph-data/thermal/history/production/nearRealTime';
            break;
    }

    urlPath = urlPath + '/' + startTime;

    let request = new Request(HOST.CATALYST_DB_API + urlPath, { method: 'GET'});

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    fetchDataCenterProductionData
}
