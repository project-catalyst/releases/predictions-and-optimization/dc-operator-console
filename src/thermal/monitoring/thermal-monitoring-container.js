import React from 'react';
import styles from '../../commons/styles/catalyst-style.css';
import {CardHeader, Nav, NavItem, NavLink, TabContent, TabPane} from 'reactstrap';
import classnames from 'classnames';
import {PredictionGranularity} from "../../commons/constants/business-constants";
import 'react-day-picker/lib/style.css';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import dateFnsFormat from 'date-fns/format';
import dateFnsParse from 'date-fns/parse';
import {DateUtils} from 'react-day-picker';
import ThermalHistoryTab from "../monitoring/thermal-history-tab";
import {CONFIG} from "../../config";
import {SCENARIO_INTERVALS} from "../../util-components/scenarios/scenario-info";
import ScenarioPicker from "../../util-components/scenarios/scenario-picker";
import en  from 'date-fns/locale/en-GB'

class ThermalMonitoringContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleDayChange = this.handleDayChange.bind(this);
        this.handleChangePartOfDay = this.handleChangePartOfDay.bind(this);
        this.handleScenarioChange = this.handleScenarioChange.bind(this);
        this.state = {
            activeTab: '3',
            value: this.props.startTime,
            startTime:  CONFIG.IS_PILOT ? Date.UTC(new Date().getYear()+1900, new Date().getMonth(), new Date().getDate(),0,0,0)
                                        : SCENARIO_INTERVALS[0].key,
            dayTime:  CONFIG.IS_PILOT ? Date.UTC(new Date().getYear()+1900, new Date().getMonth(), new Date().getDate(),0,0,0)
                                        : SCENARIO_INTERVALS[0].key,
            partOfDayTime: 0,
            dateSelected: true,
            dataCenterID: this.props.dataCenterId,
            selectedDay:  CONFIG.IS_PILOT ? Date.UTC(new Date().getYear()+1900, new Date().getMonth(), new Date().getDate(),0,0,0)
                                          : SCENARIO_INTERVALS[0].key,
            partOfDaySelected: true,
        };
        this.scenario = localStorage.getItem('scenario');
    }

    componentDidMount() {
            this.scenario = '0'; //default scenario
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    handleChange(event) {

        this.setState({
            startTime: event.target.value,
            dateSelected: false,
        }, () => this.setState({
            dateSelected: true,
        }));
        console.log(event.target.value);

        console.log(event);
    }

    handleChangePartOfDay(event){
        console.log("Picked added hours " + event.target.value);
        console.log(this.state.startTime);
        var selectedDate = new Date();
        selectedDate.setTime(event.target.value);
        var newDate = this.state.startTime + selectedDate.getTime();
        console.log("New date " + newDate);
        this.setState({
            startTime: this.state.startTime + selectedDate.getTime(),
            activeTab: '2',
            partOfDaySelected: false,
        }, () => this.setState({
            partOfDaySelected: true,
        }));

        console.log("New start time " + this.state.startTime)
    }

    handleDayChange(day) {

        var ms = Date.UTC(day.getYear()+1900, day.getMonth(), day.getDate(), 0, 0, 0);

        console.log("Picked day " + ms);
        this.setState({selectedDay: day});
        this.setState({
            startTime: Date.UTC(day.getYear()+1900, day.getMonth(), day.getDate(),0,0,0),
            dateSelected: false,
            dayTime: Date.UTC(day.getYear()+1900, day.getMonth(), day.getDate(),0,0,0),
            partOfDaySelected:false
        }, () => this.setState({
            dateSelected: true,
            partOfDaySelected: true
        }));
    }

    handleScenarioChange(time){

        console.log("Picked time " + time + " from SCENARIO");

        var newDate = new Date();
        newDate.setTime(time);

        console.log("Picked day " + newDate + " from SCENARIO");
        this.setState({selectedDay: newDate});
        this.setState({
            startTime: newDate.getTime(),
            dateSelected: false,
            dayTime: newDate.getTime(),
            partOfDaySelected:false,
        }, () => this.setState({
            dateSelected: true,
            partOfDaySelected: true,
        }));
    }

    parseDate(str, format, locale) {
        const parsed = dateFnsParse(str, format, new Date(), { locale:en });
        if (DateUtils.isDate(parsed)) {
            return parsed;
        }
        return undefined;
    }

    formatDate(date, format, locale) {
        return dateFnsFormat(date, format, { locale:en });
    }

    handleChangePartOfDay(event){

        console.log("Picked added hours " + event.target.value);

        var selectedDate = new Date();
        selectedDate.setTime(event.target.value);
        var newDate = this.state.dayTime + selectedDate.getTime();
        console.log("New date " + newDate);
        this.setState({
            partOfTime: event.target.value,
            startTime: newDate,
            activeTab: '2',
            partOfDaySelected: false,
            value: this.state.startTime
        }, () => this.setState({
            partOfDaySelected: true,
            //startTime: this.state.startTime - this.state.partOfTime
        }));
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render() {
        console.log(this.scenario);
        const FORMAT = 'dd/MM/yyyy';
        const { selectedDay } = this.state;
        return (
            <div className={styles.card}>
                {/*<h3><Badge color="danger">Thermal Energy Monitoring</Badge></h3>*/}
                <CardHeader>
                    <strong> DC Monitoring > Thermal Values </strong>
                </CardHeader>
                <div className={styles.body}>
                    <div>
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    className={classnames({active: this.state.activeTab === '3'})}
                                    onClick={() => {
                                        this.toggle('3');
                                    }}
                                >
                                    Last 24 Hours
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({active: this.state.activeTab === '2'})}
                                    onClick={() => {
                                        this.toggle('2');
                                    }}
                                >
                                    Last 4 Hours
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            {
                            <TabPane tabId="2">
                                <div className={styles.card}>
                                    <br/>
                                        {this.scenario === '0' &&
                                        <label>
                                            <div>
                                                {CONFIG.IS_PILOT &&
                                                <p>Select a date: </p>
                                                }
                                                {CONFIG.IS_PILOT &&
                                                <DayPickerInput
                                                    formatDate={this.formatDate}
                                                    format={FORMAT}
                                                    parseDate={this.parseDate}
                                                    placeholder={`${dateFnsFormat(selectedDay, FORMAT)}`}
                                                    onDayChange={this.handleDayChange}/>}
                                                {!CONFIG.IS_PILOT  &&
                                                <ScenarioPicker handler={this.handleScenarioChange}/>}
                                                &nbsp;&nbsp;Select part of day:&nbsp;&nbsp;
                                                <select value= {this.state.partOfTime}  onChange={this.handleChangePartOfDay}>
                                                    <option value={0}>00:00-04:00</option>
                                                    <option value={14400000}>04:00-08:00</option>
                                                    <option value={28800000}>08:00-12:00</option>
                                                    <option value={43200000}>12:00-16:00</option>
                                                    <option value={57600000}>16:00-20:00</option>
                                                    <option value={72000000}>20:00-00:00</option>
                                                </select>
                                            </div>
                                        </label>
                                        }
                                        &nbsp; &nbsp;
                                </div>
                                {this.state.partOfDaySelected &&
                                <ThermalHistoryTab
                                    productionConsumptionChartTitle="Energy production over the last 4 hours"
                                    granularity={PredictionGranularity.INTRADAY}
                                    dataCenterId={this.props.dataCenterId}
                                    startTime={this.state.startTime}
                                    domainUpperBound={1500}/>
                                }
                            </TabPane>
                            }
                            {
                            <TabPane tabId="3">
                                <div className={styles.card}>
                                    <br/>
                                        {this.scenario === '0' &&
                                        <label>
                                            <div>
                                                {CONFIG.IS_PILOT &&
                                                <p>Select a date: </p>
                                                }
                                                {CONFIG.IS_PILOT &&
                                                <DayPickerInput
                                                    formatDate={this.formatDate}
                                                    format={FORMAT}
                                                    parseDate={this.parseDate}
                                                    placeholder={`${dateFnsFormat(selectedDay, FORMAT)}`}
                                                    onDayChange={this.handleDayChange}/>}
                                                {!CONFIG.IS_PILOT  &&
                                                <ScenarioPicker handler={this.handleScenarioChange}/>}
                                            </div>
                                        </label>
                                        }
                                        &nbsp; &nbsp;
                                </div>
                                {this.state.dateSelected &&
                                <ThermalHistoryTab
                                    productionConsumptionChartTitle="Energy production over the last 24 hours"
                                    granularity={PredictionGranularity.DAYAHEAD}
                                    dataCenterId={this.props.dataCenterId}
                                    startTime={this.state.startTime}
                                    domainUpperBound={1500}/>
                                }
                            </TabPane>
                            }
                        </TabContent>
                    </div>
                </div>
            </div>
        )
    };
}

export default ThermalMonitoringContainer
