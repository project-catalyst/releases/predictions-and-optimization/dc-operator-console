import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";
import {CONFIG} from "../../../config";

const ENDPOINT = {

    // Electrical Production-Consumption for DC
    DAYAHEAD_PREDICTED_FLEXIBILITY_CONSUMPTION_FOR_DC:
        '/graph-data/flexibility/dayahead',

};

function fetchDataCenterElectricalFlexibilityData(granularity, dataCenterId, startTime, callback){

    let urlPath = '';

    urlPath = ENDPOINT.DAYAHEAD_PREDICTED_FLEXIBILITY_CONSUMPTION_FOR_DC + '/' + startTime;


    let request = new Request(HOST.CATALYST_DB_API + urlPath, { method: 'GET'});

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    fetchDataCenterElectricalFlexibilityData
}

