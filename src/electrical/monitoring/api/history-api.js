import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";
import {PredictionGranularity} from "../../../commons/constants/business-constants";
import {CONFIG} from "../../../config";

function fetchDataCenterProductionConsumptionData(granularity, dataCenterId, startTime, callback) {

    let urlRootPath = '/graph-data/electrical/history/production-consumption/';
    let urlPath;
    switch (granularity) {
        case PredictionGranularity.DAYAHEAD:
            urlPath = urlRootPath + 'dayahead';
            break;
        case PredictionGranularity.INTRADAY:
            urlPath = urlRootPath + 'intraday';
            break;
        case PredictionGranularity.NEAR_REAL_TIME:
            urlPath = urlRootPath + 'nearRealTime';
            break;
    }


    urlPath = urlPath + '/' + startTime;

    let request = new Request(HOST.CATALYST_DB_API + urlPath, {method: 'GET'});

    console.log("REQUEST URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}


function fetchDataCenterDisaggregatedData(granularity, dataCenterId, startTime, callback) {

    let urlRootPath = '/graph-data/electrical/history/disaggregated/';
    let urlPath;
    switch (granularity) {
        case PredictionGranularity.DAYAHEAD:
            urlPath = urlRootPath + 'dayahead';
            break;
        case PredictionGranularity.INTRADAY:
            urlPath = urlRootPath + 'intraday';
            break;
        case PredictionGranularity.NEAR_REAL_TIME:
            urlPath = urlRootPath + 'nearRealTime';
            break;
    }

    urlPath = urlPath + '/' + startTime;


    let request = new Request(HOST.CATALYST_DB_API + urlPath, {method: 'GET'});

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

const NRL_URL = '/electrical/prediction/consumption/nearRealTime';

function fetchDataCenterNRTPrediction(granularity, dataCenterId, startTime, callback) {

    let urlRootPath = '';

    urlRootPath = NRL_URL + '/entireDC/' + Number(startTime + 86400000);

    let request = new Request(HOST.CATALYST_DB_API + urlRootPath, {method: 'GET'});

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function fetchDataCenterNRTDisaggregatedCooling(granularity, dataCenterId, startTime, callback) {

    let urlRootPath = '';

    urlRootPath = NRL_URL + '/coolingSystem/' + Number(startTime + 86400000);

    let request = new Request(HOST.CATALYST_DB_API + urlRootPath, {method: 'GET'});

    RestApiClient.performRequest(request, callback);
}

function fetchDataCenterNRTDisaggregatedServer(granularity, dataCenterId, startTime, callback) {

    let urlRootPath = '';

    urlRootPath = NRL_URL + '/itComponent/' + Number(startTime + 86400000);

    let request = new Request(HOST.CATALYST_DB_API + urlRootPath, {method: 'GET'});

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    fetchDataCenterProductionConsumptionData,
    fetchDataCenterDisaggregatedData,
    fetchDataCenterNRTPrediction,
    fetchDataCenterNRTDisaggregatedCooling,
    fetchDataCenterNRTDisaggregatedServer
}
