import {HOST} from "../../../commons/hosts";
import RestApiClient from "../../../commons/api/rest-client"
import {PredictionGranularity} from "../../../commons/constants/business-constants";
import {CONFIG} from "../../../config";


function fetchDataCenterProductionConsumptionData(granularity, dataCenterId, startTime, callback) {

    let urlRootPath = '/graph-data/electrical/prediction/production-consumption/';
    let urlPath;
    switch (granularity) {
        case PredictionGranularity.DAYAHEAD:
            urlPath = urlRootPath + 'dayahead';
            break;
        case PredictionGranularity.INTRADAY:
            urlPath = urlRootPath + 'intraday';
            break;
        case PredictionGranularity.NEAR_REAL_TIME:
            urlPath = urlRootPath + 'nearRealTime';
            break;
    }


    urlPath = urlPath + '/' + startTime;

    let request = new Request(HOST.CATALYST_DB_API + urlPath, { method: 'GET'});

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function fetchDataCenterDisaggregatedData(granularity, dataCenterId, startTime, callback) {

    let urlRootPath = '/graph-data/electrical/prediction/disaggregated/';
    let urlPath;
    switch (granularity) {
        case PredictionGranularity.DAYAHEAD:
            urlPath = urlRootPath + 'dayahead';
            break;
        case PredictionGranularity.INTRADAY:
            urlPath = urlRootPath + 'intraday';
            break;
        case PredictionGranularity.NEAR_REAL_TIME:
            urlPath = urlRootPath + 'nearRealTime';
            break;
    }

    urlPath = urlPath + '/' + startTime;

    let request = new Request(HOST.CATALYST_DB_API + urlPath, { method: 'GET'});

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    fetchDataCenterProductionConsumptionData,
    fetchDataCenterDisaggregatedData
}
