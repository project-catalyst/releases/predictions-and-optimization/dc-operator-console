import React, {Component} from "react";
import {Col, Row} from "reactstrap";
import {ValuesType} from "../../commons/constants/business-constants";
import ProductionConsumptionNRT from "../../commons/graphics/production-consumption-nrt";
import DisaggregatedConsumptionChartNRT from "../../commons/graphics/disaggregated-consumption-chart-nrt";

class ElectricalPredictionWithoutFlexibilityTabNrt extends Component {

    constructor(props) {
        super(props);
        this.state = {
            startTime: Date.UTC(new Date().getYear()+1900, new Date().getMonth(), new Date().getDate(),0,0,0),
        };
    }

    render() {
        return (
            <div>
                <Row>
                    <Col sm="12">
                        <br/>
                        <br/>
                        <ProductionConsumptionNRT
                            title={this.props.historyProductionConsumptionChartTitle}
                            granularity={this.props.granularity}
                            dataCenterId={this.props.dataCenterId}
                            startTime={this.state.startTime}
                            valuesType={ValuesType.MONITORED}
                            domainUpperBound={this.props.domainUpperBound}/>
                    </Col>
                </Row>

                <Row>
                    <Col sm="12">
                        <br/>
                        <br/>
                        <DisaggregatedConsumptionChartNRT
                            title={this.props.disaggregatedHistoryConsumptionChartTitle}
                            granularity={this.props.granularity}
                            dataCenterId={this.props.dataCenterId}
                            startTime={this.state.startTime}
                            valuesType={ValuesType.MONITORED}
                            domainUpperBound={this.props.domainUpperBound}/>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default ElectricalPredictionWithoutFlexibilityTabNrt;
