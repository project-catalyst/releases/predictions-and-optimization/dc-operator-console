import React, {Component} from "react";
import {Col, Row} from "reactstrap";
import ProductionConsumptionChart from "../../commons/graphics/production-consumption-chart";
import {ValuesType} from "../../commons/constants/business-constants";
import DisaggregatedConsumptionChart from "../../commons/graphics/disaggregated-consumption-chart";

class ElectricalPredictionWithoutFlexibilityTab extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div>
                <Row>
                    <Col sm="6">
                        <br/>
                        <br/>
                        <ProductionConsumptionChart
                            title={this.props.historyProductionConsumptionChartTitle}
                            granularity={this.props.granularity}
                            dataCenterId={this.props.dataCenterId}
                            startTime={this.props.startTime}
                            valuesType={ValuesType.MONITORED}
                            domainUpperBound={this.props.domainUpperBound}/>
                    </Col>
                    <Col sm="6">
                        <br/>
                        <br/>

                        <ProductionConsumptionChart
                            title={this.props.predictionProductionConsumptionChartTitle}
                            granularity={this.props.granularity}
                            dataCenterId={this.props.dataCenterId}
                            startTime={this.props.startTime}
                            valuesType={ValuesType.PREDICTED}
                            domainUpperBound={this.props.domainUpperBound}/>
                    </Col>
                </Row>

                <Row>
                    <Col sm="6">
                        <br/>
                        <br/>
                        <DisaggregatedConsumptionChart
                            title={this.props.disaggregatedHistoryConsumptionChartTitle}
                            granularity={this.props.granularity}
                            dataCenterId={this.props.dataCenterId}
                            startTime={this.props.startTime}
                            valuesType={ValuesType.MONITORED}
                            domainUpperBound={this.props.domainUpperBound}/>
                    </Col>
                    <Col sm="6">
                        <br/>
                        <br/>
                        <DisaggregatedConsumptionChart
                            title={this.props.disaggregatedPredictedConsumptionChartTitle}
                            granularity={this.props.granularity}
                            dataCenterId={this.props.dataCenterId}
                            startTime={this.props.startTime}
                            valuesType={ValuesType.PREDICTED}
                            domainUpperBound={this.props.domainUpperBound}/>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default ElectricalPredictionWithoutFlexibilityTab;
