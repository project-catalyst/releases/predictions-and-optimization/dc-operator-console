const proxy = require('http-proxy-middleware');

var ipThermal =  process.env.THERMAL_API ||'http://dsrl.fairuse.org:9098';
var ipDBapi = process.env.MYSQL_DB_API || 'http://localhost:8081/';


module.exports = function(app) {
    app.use(proxy('/db-api-1.0.0/**', { target: ipDBapi , logLevel: 'debug'}));
    app.use(proxy('/catalyst-ebb-thermal/**', { target: ipThermal , logLevel: 'debug'}));
};
