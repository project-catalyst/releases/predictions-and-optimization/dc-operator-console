import {HOST} from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const ENDPOINTS = {
    POST: '/strategies/insert',
    GET: '/flexibility-strategy/'
};

function postStrategy(values, callback) {
    let request = new Request(HOST.CATALYST_DB_API + ENDPOINTS.POST, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            we: values.we,
            wl: values.wl,
            wf: values.wf,
            wt: values.wt,
            ren: values.wr,
            reallocActive: values.reallocActive
        })
    });

    console.log(values);
    console.log(request.body)
    RestApiClient.performRequest(request, callback);
}
export {
    postStrategy, ENDPOINTS
}
