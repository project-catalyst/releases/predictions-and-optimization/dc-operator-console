import Label from "reactstrap/es/Label";
import Slider from "react-rangeslider";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {Button, CardBody, Col} from "reactstrap";
import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import {green} from "@material-ui/core/colors";
import {Checkbox} from "@material-ui/core";
import Row from "react-bootstrap/Row";

const textStyle = {
    color: 'gray',
    bold: 'true',
    textAlign: 'justify'
};

const GreenCheckbox = withStyles({
    root: {
        color: green[400],
        '&$checked': {
            color: green[600],
        },
    },
    checked: {},
})((props) => <Checkbox color="default" {...props} />);

export default class weightSelection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            we: 0,
            wf: 100,
            wt: 0,
            wren: 0,
            ra: 0,
        };
        this.postW = this.props.postW.bind(this);

        this.handleChangeWe = this.handleChangeWe.bind(this);
        this.handleChangeWf = this.handleChangeWf.bind(this);
        this.handleChangeWt = this.handleChangeWt.bind(this);
        this.handleChangeRa = this.handleChangeRa.bind(this);
        this.handleChangeWren = this.handleChangeWren.bind(this);
    }

    handleChangeWe = value => {
        this.setState({
            we: value
        })
    };

    handleChangeWf = value => {
        this.setState({
            wf: value
        })
    };

    handleChangeWt = value => {
        this.setState({
            wt: value
        })
    };

    handleChangeWren = value => {
        this.setState({
            wren: value
        })
    };

    handleChangeRa = value => {
        this.setState({
            ra: !this.state.ra
        })
    };

    render() {
        return (
            <div>
                <Row>
                    <Col md={{size: 5, offset: 1}}>
                        <Label><strong style={textStyle}> DC Profit Maximisation Strategy Weight </strong></Label>
                        <Slider
                            min={0}
                            max={100}
                            value={this.state.we}
                            onChange={this.handleChangeWe}
                        />
                    </Col>
                    <Col md={{size: 5, offset: 1}}>
                        <CardBody>
                        <Label><strong style={textStyle}>
                            The Intra DC Energy Optimizer computes an action plan to minimize the DC operational cost
                            by shifting flexible energy to decrease energy profile when energy price is high and to
                            increase
                            energy profile when energy price is low.
                        </strong></Label>
                        </CardBody>
                    </Col>
                </Row>
                <Row>
                    <Col md={{size: 5, offset: 1}}>
                        <Label><strong style={textStyle}> DC Electrical Flexibility Strategy Weight </strong></Label>
                        <Slider
                            min={0}
                            max={100}
                            value={this.state.wf}
                            onChange={this.handleChangeWf}
                        />
                    </Col>
                    <Col md={{size: 5, offset: 1}}>
                        <CardBody>
                            <Label><strong style={textStyle}>
                                The Intra DC Energy Optimizer computes an action plan to allow the DC to follow a
                                flexibility order curve and maximize gain of the service associated incentives
                            </strong></Label>
                        </CardBody>
                    </Col>
                </Row>
                <Row>
                    <Col md={{size: 5, offset: 1}}>
                        <Label><strong style={textStyle}> DC Thermal Flexibility Strategy Weight </strong></Label>
                        <Slider
                            min={0}
                            max={100}
                            value={this.state.wt}
                            onChange={this.handleChangeWt}
                        />
                    </Col>
                    <Col md={{size: 5, offset: 1}}>
                        <CardBody>
                            <Label><strong style={textStyle}>
                                The Intra DC Energy Optimizer computes an action plan to maximize the DC heat generation
                                when the heat price is high by shifting flexible thermal energy.
                            </strong></Label>
                        </CardBody>
                    </Col>
                </Row>
                <Row>
                    <Col md={{size: 5, offset: 1}}>
                        <Label><strong style={textStyle}> DC Renewable Energy Usage Maximisation Weight </strong></Label>
                        <Slider
                            min={0}
                            max={100}
                            value={this.state.wren}
                            onChange={this.handleChangeWren}
                        />
                    </Col>
                    <Col md={{size: 5, offset: 1}}>
                        <CardBody>
                            <Label><strong style={textStyle}>
                                The Intra DC Energy Optimizer computes an action plan to maximise
                                the usage of the locally produced Renewable Energy
                            </strong></Label>
                        </CardBody>
                    </Col>
                </Row>
                <Row>
                    <Col md={{size: 5, offset: 1}}>
                        <FormControlLabel
                            control={<GreenCheckbox checked={this.state.ra} onChange={this.handleChangeRa}
                                                    name="checkedG"/>}
                            label="Realloc Active"
                        />
                    </Col>

                </Row>
                <Row>
                    <Col md={{size: 5, offset: 1}} >
                        <Button  color="success" block
                                onClick={e => this.postW(e, this.state.we, this.state.wf, this.state.wt, this.state.wren, this.state.ra)}> Save Strategies</Button>
                    </Col>
                </Row>
            </div>
        )
    };
}
