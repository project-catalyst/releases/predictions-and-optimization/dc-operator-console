import Parser from "../../commons/chart-components/chart-data-parser";
import {CHART_TYPE, LABEL_TYPE, LABELS} from "../../commons/constants/chart-constants";
import GenericChart from "../../commons/chart-components/generic-chart";
import React from "react";
import {COLOURS} from "../../commons/constants/colour-constants";
import {CONFIG, GRAPH_CONFIG} from "../../config"


const INPUT_LABEL = [{
    key: 'Electrical Price',
    accessor: 'ePrice',
    type: LABEL_TYPE.LINE,
    color: COLOURS.FLEXIBILITY_STRATEGY.ePrice
}]

const INPUT_DATA = {
    dataKey: LABELS.HOUR,
    oyLabel: LABELS.PRICE,
    oxLabel: LABELS.HOUR,
    yLimit: [0, GRAPH_CONFIG[CONFIG.PILOT].Y_LIMIT_PRICE],
    values: []
}

const MAX = 24;

const DC_PROFIT_MAXIMISATION = "DC Profit Maximisation";

export default class dcProfitMaximisation extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            results: this.props.results,
            char: null,
            isLoaded: false
        };
    }

    componentDidMount() {
        let finalResult = [];

        finalResult = finalResult.concat(Parser.parseDataLabel(INPUT_LABEL[0].key, this.state.results[INPUT_LABEL[0].accessor], MAX));

        let temp = {
            size: this.state.results[INPUT_LABEL[0].accessor].length,
            data: finalResult
        }
        console.log(temp);
        INPUT_DATA.values = Parser.formatLabeledValues(temp, LABELS.HOUR, 0, false);
        console.log(INPUT_DATA);

        this.setState({
            isLoaded: true,
            chart: <GenericChart
                type={CHART_TYPE.LINE}
                title={DC_PROFIT_MAXIMISATION}
                data={INPUT_DATA}
                labels={INPUT_LABEL}
            />
        })
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && this.state.chart}
            </div>
        )
    }
}
