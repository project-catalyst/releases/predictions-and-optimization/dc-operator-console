import React from 'react';
import {Card, CardHeader, CardTitle, Col, Collapse, Label, Row} from 'reactstrap';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import UncontrolledAlert from "reactstrap/es/UncontrolledAlert";
import WeightSelection from "./components/weight-selection";
import DcElectricalFlexibility from "./components/dc-electrical-flexibility"
import DcProfitMaximisation from "./components/dc-profit-maximisation"
import DcThermalFlexibility from "./components/dc-thermal-flexibility"
import DcRenewableEnergy from "./components/dc-renewable-energy"

import * as RestApiClient from "../commons/api/rest-client";
import {HOST} from "../commons/hosts";
import Button from "react-bootstrap/Button";
import BadgeStrategies from "../util-components/badge-strategies";

let FLEXIBILITY_API = require('./api/flexibility-api');

class FlexibilityStrategiesContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: localStorage.getItem('username'),
            exampleGraphReady: true,
            time: Date.now(),
            currentHour: 0,
            timeout: 1000,
            errorStatus: 0,
            error: null,
            posted: false,
            expanded: false,
            reallocActive: false,
            isLoaded: false
        };

        this.postW = this.postW.bind(this);
        this.onExpandedChange = this.onExpandedChange.bind(this);

    }

    componentDidMount() {
        this.fetchGraphData();
    }

    fetchGraphData() {
        let url = FLEXIBILITY_API.ENDPOINTS.GET;

        RestApiClient.performRequest(HOST.CATALYST_DB_API + url + this.state.time + "/00,00", (result, status, err) => {
            if (result != null && status === 200) {
                this.setState({
                    data: result,
                    isLoaded: true
                })
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err,
                    isLoaded: false
                }));
            }
        });
    }

    postW(e, we, wf, wt, wr, ra) {
        e.preventDefault();
        console.log(e.valueOf());
        const W = {
            we: we,
            wf: wf,
            wt: wt,
            wr: wr,
            reallocActive: ra
        };
        console.log(W);
        FLEXIBILITY_API.postStrategy(W, (result, status, err) => {
            if (result != null && status === 200) {
                console.log(result);
                this.setState({
                    posted: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err,
                    posted: false,
                }));
                console.log(err);
                console.log(status);
            }
        });
    }

    onExpandedChange(newExpanded) {
        this.setState({
            expanded: !this.state.expanded
        });
    }


    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Flexibility Strategies > Current Setup </strong>
                </CardHeader>
                <Row>
                    <Col sm={{size: '4', offset: 1}}>
                        <CardTitle>
                            <Button color="danger" onClick={() => this.onExpandedChange()}> Set Flexibility
                                Strategies
                                {this.state.expanded && <em> &#9650; </em>}
                                {!this.state.expanded && <em> &#9660;</em>}
                            </Button>
                        </CardTitle>
                    </Col>
                    <Col sm={{size: 'auto', offset: 4}}>
                        <Label> Current Setup:
                            <BadgeStrategies time={this.state.time} confidenceLevel={0.0}/></Label>
                    </Col>
                </Row>

                <Row>
                    <Collapse isOpen={this.state.expanded}>
                        {this.state.posted &&
                        <UncontrolledAlert color="info"> Flexibility Strategy Succesfully saved!
                        </UncontrolledAlert>}

                        <WeightSelection postW={this.postW}/>
                        {this.state.errorStatus > 0 &&
                        <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
                    </Collapse>
                </Row>
                <br/><br/>
                <Row>
                    <Col>
                        <Card body>
                            {this.state.isLoaded &&
                            <DcElectricalFlexibility
                                key={this.state.currentHour}
                                results={this.state.data}
                            />}
                        </Card>
                    </Col>
                    <Col>
                        <Card body>
                            {this.state.isLoaded &&
                            <DcProfitMaximisation
                                key={this.state.currentHour}
                                results={this.state.data}
                            />}
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card body>
                            {this.state.isLoaded &&
                            <DcThermalFlexibility
                                key={this.state.currentHour}
                                results={this.state.data}
                            />}
                        </Card>
                    </Col>
                    <Col>
                        <Card body>
                            {this.state.isLoaded &&
                            <DcRenewableEnergy
                                key={this.state.currentHour}
                                results={this.state.data}
                            />}
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    };
}

export default FlexibilityStrategiesContainer
